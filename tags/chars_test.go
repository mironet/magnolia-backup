package tags

import (
	"testing"
)

func Test_stringFromRange(t *testing.T) {
	type args struct {
		start int
		stop  int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ALPHA upper",
			args: args{
				start: 0x41,
				stop:  0x5a,
			},
			want: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
		},
		{
			name: "ALPHA lower",
			args: args{
				start: 0x61,
				stop:  0x7a,
			},
			want: "abcdefghijklmnopqrstuvwxyz",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := stringFromRange(tt.args.start, tt.args.stop); got != tt.want {
				t.Errorf("stringFromRange() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_stringFromRanges(t *testing.T) {
	type args struct {
		codepoints []int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ALPHA",
			args: args{
				codepoints: []int{0x41, 0x5a, 0x61, 0x7a},
			},
			want: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
		},
		{
			name: "hexdig upper",
			args: args{
				codepoints: []int{0x30, 0x39, 0x41, 0x46},
			},
			want: "0123456789ABCDEF",
		},
		{
			name: "hexdig lower",
			args: args{
				codepoints: []int{0x30, 0x39, 0x61, 0x66},
			},
			want: "0123456789abcdef",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := stringFromRanges(tt.args.codepoints...); got != tt.want {
				t.Errorf("stringFromRanges() = %v, want %v", got, tt.want)
			}
		})
	}
}
