# Why copy code?

The code in this directory has been copied from
<https://github.com/wal-g/wal-g/tree/master/internal/walparser>.

It cannot be used directly because is an internal package of the wal-g module.

Thanks :)
