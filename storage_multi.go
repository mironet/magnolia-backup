package main

import (
	"context"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

const (
	storageNameSeparator = "://"
)

type multiStorConfig struct {
	GCSConfigs map[string]*gcsStorConfig   `yaml:"gcsConfs,omitempty"`
	S3Configs  map[string]*s3StorConfig    `yaml:"s3Confs,omitempty"`
	AZConfigs  map[string]*azureStorConfig `yaml:"azConfs,omitempty"`
}

func (c multiStorConfig) validate(readonly bool) error {
	for name, conf := range c.GCSConfigs {
		if err := objectStorageConfigFromGCS(*conf, readonly).gcs.validate(); err != nil {
			return fmt.Errorf("invalid config for storage %s: %w", name, err)
		}
	}
	for name, conf := range c.S3Configs {
		if err := objectStorageConfigFromS3(*conf, readonly).s3.validate(); err != nil {
			return fmt.Errorf("invalid config for storage %s: %w", name, err)
		}
	}
	for name, conf := range c.AZConfigs {
		if err := objectStorageConfigFromAzure(*conf, readonly).azure.validate(); err != nil {
			return fmt.Errorf("invalid config for storage %s: %w", name, err)
		}
	}
	return nil
}

// merge merges second into first (overwriting second if necessary).
func (first *multiStorConfig) merge(second multiStorConfig) {
	for name, conf := range second.GCSConfigs {
		_, exists := first.GCSConfigs[name]
		if !exists {
			first.GCSConfigs[name] = second.GCSConfigs[name]
			continue
		}
		first.GCSConfigs[name].merge(name, conf)
	}
	for name, conf := range second.S3Configs {
		_, exists := first.S3Configs[name]
		if !exists {
			first.S3Configs[name] = second.S3Configs[name]
			continue
		}
		first.S3Configs[name].merge(name, conf)
	}
	for name, conf := range second.AZConfigs {
		_, exists := first.AZConfigs[name]
		if !exists {
			first.AZConfigs[name] = second.AZConfigs[name]
			continue
		}
		first.AZConfigs[name].merge(name, conf)
	}
}

type multiStor struct {
	stores map[string]ObjectStorage
}

func composeMultisourceConf(paths []string, readonly bool) (multiStorConfig, error) {
	out := multiStorConfig{
		GCSConfigs: make(map[string]*gcsStorConfig),
		S3Configs:  map[string]*s3StorConfig{},
		AZConfigs:  make(map[string]*azureStorConfig),
	}
	if len(paths) == 0 {
		return out, fmt.Errorf("at least one config file path required")
	}
	for i, path := range paths {
		bytes, err := os.ReadFile(path)
		if err != nil {
			return out, fmt.Errorf("could not read file %s: %w", path, err)
		}
		conf := multiStorConfig{}
		if err := yaml.Unmarshal(bytes, &conf); err != nil {
			return out, fmt.Errorf("could not unmarshal multisource yaml config file %d: %w", i, err)
		}
		out.merge(conf)
	}
	return out, out.validate(readonly)
}

func (s *multiStor) Close() error {
	for name, stor := range s.stores {
		if err := stor.Close(); err != nil {
			return fmt.Errorf("could not close %s: %w", name, err)
		}
	}
	return nil
}

func (s *multiStor) Prepare(ctx context.Context) error {
	for name, stor := range s.stores {
		if err := stor.Prepare(ctx); err != nil {
			return fmt.Errorf("could not prepare storage %s: %w", name, err)
		}
	}
	return nil
}

func prependStorageName(name, s string) string {
	return name + storageNameSeparator + s
}

func splitObjectName(s string) (string, string, error) {
	parts := strings.Split(s, storageNameSeparator)
	if len(parts) != 2 {
		return "", "", fmt.Errorf("expected a string of form \"<part0>%s<part1>\"", storageNameSeparator)
	}
	if parts[0] == "" {
		return "", "", fmt.Errorf("no storage name specified in front of object name")
	}
	return parts[0], parts[1], nil
}

func (s *multiStor) List(ctx context.Context, opts ...ListOpt) (backupItemList, error) {
	out := make(backupItemList, 0)
	errs := make([]error, 0)
	// Call List on all sub-storages.
	for name, stor := range s.stores {
		logrus.Infof("start listing items in storage %s", name)
		l, err := stor.List(ctx)
		// If one of the sub-storage list calls fails, log that, but try to
		// continue with further list calls (on other sub-storages).
		if err != nil {
			errs = append(errs, err)
			logrus.Errorf("could not list items in storage %s: %v", name, err)
			continue
		}
		// Need to copy items to prevent issue where s3 list cache leads to
		// multiple storage prefixes.
		listCopy := copyBackupItems(l)
		// Prepend storage name to each item key in the backup item list.
		for _, i := range listCopy {
			i.Key = prependStorageName(name, i.Key)
		}
		out = append(out, listCopy...)
	}
	// If all of the sub-storage list calls have failed, return an error with a
	// summarized error message.
	if len(s.stores) > 0 && len(errs) == len(s.stores) {
		return out, summarizeListErrs(s.stores, errs)
	}
	return out, nil
}

func summarizeListErrs(stores map[string]ObjectStorage, errs []error) error {
	if len(errs) != len(stores) {
		return fmt.Errorf("storage map and error slice must have equal number of elements, but have %d != %d", len(stores), len(errs))
	}
	var sb strings.Builder
	i := 0
	for name := range stores {
		sb.WriteString(fmt.Sprintf(" storage %s: %v,", name, errs[i]))
		i++
	}
	return fmt.Errorf("all sub-storage list calls have failed: %s", sb.String())
}

func (s *multiStor) Link(ctx context.Context, objectName string) (string, error) {
	storName, objName, err := splitObjectName(objectName)
	if err != nil {
		return "", fmt.Errorf("could not split object name %s: %w", objectName, err)
	}
	stor, ok := s.stores[storName]
	if !ok {
		return "", fmt.Errorf("storage with name %s is unknown", storName)
	}
	return stor.Link(ctx, objName)
}

func (s *multiStor) Get(ctx context.Context, objectName string) (io.ReadCloser, error) {
	storName, objName, err := splitObjectName(objectName)
	if err != nil {
		return nil, fmt.Errorf("could not split object name %s: %w", objectName, err)
	}
	stor, ok := s.stores[storName]
	if !ok {
		return nil, fmt.Errorf("storage with name %s is unknown", storName)
	}
	return stor.Get(ctx, objName)
}

func (s *multiStor) Delete(ctx context.Context, objectNameCh <-chan string) <-chan error {
	out := make(chan error)
	go func() {
		defer close(out)
		// For each object received on the channel.
		for objectName := range objectNameCh {
			// Get the storage name and the trimed object name.
			storName, objName, err := splitObjectName(objectName)
			if err != nil {
				select {
				case out <- fmt.Errorf("could not split object name %s: %w", objectName, err):
				case <-ctx.Done():
					return
				}
				continue
			}

			// Get the storage using the storage name.
			stor, ok := s.stores[storName]
			if !ok {
				select {
				case out <- fmt.Errorf("storage with name %s is unknown", storName):
				case <-ctx.Done():
					return
				}
				continue
			}

			// Delete the object on the fetched storage.
			delCh := make(chan string)
			errCh := stor.Delete(ctx, delCh)
			go func() {
				select {
				case delCh <- objName:
				case <-ctx.Done():
					return
				}
				close(delCh)
			}()
			for err := range errCh {
				select {
				case out <- err:
				case <-ctx.Done():
					return
				}
			}
		}
	}()

	return out
}

func (s *multiStor) Lifecycle() (Lifecycler, error) {
	return nil, fmt.Errorf("not implemented")
}

func (s *multiStor) Upload(ctx context.Context, objectName string, in io.Reader, tags map[string]string) (int64, error) {
	storName, objName, err := splitObjectName(objectName)
	if err != nil {
		return 0, fmt.Errorf("could not split object name %s: %w", objectName, err)
	}
	stor, ok := s.stores[storName]
	if !ok {
		return 0, fmt.Errorf("storage with name %s is unknown", storName)
	}
	return stor.Upload(ctx, objName, in, tags)
}

func (s *multiStor) Merge(ctx context.Context, srcNames []string, targetName string, tags map[string]string) error {
	storName, tarName, err := splitObjectName(targetName)
	if err != nil {
		return fmt.Errorf("could not split target name %s: %w", targetName, err)
	}
	sNames := make([]string, len(srcNames))
	for i, srcName := range srcNames {
		stName, sName, err := splitObjectName(srcName)
		if err != nil {
			return fmt.Errorf("could not split source name %d (%s): %w", i, srcName, err)
		}
		if storName != stName {
			return fmt.Errorf("can only merge on single storage but got %s and %s", storName, stName)
		}
		sNames[i] = sName
	}
	stor, ok := s.stores[storName]
	if !ok {
		return fmt.Errorf("storage with name %s is unknown", storName)
	}
	return stor.Merge(ctx, sNames, tarName, tags)
}

func (s *multiStor) Tag(ctx context.Context, objectName string, tags map[string]string) error {
	storName, objName, err := splitObjectName(objectName)
	if err != nil {
		return fmt.Errorf("could not split object name %s: %w", objectName, err)
	}
	stor, ok := s.stores[storName]
	if !ok {
		return fmt.Errorf("storage with name %s is unknown", storName)
	}
	return stor.Tag(ctx, objName, tags)
}
