package protected

import "sync"

type String struct {
	sync.RWMutex
	str string
}

// NewString returns a new protected string.
func NewString(in string) *String {
	return &String{
		str: in,
	}
}

func (s *String) Get() string {
	s.RLock()
	defer s.RUnlock()
	return s.str
}

func (s *String) Set(in string) string {
	s.Lock()
	defer s.Unlock()
	s.str = in
	return in
}

func (s *String) String() string {
	return s.Get()
}
