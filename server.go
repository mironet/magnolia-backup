package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"html/template"
	"io/fs"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/semaphore"

	_ "net/http/pprof"
)

var (
	templateFiles = []string{
		"templates/list.html",
	}
	defaultPgDataPath = "/var/lib/postgresql/data" // Where postgres stores its data as a default.
	errBusy           = fmt.Errorf("too many requests, throttling")
)

// server is our HTTP endpoint for interacting with backups.
type server struct {
	router           http.Handler  // Does not require mTLS.
	secureRouter     http.Handler  // Requires mTLS.
	storage          ObjectStorage // S3, GCS, ...
	templates        map[string]*template.Template
	prefix           string              // Prefix to prepend to object names on target.
	command          string              // Command to execute to get a new backup, not executed in a shell.
	commandSem       *semaphore.Weighted // Makes sure a backup/dump process is only executed once at a time.
	args             []string            // Argument list to the command.
	cron             *cron.Cron
	cronSchedule     string
	compressionLevel int           // The level of compression to apply to uploaded objects.
	readOnly         bool          // If set this program will not take backups or upload tx logs.
	uploadReadLimit  int           // Maximum size of parts (in MB) during upload of a base backup (or transaction log).
	dumpTimeout      time.Duration // Maximum time before a dump operation times out.

	partsSweepSchedule string
	partsSweepTimeout  time.Duration
	partsSweepDelay    time.Duration

	// Use PostgreSQL WAL archiving with base backups. In this case the
	// 'command' is ignored. After a successful base backup it is uploaded to
	// object storage as a single gzipped tarball.
	usePgWAL    bool
	pgDataPath  string              // Where postgres stores its data (in case of pg_wal mode).
	pgDbName    string              // Data base name to use to connect to postgres (for pg_wal mode). TODO: really needed?
	pgUserName  string              // Username to use to connect to postgres (for pg_wal mode).
	pgPassword  string              // Password to use to connect to postgres (for pg_wal mode).
	pgHostName  string              // Host to use to connect to postgres (for pg_wal mode).
	syncDirSem  *semaphore.Weighted // Protects concurrent access to the sync dir path.
	syncDirPath string              // Path to directory to keep in sync with the bucket.
	txLogPath   string              // Path where to restore tx logs to, relative to $PGDATA.
	pgVersion   string              // The postgres version that is currently used. It is extracted before the server start from the PG_VERSION file in the pg data path.

	// Webhooks.
	webhooks []webhook // Send HTTP requests to these targets after each backup.

	// bufferedMetaResps is used to answer requests for meta files on the
	// download endpoint.
	bufferedMetaResps *metaResponseList

	// backupMetas keeps book on all the metadata of base backups this server is
	// in charge of.
	backupMetas *backupMetaList

	// Threshold on the size of txlog merged objects. If the size of a "to be
	// merged" txlog exceeds this threshold, the txlog is not merged.
	txlogSizeThreshold int64
}

// Setup our server routes and other server handling business.
func (s *server) setup() {
	ctx := context.Background()
	s.commandSem = semaphore.NewWeighted(1)

	// Setup normal mux.
	r := mux.NewRouter()
	r.HandleFunc("/", s.handleDefault())
	r.HandleFunc("/list", s.handleList())
	r.HandleFunc("/cache", s.handleCacheClearing()).Methods(http.MethodDelete)
	r.HandleFunc("/bundle/{pointInTime}", s.handleBundle()).Methods(http.MethodGet)
	r.HandleFunc("/bundle", s.handleBundle()).Methods(http.MethodGet)
	if !s.readOnly {
		r.HandleFunc("/dump", s.handleDump())
	} else {
		// If we are in read only mode, do not allow taking a fresh database
		// backup and signal it with a meaningful error message.
		r.HandleFunc("/dump", http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			http.Error(rw, "server in read only mode, cannot take a fresh backup", http.StatusForbidden)
		}))
	}
	r.Handle("/healthz", s.handleHealthz())
	r.PathPrefix("/debug/").Handler(http.DefaultServeMux)

	logger := logrus.StandardLogger().Writer()
	loggingRouter := handlers.LoggingHandler(logger, r)
	s.router = loggingRouter

	// Initialize buffered meta file response list (used in download handler).
	s.bufferedMetaResps = &metaResponseList{
		l: make([]*metaResponseBuffer, 0),
	}

	// Setup mTLS mux.
	r = mux.NewRouter()
	r.HandleFunc("/download", s.handleDownload())
	// This serves as a simple discovery mechanism.
	r.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Hello", http.StatusOK)
	})
	loggingRouter = handlers.LoggingHandler(logger, r)
	s.secureRouter = loggingRouter

	// Setup templates for the website.
	if err := s.setupTemplates(templateFiles...); err != nil {
		logrus.Fatalf("could not setup templates for the website: %v", err)
	}

	if s.usePgWAL {
		// Get all base backup meta files from the object storage.
		if !s.readOnly {
			var err error
			s.backupMetas, err = listBackupMetas(ctx, s.storage)
			if err != nil {
				logrus.Fatalf("could not get list of backup metadata: %v", err)
			}
		}

		if err := readBackupID(s.pgDataPath); err != nil {
			logrus.Fatalf("could not read backup id from %s: %v", s.pgDataPath, err)
		}
	}

	go func() {
		// Initial count after starting
		logrus.Infof("counting backups ... ")
		list, err := s.storage.List(ctx)
		if err != nil {
			logrus.Fatalf("could not get list of backups: %s", err)
		}
		logrus.Infof("counted %d backups", len(list))

		// If pg_wal is enabled check for at least one existing base backup in
		// the current view.
		if s.readOnly {
			return
		}
		if !s.usePgWAL {
			return
		}

		// Check if we have a backupID and a corresponding basebackup.
		cur := currentBackupID.Get()
		list = filterByBackupID(list, cur)
		if byULID(list).HasBaseBackup() {
			// No need to take a basebackup now, if we already have one.
			// Thus return.
			logrus.Infof("basebackup with id %s found", cur)
			return
		}
		logrus.Infof("no basebackups found, taking the first automatically now ...")
		dumpCtx, dumpCtxCancel := context.WithTimeout(ctx, s.dumpTimeout)
		defer dumpCtxCancel()

		// Wait for our turn to fetch a fresh base backup.
		info, err := s.getNewDump(dumpCtx, true)
		if err == nil {
			logrus.Infof("automatically took first base backup: %s (%s)", info.Name, ByteCountBinary(info.Size))
		}
	}()
}

const (
	backupIdFileName = ".BACKUP_ID"
)

func readBackupID(path string) error {
	path = filepath.Join(path, backupIdFileName)
	id, err := os.ReadFile(path)
	if err != nil {
		if os.IsNotExist(err) {
			cur := currentBackupID.Set(MustNewULID()) // TODO: Does this make sense?
			recordBackupID(cur)
			return nil
		}
		return err
	}
	cur := currentBackupID.Set(string(id))
	recordBackupID(cur)
	return nil
}

func writeBackupID(path string, id string) error {
	path = filepath.Join(path, backupIdFileName)
	return os.WriteFile(path, []byte(id), 0644)
}

// Run starts the server and blocks. It returns an error when the server has
// stopped. err is never nil.
func (s *server) Run(address string) error {
	logrus.Infof("starting server version %s on %s", version, address)
	srv := &http.Server{
		Addr:         address,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 15 * time.Minute,
		IdleTimeout:  120 * time.Second,
		Handler:      s.router,
	}
	return srv.ListenAndServe()
}

func metricsServerRun(address string) error {
	logrus.Infof("starting metrics server version %s on %s", version, address)
	r := mux.NewRouter()
	r.Handle("/metrics", promhttp.Handler())
	srv := &http.Server{
		Addr:         address,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 15 * time.Minute,
		IdleTimeout:  120 * time.Second,
		Handler:      r,
	}
	return srv.ListenAndServe()
}

// RunTLS starts the server in mTLS mode for certain sensitive endpoints.
func (s *server) RunMutualTLS(address, caCertFile, certFile, keyFile string) error {
	logrus.Infof("starting server version %s on %s (mTLS)", version, address)
	caCert, err := os.ReadFile(caCertFile)
	if err != nil {
		return fmt.Errorf("could not read CA cert file: %w", err)
	}
	pool := x509.NewCertPool()
	pool.AppendCertsFromPEM(caCert)
	tlsConfig := &tls.Config{
		ClientAuth:               tls.RequireAndVerifyClientCert, // Require client certificate authentication.
		ClientCAs:                pool,
		PreferServerCipherSuites: true,
		MinVersion:               tls.VersionTLS12,
	}
	srv := &http.Server{
		Addr:        address,
		TLSConfig:   tlsConfig,
		ReadTimeout: 5 * time.Second,
		//WriteTimeout: 15 * time.Minute, // This seems to be an absolute timeout and not an idle timeout.
		IdleTimeout: 120 * time.Second,
		Handler:     s.secureRouter,
	}
	return srv.ListenAndServeTLS(certFile, keyFile)
}

func (s *server) setupCron() error {
	if s.readOnly {
		return nil
	}
	c := cron.New()
	if _, err := c.AddFunc(s.cronSchedule, func() {
		ctx, cancel := context.WithTimeout(context.Background(), s.dumpTimeout)
		defer cancel()

		var info *objectInfo

		// Get a new dump with retry.
		var err error
		info, err = s.getNewDump(ctx, true)
		if err != nil {
			logrus.Errorf("cronjob: error getting new dump: %s", err)
			return
		}

		// Send off webhooks. We don't care if they work or not, also this is
		// asynchronous.
		if info != nil {
			s.sendWebhooks(context.Background(), info.Name)
		}

		logrus.Infof("successfully executed cronjob: %s", info)
	}); err != nil {
		return err
	}

	// Add a cron job for part sweeping.
	if _, err := c.AddFunc(s.partsSweepSchedule, func() {
		ctx, cancel := context.WithTimeout(context.Background(), s.partsSweepTimeout)
		defer cancel()
		if partsSweeper, ok := s.storage.(interface {
			SweepParts(context.Context, time.Duration)
		}); ok {
			partsSweeper.SweepParts(ctx, s.partsSweepDelay)
			logrus.Infof("successfully executed parts sweeper")
		} else {
			logrus.Infof("cannot sweep parts: storage is not a part sweeper")
		}
	}); err != nil {
		return err
	}

	c.Start()
	s.cron = c
	return nil
}

// Reads in templates and prepares them.
func (s *server) setupTemplates(filenames ...string) error {
	if s.templates == nil {
		s.templates = make(map[string]*template.Template)
	}

	funcMap := template.FuncMap{
		"toBinary":  ByteCountBinary,
		"toRFC3339": ObjectNameToTimeRFC3339,
	}

	for _, name := range filenames {
		basename := filepath.Base(name)
		bb, err := os.ReadFile(name)
		if err != nil {
			return err
		}
		tpl, err := template.New(basename).Funcs(funcMap).Parse(string(bb))
		if err != nil {
			return fmt.Errorf("error parsing template: %w", err)
		}
		s.templates[basename] = tpl
	}
	return nil
}

func sendOffSegments(syncDirPath string, files chan<- string) {
	// Send all segments. Ignore files that are not segments.
	sendFiles := func(path string, info fs.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		name := filepath.Base(path)
		// The archive copy operation should copy to a temp (hidden
		// dot-) file first and then move the file. A move operation
		// is atomic, but cp is not and could result in partially
		// uploaded files if the fs notify event is being acted upon
		// too early.
		if strings.HasPrefix(name, ".") {
			logrus.Tracef("don't send path %s", path)
			return nil
		}
		if isBackupHistoryFile(name) {
			logrus.Tracef("don't send backup history file %s", path)
			if time.Since(info.ModTime()) > 24*time.Hour {
				subdirectoryName := "history"
				if err := moveToOld(path, subdirectoryName); err != nil {
					logrus.Errorf("could not move %s to %s: %v", path, filepath.Join("/.old", subdirectoryName), err)
				} else {
					logrus.Infof("moved %s to %s", path, filepath.Join("/.old", subdirectoryName))
				}
			}
			return nil
		}
		if len(name) != 24 {
			logrus.Debugf("don't send file %s with name of len %d, expected 24", path, len(name))
			return nil
		}
		// TODO: Need to ignore further files?
		logrus.Tracef("sending file %s", path)
		files <- path
		return nil
	}
	expectedSubdirectories := []string{
		"lost+found",
		".old",
	}
	if err := walkDirNonRecursive(syncDirPath, sendFiles, expectedSubdirectories...); err != nil {
		logrus.Errorf("could not walk dir %s to send files: %v", syncDirPath, err)
		return
	}
}

// StartTxLogDirSync starts the directory synchronization background process.
func (s *server) StartTxLogDirSync(ctx context.Context) error {
	if s.readOnly {
		return nil
	}
	s.syncDirSem = semaphore.NewWeighted(1)
	files := make(chan string, 10) // Allow some buffering.

	go func() {
		// Use 31 instead of 30 seconds to prevent this and the below
		// go-routines from running in (almost) sync.
		ticker := time.NewTicker(31 * time.Second)
		defer ticker.Stop()
		logrus.Debugf("starting dirsync ...")
		defer logrus.Debugf("stopping dirsync ...")
		defer close(files)
		for {
			// Send off all segments that must be uploaded right now to the files channel.
			sendOffSegments(s.syncDirPath, files)

			// Use the ticker to only do the above every <ticker duration>.
			select {
			case <-ticker.C:
				continue
			case <-ctx.Done():
				return
			}
		}
	}()

	go func() {
		// Wait for this long before sending off to sync process. In the
		// meantime collect all new file create events so we can upload them to
		// the same object.
		delay := 30 * time.Second
		tick := time.NewTicker(delay)
		defer tick.Stop()
		var timerCh <-chan time.Time

		// Use a map instead of a slice to get "free" deduplication.
		finfoMap := make(map[string]finfo)
		for {
			select {
			case path := <-files:
				// Stat the file.
				info, err := os.Stat(path)
				if err != nil {
					logrus.Errorf("could not stat file: %s", err)
					continue
				}
				finfoMap[path] = finfo{
					path: path,
					info: info,
				}
				if timerCh == nil {
					// This is the first event after we sent off the last file
					// list. Start a new delay timer.
					tick.Reset(delay)
					timerCh = tick.C
				}
			case <-timerCh:
				switch s.commandSem.TryAcquire(1) {
				case true:
					func() {
						defer func() { s.commandSem.Release(1) }()

						// Fetch lifecycler.
						lifecycler, err := s.storage.Lifecycle()
						if err != nil {
							logrus.Errorf("could not get lifecycler from storage: %v", err)
							return
						}
						exp, ok := lifecycler.(ExpirationTimer)
						if !ok {
							logrus.Errorf("lifecycler not supported for txlogs: %T", lifecycler)
							return
						}

						list := make([]finfo, 0)
						for _, finfo := range finfoMap {
							list = append(list, finfo)
						}

						// Send off to sync process.
						err = s.syncTxLogDir(ctx, list, exp)
						if err != nil {
							logrus.Errorf("error syncing files: %v", err)
						} else {
							// Start txlog merge routine after syncTxLogDir
							// returned successfully.
							s.txlogMerge(ctx, currentBackupID.Get())
						}

						finfoMap = make(map[string]finfo) // Reset the list.
						// Skip timer when we have no events to prevent triggering
						// sync for nothing.
						timerCh = nil
					}()
				default:
					// In case we were busy, try again later, after the next
					// tick event fires, but allow to collect even more events
					// during that time.
				}
			case <-ctx.Done():
				return
			}
		}
	}()

	return nil
}

func isBackupHistoryFile(name string) bool {
	parts := strings.Split(name, ".")
	if len(parts) == 2 && parts[1] == "history" {
		return true
	}
	if len(parts) == 3 && parts[2] == "backup" {
		return true
	}
	return false
}
