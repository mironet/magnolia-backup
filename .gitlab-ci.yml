variables:
  GO_VERSION: "1.21"
  REPO_NAME: gitlab.com/mironet/magnolia-backup

stages:
    - test
    - build
    - release

# Execute all go tests.
test-all:
  stage: test
  image: "golang:$GO_VERSION"
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - vendor
  before_script:
    - mkdir -p $GOPATH/src/$(dirname $REPO_NAME)
    - ln -svf $CI_PROJECT_DIR $GOPATH/src/$REPO_NAME
    - cd $GOPATH/src/$REPO_NAME
    - export GO111MODULE=on
    #- export CGO_ENABLED=0
    - go mod download
    - export AZURE_STORAGE_ACCOUNT_NAME=magnoliabackuptest1
    - export AZURE_STORAGE_ACCOUNT_KEY=c3VwZXJzZWNyZXQK
  script:
    - go fmt $(go list ./... | grep -v /vendor/)
    - go vet $(go list ./... | grep -v /vendor/)
    - go test -race $(go list ./... | grep -v /vendor/) -v -coverprofile .testCoverage.txt

# Build app.
build-app:
  stage: build
  image: docker:19
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - vendor
  before_script:
    - apk add --no-cache git
    - git --version
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - export GIT_TAG=$(git describe --always)
    - export DOCKER_BUILDKIT=1
  script:
    - echo $GIT_TAG
    - docker build --pull --build-arg GO_VERSION=$GO_VERSION --build-arg APP_VERSION=$GIT_TAG -t "$CI_REGISTRY_IMAGE" -f docker/Dockerfile .
    - docker push "$CI_REGISTRY_IMAGE"

# Build app.
build-tagged-app:
  stage: build
  image: docker:19
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - vendor
  before_script:
    - apk add --no-cache git
    - git --version
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - export GIT_TAG=$(git describe --always)
    - export DOCKER_BUILDKIT=1
  script:
    - echo $GIT_TAG
    - docker build --pull --build-arg GO_VERSION=$GO_VERSION --build-arg APP_VERSION=$GIT_TAG -t "$CI_REGISTRY_IMAGE:$GIT_TAG" -f docker/Dockerfile .
    - docker push "$CI_REGISTRY_IMAGE:$GIT_TAG"
    - docker tag "$CI_REGISTRY_IMAGE:$GIT_TAG" "$CI_REGISTRY_IMAGE:latest"
    - docker push "$CI_REGISTRY_IMAGE:latest"
  only:
  - /v[0-9|\.]+-?[0-9A-Za-z-]*/
  except:
    - branches

# Manually build prerelease version of app.
build-app-manually:
  stage: build
  image: docker:19
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - vendor
  before_script:
    - apk add --no-cache git
    - git --version
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - export GIT_TAG=$(git describe --always)
    - export DOCKER_BUILDKIT=1
  script:
    - echo $GIT_TAG
    - docker build --pull --build-arg GO_VERSION=$GO_VERSION --build-arg APP_VERSION=$GIT_TAG -t "$CI_REGISTRY_IMAGE:$GIT_TAG" -f docker/Dockerfile .
    - docker push "$CI_REGISTRY_IMAGE:$GIT_TAG"
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^prerelease\/.+/'
      when: manual
      allow_failure: true
      
build-app-stable:
  stage: build
  image: docker:19
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - vendor
  before_script:
    - apk add --no-cache git
    - git --version
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - export GIT_TAG=$(git describe --always)
    - export DOCKER_BUILDKIT=1
  script:
    - echo $GIT_TAG
    - docker build --pull --build-arg GO_VERSION=$GO_VERSION --build-arg APP_VERSION=$GIT_TAG -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH" -f docker/Dockerfile .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_BRANCH"
  rules:
    - if: $CI_COMMIT_BRANCH =~ /v[0-9]+\.[0-9]+-[0-9A-Za-z-]+/