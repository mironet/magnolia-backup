package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/magnolia-backup/api"
)

type webhook interface {
	Send(ctx context.Context, objectName string) error
}

type gitlabWebhook struct {
	cli        *api.Client
	name       string
	target     *url.URL
	bucketName string
}

const (
	gitlabTpl = "%s/api/v4/projects/%s/ref/%s/trigger/pipeline"
)

func newGitlabWebhook(name string, endpoint, project, ref, token, bucketName string, vars map[string]string) (*gitlabWebhook, error) {
	endpoint = strings.TrimRight(endpoint, "/")
	project, ref = url.PathEscape(project), url.PathEscape(ref)
	u, err := url.Parse(fmt.Sprintf(gitlabTpl, endpoint, project, ref))
	if err != nil {
		return nil, fmt.Errorf("error creating webhook: %w", err)
	}
	params := u.Query()
	params.Set("token", token)

	vars = gitlabVariablesParam(vars)
	for k, v := range vars {
		params.Set(k, v)
	}

	u.RawQuery = params.Encode()
	var verbosity int
	switch logrus.GetLevel() {
	case logrus.TraceLevel:
		verbosity = api.ModeTrace
	case logrus.DebugLevel:
		verbosity = api.ModeDebug
	default:
		verbosity = api.ModeInfo
	}
	cli, err := api.New(api.WithVerbosity(verbosity))
	if err != nil {
		return nil, err
	}
	g := &gitlabWebhook{
		cli:        cli,
		name:       name,
		target:     u,
		bucketName: bucketName,
	}
	return g, nil
}

func (g *gitlabWebhook) String() string {
	return g.name
}

// Send off gitlab webhook.
func (g *gitlabWebhook) Send(ctx context.Context, objectName string) error {
	// Mangle parameter values.
	params := g.target.Query()
	for k := range params {
		v := params.Get(k)
		if strings.Contains(v, "%b") || strings.Contains(v, "%f") {
			replacer := strings.NewReplacer(
				"%f", objectName,
				"%b", g.bucketName,
			)
			v = replacer.Replace(v)
			params.Set(k, v)
		}
	}
	g.target.RawQuery = params.Encode()
	req, err := g.cli.NewRequest(ctx, http.MethodPost, g.target.String(), nil)
	if err != nil {
		return fmt.Errorf("%s: error creating gitlab request: %w", g.name, err)
	}
	if _, err := g.cli.Do(ctx, req, nil); err != nil {
		return fmt.Errorf("%s: error sending gitlab request: %w", g.name, err)
	}
	return nil
}
