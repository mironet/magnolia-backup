package tags

import "strings"

// See https://prometheus.io/docs/prometheus/latest/querying/basics/ for further
// syntax explanations.
const (
	// Symbols.
	startBracket      = '{'
	endBracket        = '}'
	equal             = '='
	negate            = '!'
	regex             = '~'
	underline         = '_'
	hyphen            = '-'
	quote             = '"'
	backslash    rune = 92
	comma             = ','
)

var (
	// Only ASCII uppercase chars.
	alphaUpper = stringFromRange(0x41, 0x5a)
	// Only ASCII lowercase chars.
	alphaLower = stringFromRange(0x61, 0x7a)
	// Uppercase and lowercase combined.
	alpha = alphaUpper + alphaLower
	// Digits.
	digit = stringFromRanges(0x30, 0x39)
	// Valid chars for a tag name.
	tagName = alpha + digit + string([]rune{underline, hyphen})
)

func stringFromRange(start, stop int) string {
	n := stop - start
	if n < 0 {
		return ""
	}
	out := make([]rune, n+1)
	for i := 0; i <= n; i++ {
		out[i] = rune(i + start)
	}
	return string(out)
}

func stringFromRanges(codepoints ...int) string {
	if len(codepoints) < 2 || len(codepoints)%2 != 0 {
		panic("even number of range codepoints needed")
	}
	var buf strings.Builder
	for i := 0; i < len(codepoints); i = i + 2 {
		buf.WriteString(stringFromRange(codepoints[i], codepoints[i+1]))
	}
	return buf.String()
}
