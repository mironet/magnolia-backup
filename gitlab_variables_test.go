package main

import (
	"reflect"
	"testing"
)

func Test_gitlabVariablesParam(t *testing.T) {
	type args struct {
		vars map[string]string
	}
	tests := []struct {
		name string
		args args
		want map[string]string
	}{
		{
			name: "trigger variables",
			args: args{
				vars: map[string]string{
					"a":    "b",
					"a2":   "b2",
					"key":  "value",
					"key2": "value2",
				},
			},
			want: map[string]string{
				"variables[a]":    "b",
				"variables[a2]":   "b2",
				"variables[key]":  "value",
				"variables[key2]": "value2",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := gitlabVariablesParam(tt.args.vars); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("gitlabVariablesParam() = %v, want %v", got, tt.want)
			}
		})
	}
}
