package main

import (
	"sync"
)

// ListCache caches list operations for all object storages because that's their
// weak point.
type ListCache struct {
	sync.Mutex
	list       []*backupItem
	bundleList []*backupItem
}

func (c *ListCache) Update(list []*backupItem) {
	c.Lock()
	defer c.Unlock()
	c.list = list
}

func (c *ListCache) Get() []*backupItem {
	c.Lock()
	defer c.Unlock()
	return copyBackupItems(c.list)
}

func (c *ListCache) Clear() {
	c.Lock()
	defer c.Unlock()
	c.list = nil
	c.bundleList = nil
}

func (c *ListCache) UpdateBundles(bundleList []*backupItem) {
	c.Lock()
	defer c.Unlock()
	// Note: A nil bundle list cache and a bundle list cache with zero length
	// have different logical implications. A nil bundle list cache means that
	// there is no cache yet (i.e. since the last call to Clear() no update
	// method was called). Whereas a bundle list cache with zero length means
	// that there exists a cached list, but it is empty. That is what we want
	// here.
	if bundleList == nil {
		c.bundleList = make([]*backupItem, 0)
	}
	c.bundleList = bundleList
}

func (c *ListCache) GetBundles() []*backupItem {
	c.Lock()
	defer c.Unlock()
	return copyBackupItems(c.bundleList)
}
