package main

import (
	"archive/tar"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/klauspost/compress/gzip"
	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/magnolia-backup/api"
	"gitlab.com/mironet/magnolia-backup/archive"
	"gitlab.com/mironet/magnolia-backup/internal/walparser"
)

// bootProcess holds the current state of the boot process.
type bootProcess struct {
	Err error // Last error occurred.

	ctx            context.Context // If we cancel this, we're done.
	hostname       string          // Hostname override if not empty.
	cli            *api.Client     // Client used to sync from other instances.
	markerFilePath string          // Path to marker file used to check if we need to bootstrap or not.
	serviceName    string          // K8s service name.
	dataDir        string          // Target data dir in case we sync content from another public node.
	dataSourceURL  *url.URL        // URL to metadata file to fetch remote (backup) data for restoration.
	fileName       string
	endpoint       string
	stor           ObjectStorage
	meta           *PointInTimeRestore
	walPathList    []string
}

func (b bootProcess) Error() string {
	return b.Err.Error()
}

// Part of our FSM.
type bootStateFn func(*bootProcess) bootStateFn

func stateInit(b *bootProcess) bootStateFn {
	// If we are doing a content sync from another instance, we need to ...
	if b.dataSourceURL == nil {
		// ... first check if our pod is the first pod in the deployment.
		hosts, err := discoverEndpoints(b.ctx, b.serviceName)
		if err != nil || len(hosts) == 0 {
			if err != nil {
				logrus.Infof("could not discover endpoints: %v", err)
			}
			// We are the first pod in the StatefulSet. Since we probably have no
			// other public instance to sync from and no static data source, let's
			// just start normally.
			logrus.Infof("we are the first pod in the deployment, not syncing anything ...")
			return nil
		}
	}

	// Check if this is a fresh boot or just a restart.
	_, err := os.Stat(b.markerFilePath)
	switch {
	case err != nil && os.IsNotExist(err):
		// The marker file does not exist. Now check if the target directory is
		// empty.
		empty, err := isEmpty(b.dataDir)
		if err != nil && os.IsNotExist(err) {
			// Target data dir does not exist, create dir recursively and
			// continue.
			if err := os.MkdirAll(b.dataDir, 0700); err != nil {
				b.Err = fmt.Errorf("could not create data dir: %w", err)
				return nil
			}
			return stateFetchBackup
		}
		if err != nil {
			b.Err = fmt.Errorf("error while checking if target dir is empty: %w", err)
			return nil
		}
		if !empty {
			// We are done here as a safety measure.
			logrus.Warnf("target dir %s not empty, not syncing", b.dataDir)
			return nil
		}
		// In this case we start syncing from any good public instance we can
		// find.
		return stateFetchBackup
	case err != nil:
		// Another error happened.
		b.Err = fmt.Errorf("could not stat marker file: %w", err)
		return nil
	default:
		// Normal boot, nothing to do.
		logrus.Infof("marker file %s present, not syncing", b.markerFilePath)
		return nil
	}
}

// Decide where to fetch the backup from.
func stateFetchBackup(b *bootProcess) bootStateFn {
	if b.dataSourceURL == nil {
		// Try to get the backup from another instance via k8s Service
		// definitions.
		return stateFetchBackupK8s(b)
	}
	return stateFetchBackupPITR
}

// Fetch backups from point in time recovery metadata.
func stateFetchBackupPITR(b *bootProcess) bootStateFn {
	// Fetch the metadata file.
	cli := &http.Client{
		Timeout: 0, // Allow for a long time for the download.
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout:   180 * time.Second,
				KeepAlive: 180 * time.Second,
			}).Dial,
			TLSHandshakeTimeout:   30 * time.Second,
			ResponseHeaderTimeout: 30 * time.Second,
			ExpectContinueTimeout: time.Second,
		},
	}
	logrus.Infof("fetching point in time recovery metadata ...")
	req, err := http.NewRequestWithContext(b.ctx, http.MethodGet, b.dataSourceURL.String(), nil)
	if err != nil {
		b.Err = err
		return nil
	}
	var meta PointInTimeRestore
	resp, err := cli.Do(req)
	if err != nil {
		b.Err = fmt.Errorf("error requesting pitr metadata: %w", err)
		return nil
	}
	defer resp.Body.Close()
	if resp.StatusCode < 200 || resp.StatusCode > 399 {
		res, err := httputil.DumpResponse(resp, true)
		if err != nil {
			b.Err = fmt.Errorf("error while dumping response: %w", err)
			return nil
		}
		b.Err = fmt.Errorf("error requesting pitr metadata, dumping response: \n%s", string(res))
		return nil
	}
	if err := json.NewDecoder(resp.Body).Decode(&meta); err != nil {
		b.Err = fmt.Errorf("error parsing pitr metadata: %w", err)
		return nil
	}
	b.meta = &meta

	// Now loop through the list and restore to the desired point in time.
	r, w := io.Pipe()
	go b.downloadBackupBundle(cli, w)
	return stateExtractBackup(r)
}

func (b *bootProcess) extractPostgresVersion() (int, error) {
	// Read PG_VERSION file content.
	bytes, err := os.ReadFile(path.Join(b.dataDir, pgVersionFileName))
	if err != nil {
		return 0, fmt.Errorf("could not read pg version file: %w", err)
	}
	str := string(bytes)
	str = strings.TrimSuffix(str, "\n")
	return strconv.Atoi(str)
}

func (b *bootProcess) downloadBackupBundle(cli *http.Client, w *io.PipeWriter) {
	defer w.Close()
	gzw := gzip.NewWriter(w)
	defer gzw.Close()
	tw := tar.NewWriter(gzw)
	defer tw.Close()

	if b.meta == nil {
		w.CloseWithError(fmt.Errorf("PITR metadata pointer cannot be nil"))
		return
	}

	// Copy each file in the list, extract its contents and forward
	logrus.Infof("downloading backup bundle for point in time %s ...", b.meta.PointInTime)
	b.walPathList = make([]string, 0)
	for _, v := range b.meta.BackupList {
		hasRecovered := false
		if err := func() error {
			req, err := http.NewRequestWithContext(b.ctx, http.MethodGet, v.Link.String(), nil)
			if err != nil {
				return err
			}
			resp, err := cli.Do(req)
			var readCloser io.ReadCloser
			readCloser = resp.Body
			if err != nil {
				return err
			}
			if resp.StatusCode == http.StatusNotFound && b.stor != nil {
				logrus.Infof("trying to recover from txlog not found error for item %s", v.Key)
				additionalInfoStr := "during txlog not found recovery"
				// Try to recover from a not found error caused by tx log merge.
				missingTxlogTime, err := v.TimeFromUlidKey()
				if err != nil {
					return fmt.Errorf("could not find time from ulid key %s: %w", additionalInfoStr, err)
				}
				list, err := b.stor.List(b.ctx, WithPrefix{path.Dir(v.Key)})
				if err != nil {
					return fmt.Errorf("could not list object storage items with prefix %s %s: %w", path.Dir(v.Key), additionalInfoStr, err)
				}
				// Compare timestamps.
				sort.Sort(byULID(list))
				for i := len(list) - 1; i >= 0; i-- {
					item := list[i]
					if item == nil {
						continue
					}
					t, err := item.TimeFromUlidKey()
					if err != nil {
						return fmt.Errorf("could not find time from ulid key %s: %w", additionalInfoStr, err)
					}
					if t.After(missingTxlogTime) {
						continue
					}
					// Fetch the newest txlog item which has a timestamp
					// that is not after the missing txlog's timestamp.
					// It must be the txlog the missing txlog has been
					// merged into. This is a logical consequence of how
					// the txlog merge algorithm works.
					readCloser, err = b.stor.Get(b.ctx, item.Key)
					if err != nil {
						return fmt.Errorf("could not get item %s %s: %w", item.Key, additionalInfoStr, err)
					}
					// Make sure that we copy this item's content to the
					// writer and exit the loop afterwards.
					hasRecovered = true
					logrus.Infof("can recover from txlog not found error using item %s", item.Key)
					break
				}
				if !hasRecovered {
					return fmt.Errorf("could not recover from txlog not found error: no suitable item found in list %s", list.String())
				}
			}
			defer readCloser.Close()
			if resp.StatusCode >= 400 && resp.StatusCode != http.StatusNotFound {
				bytes, _ := io.ReadAll(readCloser)
				return fmt.Errorf("could not fetch from requested link: received response %s: %s", resp.Status, string(bytes))
			}

			gz, err := gzip.NewReader(readCloser)
			if err != nil {
				return err
			}

			defer gz.Close()
			tr := tar.NewReader(gz)
			for {
				hdr, err := tr.Next()
				if err == io.EOF {
					break // End of archive.
				}
				if err != nil {
					return err
				}
				if v.IsTxLog() {
					// Fixup the path so we make sure the tx log files do not
					// land in the root directory.
					hdr.Name = path.Join("archive", hdr.Name)
					b.walPathList = append(b.walPathList, hdr.Name)
				}
				if err := tw.WriteHeader(hdr); err != nil {
					return err
				}
				if _, err := io.Copy(tw, tr); err != nil {
					return err
				}
			}
			return nil
		}(); err != nil {
			logrus.Error(err) // To ensure the error is logged whatever is done on the read half of the pipe.
			w.CloseWithError(err)
			return
		}

		// If the txlog not found recovery has been executed, we can break
		// the loop here. A recovered txlog must be the last relevant txlog
		// in the backup item list. This is a logical consequence of how the
		// txlog merge algorithm works.
		if hasRecovered {
			break
		}
	}
}

func stateAddRecoveryConfig(b *bootProcess) bootStateFn {
	if b.meta == nil {
		b.Err = fmt.Errorf("stateAddRecoveryConfig: PITR metadata pointer cannot be nil")
		return nil
	}

	postgresVersion, err := b.extractPostgresVersion()
	if err != nil {
		logrus.Infof("could not determine backup's postgres version from PG_VERSION file: %v", err)
		logrus.Infof("assuming backup's postgres version is 11")
		postgresVersion = 11
	}
	logrus.Infof("backup's postgres version is %d", postgresVersion)

	// Now add the recovery file if a point in time is requested while
	// pulling through the tar file.
	formattedTime := b.meta.PointInTime.In(time.UTC).Format(pointInTimeFormat)
	recTempl := recoveryTempl
	if postgresVersion >= 13 {
		latestCommittedTx, err := b.extractLastCommittedTxTime()
		if err != nil {
			b.Err = err
			return nil
		}
		recTempl = pickRecoveryTemplate(b.meta.PointInTime, latestCommittedTx)
	}
	templ := template.Must(template.New("restore").Parse(recTempl))
	data := struct {
		FormattedTime string
	}{
		FormattedTime: formattedTime,
	}
	var fileBuffer bytes.Buffer
	if err := templ.Execute(&fileBuffer, data); err != nil {
		b.Err = err
		return nil
	}
	var fileName string
	// Recovery works different for older postgres versions.
	if postgresVersion <= 11 {
		// Now add the recovery file if a point in time is requested while
		// pulling through the tar file.
		logrus.Infof("generating recovery.conf file ...")
		fileName = "recovery.conf"
	} else {
		logrus.Infof("generating postgresql.auto.conf file ...")
		fileName = "postgresql.auto.conf"
	}
	if err := os.WriteFile(filepath.Join(b.dataDir, fileName), fileBuffer.Bytes(), 0600); err != nil {
		b.Err = err
		return nil
	}

	// For newer postgres versions.
	if postgresVersion > 11 {
		// Also add the recovery.signal file. It can be empty.
		logrus.Infof("adding recovery.signal file ...")
		if err := os.WriteFile(filepath.Join(b.dataDir, "recovery.signal"), make([]byte, 0), 0600); err != nil {
			b.Err = err
			return nil
		}
	}

	// We are done here.
	return nil
}

func (b *bootProcess) extractLastCommittedTxTime() (time.Time, error) {
	logrus.Info("start extracting last committed transaction time")

	// Work on a copy of the string slice (just to be sure).
	walPathList := make([]string, len(b.walPathList))
	copy(walPathList, b.walPathList)

	// Find out which is the last wal segment by sorting the wal paths
	// alphabetically and looping in reverse order through the list.
	sort.Strings(walPathList)
	for i := len(walPathList) - 1; i >= 0; i-- {
		walPath := filepath.Join(b.dataDir, walPathList[i])
		logrus.Infof("analyzing wal segment %s", walPath)

		walFile, err := os.Open(walPath)
		if err != nil {
			return time.Time{}, fmt.Errorf("could not open wal segment %s: %w", walPath, err)
		}
		t, err := lastCommittedTxTime(walFile)
		if err == nil && t.IsZero() {
			// If no committed transaction was found in the wal segment, try the
			// next wal segment.
			logrus.Info("no committed transaction found in wal segment: try next wal segment")
			continue
		}
		if err == nil {
			logrus.Infof("found last committed transaction time %s", t.String())
		}
		return t, err
	}
	logrus.Infof("did not find committed transaction in wal segments")
	return time.Time{}, nil
}

const (
	walPageSize                  = 8 * 1024
	transactionResourceManagerID = 1
	opCodeMask                   = 16 * 7 // https://doxygen.postgresql.org/xact_8h_source.html#l00178
	opCodeCommit                 = 0      // https://doxygen.postgresql.org/xact_8h_source.html#l00169
)

func lastCommittedTxTime(file *os.File) (time.Time, error) {
	walp := walparser.NewWalParser()
	record := 0
	pageNumber := 0
	lastT := time.Time{}
	pageBytes := make([]byte, walPageSize)
	for {
		pageNumber = pageNumber + 1
		n, err := io.ReadFull(file, pageBytes)
		if err != nil {
			if !errors.Is(err, io.EOF) {
				switch {
				case errors.Is(err, io.ErrUnexpectedEOF):
					logrus.Errorf("unexpected end of file on page number %d: could read only %d of %d bytes", pageNumber, n, walPageSize)
				default:
					logrus.Errorf("could not read page number %d: %v", pageNumber, err)
				}
			}
			break
		}
		_, recs, err := walp.ParseRecordsFromPage(bytes.NewReader(pageBytes))
		if err != nil && !errors.As(err, &walparser.ZeroPageError{}) {
			logrus.Warnf("could not parse records from page %d: %v", pageNumber, err)
			// Note: In some cases the walparser cannot recover from an error
			// during page parsing. Thus we have to recreate it.
			walp = walparser.NewWalParser()
			continue
		}
		for _, rec := range recs {
			record = record + 1
			if rec.Header.ResourceManagerID == transactionResourceManagerID && // Resource manager with id==1 is the "Transaction" resource manager: https://github.com/postgres/postgres/blob/master/src/include/access/rmgrlist.h#L29
				rec.Header.Info&opCodeMask == opCodeCommit { // Check the header info to find whether we are looking at a commit record: https://github.com/postgres/postgres/blob/master/src/backend/access/rmgrdesc/xactdesc.c#L488
				if len(rec.MainData) < 8 {
					logrus.Errorf("record %d: main data must have at least len 8, but has %d", record, len(rec.MainData))
					continue
				}

				// Some useful references for the following code block:
				//
				// https://stackoverflow.com/questions/65143816/how-does-postgresql-store-datetime-types-internally
				// https://github.com/postgres/postgres/blob/master/src/backend/access/transam/commit_ts.c#L274
				// https://github.com/postgres/postgres/blob/master/src/backend/utils/adt/timestamp.c#L1654
				// https://doxygen.postgresql.org/backend_2utils_2adt_2timestamp_8c_source.html#l01654
				//
				// The first 8 bytes of the main data of a commit record contain
				// the commit timestamp. The commit timestamp is a
				// (little-endian) 64-bit integer representing the number of
				// microseconds since 2000-01-01 00:00:00.

				microSecs := int64(0)
				for i, b := range rec.MainData {
					if i >= 8 {
						break
					}
					add := int64(b) * (1 << (8 * int64(i)))
					microSecs = microSecs + add
				}
				t := time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC).Add(time.Duration(microSecs) * time.Microsecond)
				logrus.Debugf("record %d is COMMIT %s", record, t)
				lastT = t
			}
		}
	}
	return lastT, nil
}

// The time format we want to recover to, should be UTC.
const pointInTimeFormat = "2006-01-02 15:04:05"

// Template for the recovery.conf (or postgresql.auto.conf for postgres >=12)
// file used by PITR.
const recoveryTempl = `restore_command = 'cp archive/%f "%p"'
recovery_target_time = '{{ .FormattedTime }}'
recovery_target_action = 'promote'
`

// Template for PITR used for recoveries with postgres >=13. This template is
// only used when the requested point in time is after the time when the last
// wal segment was modified (see func pickRevoceryTemplate).
const recoveryTemplNoRecoveryTargetTime = `restore_command = 'cp archive/%f "%p"'
recovery_target_action = 'promote'
`

// For postgres >=13 we cannot always specify the recovery target time because
// we'd risk the recovery to fail with a fatal error. Refer to:
// https://www.postgresql.org/docs/13/release-13.html#id-1.11.6.15.4:~:text=Generate%20an%20error%20if%20recovery%20does%20not%20reach%20the%20specified%20recovery%20target%20(Leif%20Gunnar%20Erlandsen%2C%20Peter%20Eisentraut)
func pickRecoveryTemplate(recoveryPIT, latestCommittedTx time.Time) string {
	// If the point in time to recover to is after the latest committed
	// transaction time ...
	if recoveryPIT.After(latestCommittedTx) {
		// ... we want to use the special template.
		logrus.Infof(
			"recovery point in time %s is after the latest committed transaction time %s: use template without recovery_target_time",
			recoveryPIT.String(), latestCommittedTx.String(),
		)
		return recoveryTemplNoRecoveryTargetTime
	}
	// Otherwise we want to use the default template.
	logrus.Infof(
		"recovery point in time %s is not after the latest committed transaction time %s: use template with recovery_target_time",
		recoveryPIT.String(), latestCommittedTx.String(),
	)
	return recoveryTempl
}

// Fetch a backup inside a k8s environment, from another instance in this case.
func stateFetchBackupK8s(b *bootProcess) bootStateFn {
	hosts, err := discoverEndpoints(b.ctx, b.serviceName)
	if err != nil {
		b.Err = err
		return nil
	}
	logrus.Debugf("discovered endpoints: %s", strings.Join(hosts, ", "))
	if b.cli == nil {
		b.Err = fmt.Errorf("api client cannot be nil")
		return nil
	}
	b.endpoint, err = findQuickestEndpoint(b.ctx, hosts, b.cli, "/hello")
	if err != nil {
		b.Err = err
		return nil
	}
	// Try to get a fresh base backup.
	path := fmt.Sprintf("https://%s/download", b.endpoint)
	req, err := b.cli.NewRequest(b.ctx, http.MethodGet, path, nil)
	if err != nil {
		b.Err = err
		return nil
	}
	r, w := io.Pipe()

	go func() {
		defer w.Close()
		logrus.Infof("requesting content from %s", path)
		resp, err := b.cli.Do(b.ctx, req, w)
		if err != nil {
			w.CloseWithError(err)
			return
		}
		b.fileName, b.Err = fileName(resp.Header)
	}()
	return stateExtractBackup(r)
}

func stateFetchBackupMetaK8s() bootStateFn {
	return func(b *bootProcess) bootStateFn {
		if b.cli == nil {
			b.Err = fmt.Errorf("api client cannot be nil")
			return nil
		}
		// Try to get a fresh base backup.
		path := fmt.Sprintf("https://%s/download", b.endpoint)
		req, err := b.cli.NewRequest(b.ctx, http.MethodGet, path, nil)
		// Add query to path.
		if b.fileName == "" {
			return nil // Stop here because file name cannot be empty at this point.
		}
		query := make(url.Values)
		query.Add("meta", backupIDFromFileName(b.fileName))
		req.URL.RawQuery = query.Encode()

		if err != nil {
			b.Err = err
			return nil
		}
		r, w := io.Pipe()
		go func() {
			defer w.Close()
			logrus.Infof("requesting content from %s", path)
			_, err := b.cli.Do(b.ctx, req, w)
			if err != nil {
				w.CloseWithError(err)
			}
		}()
		return stateExtractBackupMeta(r)
	}
}

func stateExtractBackup(r io.Reader) bootStateFn {
	return func(b *bootProcess) bootStateFn {
		// Extract the source into the data dir.
		startTime := time.Now()
		pr := newProgressReader(r, func(n int64) {
			rate := rateString(time.Since(startTime), n)
			logrus.Infof("synced %s (%s) ...", ByteCountBinary(n), rate)
		})

		// Start untaring the backup.
		if err := archive.Untar(pr, b.dataDir, nil); err != nil {
			b.Err = err
			return nil
		}
		// Create (touch) the marker file.
		_, err := os.Stat(b.markerFilePath)
		switch {
		case err != nil && os.IsNotExist(err):
			if _, err := os.Create(b.markerFilePath); err != nil {
				b.Err = fmt.Errorf("could not create marker file: %w", err)
				return nil
			}
			logrus.Infof("created marker file %s", b.markerFilePath)
		case err != nil:
			// Another error happened while stat-ing.
			b.Err = fmt.Errorf("could not stat marker file: %w", err)
			return nil
		default:
			logrus.Infof("marker file %s present after syncing, not touching", b.markerFilePath)
		}
		// If we are doing a PITR restore ...
		if b.dataSourceURL != nil {
			// ... we also need to add the recovery config.
			return stateAddRecoveryConfig(b)
		}
		return stateFetchBackupMetaK8s()
	}
}

func stateExtractBackupMeta(r io.Reader) bootStateFn {
	return func(b *bootProcess) bootStateFn {
		// Extract the source into the data dir.
		startTime := time.Now()
		pr := newProgressReader(r, func(n int64) {
			rate := rateString(time.Since(startTime), n)
			logrus.Infof("synced %s (%s) ...", ByteCountBinary(n), rate)
		})
		if err := archive.Untar(pr, b.dataDir, nil); err != nil {
			b.Err = err
		}
		return nil // We're done here.
	}
}

var (
	errNoResponse = fmt.Errorf("no response from any target")
)

// findQuickestEndpoint issues GET requests to a target with the client a few
// times and reports back the addr with the fastest response time. It
// automatically assumes https:// for the scheme and expects addresses in the
// form of 'host:port' or just 'host'.
func findQuickestEndpoint(ctx context.Context, addrs []string, cli *api.Client, target string) (string, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	var max = 10
	var wg sync.WaitGroup

	work := make(chan string)
	results := make(chan string)
	errc := make(chan error, max)

	for i := 0; i < max; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			cli, err := cli.Clone()
			if err != nil {
				errc <- err
				return
			}
			for addr := range work {
				logrus.Debugf("checking if %s is responding", addr)
				req, err := cli.NewRequest(ctx, http.MethodGet, target, nil, api.WithBaseURL("https://"+addr))
				if err != nil {
					errc <- err
					return
				}
				if _, err := cli.Do(ctx, req, nil); err != nil {
					// We just ignore this since there are potentially other
					// requests which are successful.
					logrus.Debugf("error while checking response time of endpoint %s: %s", addr, err)
					continue
				}

				select {
				case results <- addr:
				case <-ctx.Done():
					return
				}
			}
		}()
	}

	go func() {
		defer close(work)
		for _, v := range addrs {
			select {
			case work <- v:
			case <-ctx.Done():
				errc <- ctx.Err()
				return
			}
		}
	}()

	go func() {
		// We do not close results ever, but do close errc. This signals no
		// target has been contacted successfully.
		defer close(errc)
		wg.Wait()
	}()

	select {
	case result := <-results:
		return result, nil
	case err := <-errc:
		if err != nil {
			return "", err
		}
		return "", errNoResponse
	}
}

// discoverEndpoints expects a k8s headless service name and tries to discover
// the DNS names of all pods in that service for a specific (DNS) service name.
func discoverEndpoints(ctx context.Context, serviceName string) ([]string, error) {
	return discoverEndpointsWithResolver(ctx, serviceName, net.DefaultResolver)
}

const (
	contentSyncDNSServiceName  = "contentsync"
	contentSyncDNSServiceProto = "tcp"
)

type srvResolver interface {
	LookupSRV(ctx context.Context, service, proto, name string) (cname string, addrs []*net.SRV, err error)
}

func discoverEndpointsWithResolver(ctx context.Context, serviceName string, resolver srvResolver) ([]string, error) {
	var out []string
	// First find all A records of the service name.
	_, addrs, err := resolver.LookupSRV(ctx, contentSyncDNSServiceName, contentSyncDNSServiceProto, serviceName)
	if err != nil {
		return nil, err
	}
	for _, v := range addrs {
		name := fmt.Sprintf("%s:%d", strings.TrimSuffix(v.Target, "."), v.Port)
		out = append(out, name)
	}
	sort.Strings(out)
	return out, nil
}
