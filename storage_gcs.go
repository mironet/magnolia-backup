package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"github.com/sirupsen/logrus"
	"golang.org/x/exp/slices"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/iterator"
)

type gcsStorConfig struct {
	Prefix       string `yaml:"prefix,omitempty"`
	Bucket       string `yaml:"bucket,omitempty"`
	ProjectID    string `yaml:"projectID,omitempty"`
	Location     string `yaml:"location,omitempty"`
	LocationType string `yaml:"locationType,omitempty"`
}

func objectStorageConfigFromGCS(conf gcsStorConfig, readonly bool) ObjectStorageConfig {
	return ObjectStorageConfig{
		prefix:     conf.Prefix,
		bucketName: conf.Bucket,
		readonly:   readonly,
		gcs: GcsConfig{
			enabled:      true,
			projectID:    conf.ProjectID,
			location:     conf.Location,
			locationType: conf.LocationType,
		},
	}
}

func (first *gcsStorConfig) merge(name string, second *gcsStorConfig) {
	if second.Bucket != "" {
		if first.Bucket != "" {
			logrus.Debugf("overwriting bucket %s with %s in storage %s", first.Bucket, second.Bucket, name)
		}
		first.Bucket = second.Bucket
	}
	if second.Location != "" {
		if first.Location != "" {
			logrus.Debugf("overwriting location %s with %s in storage %s", first.Location, second.Location, name)
		}
		first.Location = second.Location
	}
	if second.LocationType != "" {
		if first.LocationType != "" {
			logrus.Debugf("overwriting location type %s with %s in storage %s", first.LocationType, second.LocationType, name)
		}
		first.LocationType = second.LocationType
	}
	if second.Prefix != "" {
		if first.Prefix != "" {
			logrus.Debugf("overwriting prefix %s with %s in storage %s", first.Prefix, second.Prefix, name)
		}
		first.Prefix = second.Prefix
	}
	if second.ProjectID != "" {
		if first.ProjectID != "" {
			logrus.Debugf("overwriting project id %s with %s in storage %s", first.ProjectID, second.ProjectID, name)
		}
		first.ProjectID = second.ProjectID
	}
}

type gcsStorage struct {
	cli          *storage.Client
	projectID    string
	bucketName   string
	prefix       string
	lifecycle    Lifecycler
	location     string // GCS location, see: https://cloud.google.com/storage/docs/locations#location-mr
	locationType string // "region", "multi-region", "dual-region", see: https://pkg.go.dev/cloud.google.com/go/storage?tab=doc#BucketAttrs
}

// gcsStorageOpt is an option for configuring GCS storage.
type gcsStorageOpt func(*gcsStorage) error

// withGCSPrefix sets a prefix for objects.
func withGCSPrefix(prefix string) gcsStorageOpt {
	return func(u *gcsStorage) error {
		u.prefix = prefix
		return nil
	}
}

// withBucketName sets a bucket name for this storage.
func withBucketName(bucket string) gcsStorageOpt {
	return func(u *gcsStorage) error {
		u.bucketName = bucket
		return nil
	}
}

// withGCSProjectID sets the Google project ID.
func withGCSProjectID(projectID string) gcsStorageOpt {
	return func(u *gcsStorage) error {
		u.projectID = projectID
		return nil
	}
}

var (
	validGCSRegionTypes = []string{
		"multi-region",
		"dual-region",
		"region",
	}
)

func withGCSLocation(location, typ string) gcsStorageOpt {
	return func(u *gcsStorage) error {
		u.location = location
		if !slices.Contains(validGCSRegionTypes, typ) {
			return fmt.Errorf("region type %s is not one of %s", typ, strings.Join(validGCSRegionTypes, ", "))
		}
		u.locationType = typ
		return nil
	}
}

func (u *gcsStorage) Close() error {
	return u.cli.Close()
}

// newGCSStorage for Google Cloud Storage.
func newGCSStorage(cli *storage.Client, opts ...gcsStorageOpt) (*gcsStorage, error) {
	u := &gcsStorage{
		cli: cli,
	}
	for _, opt := range opts {
		if err := opt(u); err != nil {
			return nil, err
		}
	}
	return u, nil
}

var _ LifecyclerSetter = &gcsStorage{}

func (u *gcsStorage) SetLifecycler(lifecycler Lifecycler) error {
	u.lifecycle = lifecycler
	return nil
}

func (u *gcsStorage) Prepare(ctx context.Context) error {
	bucket := u.cli.Bucket(u.bucketName)
	_, err := bucket.Attrs(ctx)
	if err == storage.ErrBucketNotExist {
		// Try to create bucket if it doesn't exist.
		attrs := &storage.BucketAttrs{
			Location:     u.location,
			LocationType: u.locationType,
		}
		if err := bucket.Create(ctx, u.projectID, attrs); err != nil {
			return fmt.Errorf("error creating bucket: %w", err)
		}
		return nil
	}
	if err != nil {
		return fmt.Errorf("error checking for bucket: %w", err)
	}
	return nil
}

func (u *gcsStorage) Get(ctx context.Context, objectName string) (io.ReadCloser, error) {
	rc, err := u.cli.Bucket(u.bucketName).Object(objectName).NewReader(ctx)
	if err != nil {
		return nil, fmt.Errorf("error creating object reader: %w", err)
	}
	return rc, nil
}

func (u *gcsStorage) Upload(ctx context.Context, objectName string, src io.Reader, tags map[string]string) (int64, error) {
	// Append lifecycle tags.
	var err error
	tags, err = appendLifecycleTags(ctx, u.lifecycle, tags)
	if err != nil {
		return 0, fmt.Errorf("could not append lifecycle tags: %w", err)
	}

	logrus.Infof("uploading backup %s to GCS with tags: %s", objectName, tags)
	wc := u.cli.Bucket(u.bucketName).Object(objectName).NewWriter(ctx)
	startTime := time.Now()
	wc.ProgressFunc = func(n int64) {
		rate := rateString(time.Since(startTime), n)
		logrus.Debugf("uploaded %s (%s) of object %s", ByteCountBinary(n), rate, objectName)
	}
	wc.ObjectAttrs.Metadata = tags
	n, err := io.Copy(wc, src)
	if err != nil {
		return n, fmt.Errorf("io.Copy: %w", err)
	}
	if err := wc.Close(); err != nil {
		return n, fmt.Errorf("Writer.Close: %w", err)
	}
	if u.lifecycle != nil {
		logrus.Infof("starting backup sweep cycle ...")
		if err := u.lifecycle.Sweep(ctx, time.Now()); err != nil {
			return 0, fmt.Errorf("could not recycle old backups: %w", err)
		}
		logrus.Infof("sweep cycle done")
	} else {
		logrus.Warnf("no lifecycle set, keeping backups forever (!)")
	}
	return n, nil
}

func (u *gcsStorage) List(ctx context.Context, opts ...ListOpt) (backupItemList, error) {
	listOpts, err := ApplyListOptions(opts...)
	if err != nil {
		return nil, err
	}
	return u.list(ctx, listOpts)
}

func (u *gcsStorage) ListBundles(ctx context.Context) (backupItemList, error) {
	listOpts, err := ApplyListOptions(OnlyBundles{})
	if err != nil {
		return nil, err
	}
	return u.list(ctx, listOpts)
}

func (u *gcsStorage) list(ctx context.Context, listOpts ListOpts) (backupItemList, error) {
	var list, bundleList []*backupItem
	it := u.cli.Bucket(u.bucketName).Objects(ctx, nil)
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("Bucket(%q).Objects: %v", u.bucketName, err)
		}
		prefix := u.prefix
		if listOpts.withPrefix != "" {
			prefix = listOpts.withPrefix
		}
		if !strings.HasPrefix(attrs.Name, prefix) {
			continue
		}
		if !listOpts.matchingSuffix(attrs.Name) {
			continue
		}
		tags := attrs.Metadata
		if !listOpts.matchingTags(tags) {
			continue
		}
		item := &backupItem{
			Key:          attrs.Name,
			LastModified: attrs.Updated,
			Size:         attrs.Size,
			Tags:         tags,
			BucketName:   u.bucketName,
		}
		if val, ok := tags["is_bundle"]; ok && val == "true" {
			// Ignore backup bundle metadata objects as return value for the
			// "regular" list command. But still store them in a separate list.
			bundleList = append(bundleList, item)
			continue
		}
		// Apply tags now if relevant for backups.
		if daily, ok := tags[backupDaily]; ok {
			item.Daily = daily == "true"
		}
		if weekly, ok := tags[backupWeekly]; ok {
			item.Weekly = weekly == "true"
		}
		if monthly, ok := tags[backupMonthly]; ok {
			item.Monthly = monthly == "true"
		}
		link, err := u.getDownloadLink(item.Key)
		if err != nil {
			logrus.Warnf("could not generate signed url: %s", err)
		}
		parsedLink, err := url.Parse(link)
		if err != nil {
			logrus.Warnf("could not parse signed URL from Google: %v", err)
		}
		item.Link = parsedLink
		list = append(list, item)
	}
	sort.Sort(byULID(list))
	countBackups(list)
	if listOpts.returnBundles {
		return bundleList, nil
	} else {
		return list, nil
	}
}

func (u *gcsStorage) Link(ctx context.Context, objectName string) (string, error) {
	return u.getDownloadLink(objectName)
}

const (
	googleKeyEnvName = "GOOGLE_APPLICATION_CREDENTIALS"
)

func (u *gcsStorage) getDownloadLink(objectName string) (string, error) {
	// Apparently we need to re-read the key.json again.
	filename := os.Getenv(googleKeyEnvName)
	jsonKey, err := os.ReadFile(filename)
	if err != nil {
		return "", fmt.Errorf("error reading GCS key file %s: %w", filename, err)
	}
	conf, err := google.JWTConfigFromJSON(jsonKey)
	if err != nil {
		return "", fmt.Errorf("error parsing GCS key file %s: %w", filename, err)
	}
	opts := &storage.SignedURLOptions{
		Scheme:         storage.SigningSchemeV4,
		Method:         http.MethodGet,
		GoogleAccessID: conf.Email,
		PrivateKey:     conf.PrivateKey,
		Expires:        time.Now().Add(time.Hour * 24), // 1 day.
	}
	link, err := storage.SignedURL(u.bucketName, objectName, opts)
	if err != nil {
		return "", fmt.Errorf("error creating direct download URL: %w", err)
	}
	return link, nil
}

func (u *gcsStorage) Delete(ctx context.Context, in <-chan string) <-chan error {
	errc := make(chan error, 1)
	bucket := u.cli.Bucket(u.bucketName)

	go func() {
		defer close(errc)
		numDeletes := 0
		for name := range in {
			o := bucket.Object(name)
			if err := o.Delete(ctx); err != nil {
				errc <- fmt.Errorf("could not delete object %s: %w", name, err)
				return
			}
			numDeletes = numDeletes + 1
		}
		logrus.Infof("deleted %d objects from gcs storage", numDeletes)
	}()

	return errc
}

func (u *gcsStorage) Merge(ctx context.Context, srcNames []string, targetName string, tags map[string]string) error {
	// Append lifecycle tags.
	var err error
	tags, err = appendLifecycleTags(ctx, u.lifecycle, tags)
	if err != nil {
		return fmt.Errorf("could not append lifecycle tags: %w", err)
	}

	// Get all source object handles.
	srcs := make([]*storage.ObjectHandle, len(srcNames))
	for i, objectName := range srcNames {
		srcs[i] = u.cli.Bucket(u.bucketName).Object(objectName)
	}

	// Get the composer for the destination object.
	c := u.cli.Bucket(u.bucketName).Object(targetName).ComposerFrom(srcs...)
	c.ObjectAttrs.Metadata = tags

	// Compose the merged object.
	_, err = c.Run(ctx)
	if err != nil {
		return fmt.Errorf("could not compose objects: %w", err)
	}

	// Delete the source objects after success.
	deleteErrs := make([]error, 0)
	for _, src := range srcs {
		err := src.Delete(ctx)
		if err != nil {
			deleteErrs = append(deleteErrs, err)
		}
	}
	if len(deleteErrs) != 0 {
		return fmt.Errorf("delete errors: %v", deleteErrs)
	}
	return nil
}

func (u *gcsStorage) Lifecycle() (Lifecycler, error) {
	if u.lifecycle == nil {
		return nil, errNotFound
	}
	return u.lifecycle, nil
}

func (u *gcsStorage) Tag(ctx context.Context, objectName string, tags map[string]string) error {
	// Fetch existing object attributes.
	attrs, err := u.cli.Bucket(u.bucketName).Object(objectName).Attrs(ctx)
	if err != nil {
		return fmt.Errorf("could not fetch existing objects attributes: %w", err)
	}

	// Fetch the existing tags and add the given tags to them (overwriting
	// existing tags on conflict).
	md := attrs.Metadata
	for key, value := range tags {
		md[key] = value
	}
	uattrs := storage.ObjectAttrsToUpdate{
		Metadata: md,
	}

	// Update the object attributes.
	_, err = u.cli.Bucket(u.bucketName).Object(objectName).Update(ctx, uattrs)
	if err != nil {
		return fmt.Errorf("could not update object attributes: %w", err)
	}
	return nil
}
