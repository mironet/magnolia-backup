package main

import (
	"archive/tar"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"reflect"
	"testing"
	"time"

	"github.com/klauspost/compress/gzip"
	"github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func Test_parseLifecycle(t *testing.T) {
	type args struct {
		cycle string
	}
	tests := []struct {
		name        string
		args        args
		wantDaily   int
		wantWeekly  int
		wantMonthly int
		wantErr     bool
	}{
		{
			name:        "15 days only",
			args:        args{cycle: "15,0,0"},
			wantDaily:   15,
			wantWeekly:  0,
			wantMonthly: 0,
			wantErr:     false,
		},
		{
			name:        "15 days, 4 weeks, 3 months",
			args:        args{cycle: "15,4,3"},
			wantDaily:   15,
			wantWeekly:  4,
			wantMonthly: 3,
			wantErr:     false,
		},
		{
			name:        "15 days, -4 weeks, 3 months",
			args:        args{cycle: "15,-4,3"},
			wantDaily:   0,
			wantWeekly:  0,
			wantMonthly: 0,
			wantErr:     true,
		},
		{
			name:        "0 days, 0 weeks, 0 months",
			args:        args{cycle: "0,0,0"},
			wantDaily:   0,
			wantWeekly:  0,
			wantMonthly: 0,
			wantErr:     true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotDaily, gotWeekly, gotMonthly, err := parseLifecycle(tt.args.cycle)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseLifecycle() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotDaily != tt.wantDaily {
				t.Errorf("parseLifecycle() gotDaily = %v, want %v", gotDaily, tt.wantDaily)
			}
			if gotWeekly != tt.wantWeekly {
				t.Errorf("parseLifecycle() gotWeekly = %v, want %v", gotWeekly, tt.wantWeekly)
			}
			if gotMonthly != tt.wantMonthly {
				t.Errorf("parseLifecycle() gotMonthly = %v, want %v", gotMonthly, tt.wantMonthly)
			}
		})
	}
}

func Test_isWithin(t *testing.T) {
	now := time.Now()
	yesterday := now.AddDate(0, 0, -1)
	lastWeek := now.AddDate(0, 0, -7)
	lastMonth := now.AddDate(0, -1, 0)
	_ = lastMonth
	list := []*backupItem{
		{
			Key:          "new-backup",
			LastModified: now,
			Daily:        true,
		},
	}
	oneWeekly := []*backupItem{
		{
			Key:          "last-week",
			LastModified: yesterday,
			Daily:        true,
			Weekly:       true,
		},
	}
	oneMonthly := []*backupItem{
		{
			Key:          "new-backup",
			LastModified: now,
			Daily:        true,
		},
		{
			Key:          "last-month",
			LastModified: lastWeek,
			Daily:        true,
			Weekly:       false,
			Monthly:      true,
		},
	}
	type args struct {
		list []*backupItem
		date time.Time
		f    filterFunc
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "new backup, weekly",
			args: args{
				list: list,
				date: lastWeek,
				f:    weeklyBackups,
			},
			want: false,
		},
		{
			name: "new backup, monthly",
			args: args{
				list: list,
				date: lastMonth,
				f:    monthlyBackups,
			},
			want: false,
		},
		{
			name: "existing weekly",
			args: args{
				list: oneWeekly,
				date: lastWeek,
				f:    weeklyBackups,
			},
			want: true,
		},
		{
			name: "existing monthly",
			args: args{
				list: oneMonthly,
				date: lastMonth,
				f:    monthlyBackups,
			},
			want: true,
		},
		{
			name: "existing weekly, but filtering for monthly backups",
			args: args{
				list: oneWeekly,
				date: lastWeek,
				f:    monthlyBackups,
			},
			want: false,
		},
		{
			name: "existing monthly, but filtering for weekly backups",
			args: args{
				list: oneMonthly,
				date: lastMonth,
				f:    weeklyBackups,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isWithin(tt.args.list, tt.args.date, tt.args.f); got != tt.want {
				t.Errorf("isWithin() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_tagsFromEnv(t *testing.T) {
	type args struct {
		in []string
	}
	tests := []struct {
		name string
		args args
		want map[string]string
	}{
		{
			name: "simple env var list",
			args: args{in: []string{
				"MGNLBACKUP_TAGS_NAMESPACE=integration",
				"MGNLBACKUP_TAGS_POD_NAME=mysuperpod",
				"MGNLBACKUP_TAGS_APP=huiii",
				"KUBERNETES_ADDRESS=http://localhost:12345",
			}},
			want: map[string]string{
				"namespace": "integration",
				"pod_name":  "mysuperpod",
				"app":       "huiii",
			},
		},
		{
			name: "env var list without interesting var names",
			args: args{in: []string{
				"MGNLBACKUP_NAMESPACE=integration",
				"MGNLBACKUP_POD_NAME=mysuperpod",
				"MGNLBACKUP_APP=huiii",
				"KUBERNETES_ADDRESS=http://localhost:12345",
			}},
			want: map[string]string{},
		},
		{
			name: "try to provoke out of bounds",
			args: args{in: []string{
				"MGNLBACKUP_TAGS_=integration",
				"MGNLBACKUP_TAGS=integration",
				"MGNLBACKUP_TAGS_A=integration",
				"MGNLBACKUP_POD_NAME=mysuperpod",
				"MGNLBACKUP_APP=huiii",
				"KUBERNETES_ADDRESS=http://localhost:12345",
			}},
			want: map[string]string{
				"a": "integration",
			},
		},
		{
			name: "empty list",
			args: args{in: []string{}},
			want: map[string]string{},
		},
		{
			name: "real os.Environ just for fun",
			args: args{in: os.Environ()},
			want: map[string]string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tagsFromEnv(tt.args.in); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("tagsFromEnv() = %v, want %v", got, tt.want)
			}
		})
	}
}

var (
	expiryLongAgo         = time.Date(1990, 6, 27, 4, 0, 0, 0, time.UTC).Format(time.RFC3339)
	expiryEvenLongerAgo   = time.Date(1890, 6, 27, 4, 0, 0, 0, time.UTC).Format(time.RFC3339)
	expiryFarAway         = time.Date(2990, 6, 27, 4, 0, 0, 0, time.UTC).Format(time.RFC3339)
	expiryEvenFurtherAway = time.Date(3990, 6, 27, 4, 0, 0, 0, time.UTC).Format(time.RFC3339)

	// NOTE: Do not change the order of this list.
	stdBackupListWithExpiry = []*backupItem{
		{
			// key = 0
			Key:          fmt.Sprintf(fmtTxLog, orphanBackupID, MustULID(oneMonthAgo)),
			LastModified: oneMonthAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID: orphanBackupID,
				tagKeyIsTxLog:  "true",
				tagKeyExpiry:   expiryEvenLongerAgo,
			},
		},
		{
			// key = 1
			Key:          fmt.Sprintf(fmtBaseBackup, threeWeeksBackupID, MustULID(threeWeeksAgo)),
			LastModified: threeWeeksAgo,
			Size:         512 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:     threeWeeksBackupID,
				tagKeyIsBaseBackup: "true",
				tagKeyExpiry:       expiryLongAgo,
			},
		},
		{
			// key = 2
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(twoWeeksAgo)),
			LastModified: twoWeeksAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID: threeWeeksBackupID,
				tagKeyIsTxLog:  "true",
				tagKeyExpiry:   expiryLongAgo,
			},
		},
		{
			// key = 3
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(oneWeekAgo)),
			LastModified: oneWeekAgo,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID: threeWeeksBackupID,
				tagKeyIsTxLog:  "true",
				tagKeyExpiry:   expiryLongAgo,
			},
		},
		{
			// key = 4
			Key:          fmt.Sprintf(fmtTxLog, threeWeeksBackupID, MustULID(now)),
			LastModified: now,
			Size:         1 * Mi,
			Tags: map[string]string{
				tagKeyBackupID: threeWeeksBackupID,
				tagKeyIsTxLog:  "true",
				tagKeyExpiry:   expiryLongAgo,
			},
		},
		{
			// key = 5
			Key:          fmt.Sprintf(fmtBaseBackup, inOneWeekBackupID, MustULID(inOneWeek)),
			LastModified: inOneWeek,
			Size:         256 * Mi,
			Tags: map[string]string{
				tagKeyBackupID:     inOneWeekBackupID,
				tagKeyIsBaseBackup: "true",
				tagKeyExpiry:       expiryFarAway,
			},
		},
	}
)

type mockStorage struct {
	listItems        []*backupItem
	deleteKeys       []string
	objects          map[string][]byte
	tags             map[string]map[string]string
	compressionLevel int
}

func (m *mockStorage) Upload(ctx context.Context, objectName string, in io.Reader, tags map[string]string) (int64, error) {
	// Get the object name from the (possible) part.
	objectNameWithoutPart, err := objectNameFromParts([]string{objectName})
	if err == nil {
		objectName = objectNameWithoutPart
	}

	// Prepare all required readers and writers.
	zr, err := gzip.NewReader(in)
	if err != nil {
		return 0, fmt.Errorf("could not get gzip reader: %w", err)
	}
	defer zr.Close()
	tr := tar.NewReader(zr)

	n := int64(0)
	for {
		_, err := tr.Next()
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			return 0, fmt.Errorf("tar error: %s", err)
		}

		// Read all bytes from the input.
		bytes, err := io.ReadAll(tr)
		if err != nil {
			return 0, fmt.Errorf("could not read input: %w", err)
		}
		// "Store" the input.
		m.objects[filepath.Base(objectName)] = append(m.objects[filepath.Base(objectName)], bytes...)
		n = n + int64(len(bytes))
	}

	// "Store" the tags.
	m.tags[filepath.Base(objectName)] = tags

	return n, nil
}

func (m *mockStorage) Prepare(ctx context.Context) error {
	return fmt.Errorf("not implemented")
}

func (m *mockStorage) Merge(ctx context.Context, srcNames []string, targetName string, tags map[string]string) error {
	if _, ok := m.objects[filepath.Base(targetName)]; !ok {
		return fmt.Errorf("expected to have merged object %q in storage", targetName)
	}
	logrus.Infof("no need to merge %v to target %q, already have content %q in mock storage", srcNames, targetName, m.objects[filepath.Base(targetName)])
	m.tags[filepath.Base(targetName)] = tags
	return nil
}

func (m *mockStorage) Lifecycle() (Lifecycler, error) {
	return nil, fmt.Errorf("not implemented")
}

func (m *mockStorage) Link(ctx context.Context, objectName string) (string, error) {
	return "", fmt.Errorf("not implemented")
}

func (m *mockStorage) Get(ctx context.Context, objectName string) (io.ReadCloser, error) {
	obj, ok := m.objects[filepath.Base(objectName)]
	if !ok {
		return nil, errNotFound
	}
	r, w := io.Pipe()
	go func() {
		defer w.Close()
		zw, err := gzip.NewWriterLevel(w, m.compressionLevel)
		if err != nil {
			w.CloseWithError(fmt.Errorf("could not get new gzip writer: %v", err))
		}
		defer zw.Close()
		tw := tar.NewWriter(zw)
		if err := tw.WriteHeader(&tar.Header{
			Typeflag: tar.TypeReg,
			Name:     filepath.Base(objectName),
			Mode:     0644,
			ModTime:  time.Now(),
			Size:     int64(len(obj)),
		}); err != nil {
			w.CloseWithError(err)
		}

		if _, err := tw.Write(obj); err != nil {
			w.CloseWithError(err)
		}

		if err := tw.Close(); err != nil {
			logrus.Errorf("tar close error: %v", err)
		}
	}()
	logrus.Printf("getting object obj with content %s as tar.gz", string(obj))
	return r, nil
}

func (m *mockStorage) List(context.Context, ...ListOpt) (backupItemList, error) {
	return m.listItems, nil
}

func (m *mockStorage) Delete(ctx context.Context, keyChan <-chan string) <-chan error {
	out := make(chan error)
	go func() {
		defer close(out)
		numDeletedKeys := 0
		for key := range keyChan {
			if !(numDeletedKeys < len(m.deleteKeys)) {
				select {
				case out <- fmt.Errorf("unexpected key %q: no more key expected", key):
				case <-ctx.Done():
					return
				}
				break
			}
			if key != m.deleteKeys[numDeletedKeys] {
				select {
				case out <- fmt.Errorf("unexpected key %q: want %q", key, m.deleteKeys[numDeletedKeys]):
				case <-ctx.Done():
					return
				}
			}
			numDeletedKeys = numDeletedKeys + 1
			delete(m.objects, filepath.Base(key))
			delete(m.tags, filepath.Base(key))
		}
		if numDeletedKeys != len(m.deleteKeys) {
			select {
			case out <- fmt.Errorf("unexpected deletion of %d key(s): but only deleted %d", len(m.deleteKeys), numDeletedKeys):
			case <-ctx.Done():
				return
			}
		}
	}()
	return out
}

func (m *mockStorage) Tag(ctx context.Context, objectName string, tags map[string]string) error {
	if _, ok := m.objects[filepath.Base(objectName)]; !ok {
		return errNotFound
	}
	if m.tags[filepath.Base(objectName)] == nil {
		m.tags[filepath.Base(objectName)] = make(map[string]string)
	}
	for key, value := range tags {
		m.tags[filepath.Base(objectName)][key] = value
	}
	return nil
}

func (m *mockStorage) Close() error {
	return fmt.Errorf("not implemented")
}

func TestContinuousLifecycle_Sweep(t *testing.T) {
	now := time.Date(2022, 2, 3, 1, 45, 0, 0, time.UTC)
	list3 := copyBackupItems(stdBackupListWithExpiry)
	for i := 1; i <= 4; i++ {
		list3[i].Tags[tagKeyExpiry] = expiryEvenFurtherAway
	}
	type fields struct {
		max  time.Duration
		stor ObjectStorage
	}
	type args struct {
		ctx context.Context
		t   time.Time
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "delete old bundle, but don't delete current bundle",
			fields: fields{
				max: 1 * time.Hour,
				stor: &mockStorage{
					listItems: copyBackupItems(stdBackupListWithExpiry),
					deleteKeys: []string{
						stdBackupListWithExpiry[0].Key,
					},
				},
			},
			args: args{
				ctx: context.Background(),
				t:   now,
			},
			wantErr: false,
		},
		{
			name: "delete two old bundles",
			fields: fields{
				max: 1 * time.Hour,
				stor: &mockStorage{
					listItems: copyBackupItems(stdBackupListWithExpiry),
					deleteKeys: []string{
						stdBackupListWithExpiry[0].Key,
						stdBackupListWithExpiry[1].Key,
						stdBackupListWithExpiry[2].Key,
						stdBackupListWithExpiry[3].Key,
						stdBackupListWithExpiry[4].Key,
					},
				},
			},
			args: args{
				ctx: context.Background(),
				t:   time.Date(3000, 6, 27, 4, 0, 0, 0, time.UTC),
			},
			wantErr: false,
		},
		{
			name: "two old bundles, delete only older one",
			fields: fields{
				max: 1 * time.Hour,
				stor: &mockStorage{
					listItems: list3,
					deleteKeys: []string{
						stdBackupListWithExpiry[0].Key,
					},
				},
			},
			args: args{
				ctx: context.Background(),
				t:   time.Date(3000, 6, 27, 4, 0, 0, 0, time.UTC),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &ContinuousLifecycle{
				max:  tt.fields.max,
				stor: tt.fields.stor,
			}
			if err := c.Sweep(tt.args.ctx, tt.args.t); (err != nil) != tt.wantErr {
				t.Errorf("ContinuousLifecycle.Sweep() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
