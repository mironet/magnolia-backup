package main

import (
	"reflect"
	"testing"

	"github.com/spf13/viper"
)

func Test_objectNameFromParts(t *testing.T) {
	type args struct {
		parts []string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "simple case",
			args: args{
				parts: []string{
					"foo.part1",
					"foo.part2",
					"foo.part3",
					"foo.part4",
					"foo.part5",
				},
			},
			want: "foo",
		},
		{
			name: "multiple dots",
			args: args{
				parts: []string{
					"foo.tar.gz.part1",
					"foo.tar.gz.part2",
					"foo.tar.gz.part3",
					"foo.tar.gz.part4",
					"foo.tar.gz.part5",
				},
			},
			want: "foo.tar.gz",
		},
		{
			name: "unexpected empty string",
			args: args{
				parts: []string{
					"foo.tar.gz.part1",
					"foo.tar.gz.part2",
					"",
					"foo.tar.gz.part4",
					"foo.tar.gz.part5",
				},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "no parts",
			args: args{
				parts: []string{},
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "not a part",
			args: args{
				parts: []string{"foo.tar.gz"},
			},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := objectNameFromParts(tt.args.parts)
			if (err != nil) != tt.wantErr {
				t.Errorf("objectNameFromParts() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("objectNameFromParts() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestListOpts_matchingSuffix(t *testing.T) {
	tests := []struct {
		name       string
		withSuffix string
		objectKey  string
		want       bool
	}{
		{
			name:       "empty withSuffix",
			withSuffix: "",
			objectKey:  "test.test",
			want:       true,
		},
		{
			name:       "empty objectKey",
			withSuffix: "test",
			objectKey:  "",
			want:       false,
		},
		{
			name:       "both empty",
			withSuffix: "",
			objectKey:  "",
			want:       true,
		},
		{
			name:       "matching",
			withSuffix: "test",
			objectKey:  "abc.test",
			want:       true,
		},
		{
			name:       "not matching",
			withSuffix: "test",
			objectKey:  "test.abc",
			want:       false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := ListOpts{
				withSuffix: tt.withSuffix,
			}
			if got := o.matchingSuffix(tt.objectKey); got != tt.want {
				t.Errorf("ListOpts.matchingSuffix() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestListOpts_matchingTags(t *testing.T) {
	tests := []struct {
		name                         string
		withTags                     map[string]string
		tags                         map[string]string
		withoutTxLogArchivingVariant bool
		want                         bool
	}{
		{
			name:     "empty withTags",
			withTags: nil,
			tags: map[string]string{
				"test a": "oh",
				"test b": "hai",
			},
			withoutTxLogArchivingVariant: true,
			want:                         true,
		},
		{
			name: "empty tags",
			withTags: map[string]string{
				"test a": "oh",
				"test b": "hai",
			},
			tags:                         nil,
			withoutTxLogArchivingVariant: true,
			want:                         false,
		},
		{
			name:                         "both empty",
			withTags:                     nil,
			tags:                         nil,
			withoutTxLogArchivingVariant: true,
			want:                         true,
		},
		{
			name: "matching",
			withTags: map[string]string{
				"test a": "oh",
				"test b": "hai",
			},
			tags: map[string]string{
				"test a": "oh",
				"test c": "schwupp",
				"test b": "hai",
			},
			withoutTxLogArchivingVariant: true,
			want:                         true,
		},
		{
			name: "not matching",
			withTags: map[string]string{
				"test a": "oh",
				"test b": "hai",
			},
			tags: map[string]string{
				"test a": "oh",
				"test c": "hai",
			},
			withoutTxLogArchivingVariant: true,
			want:                         false,
		},
		{
			name: "matching with tx log archiving variant",
			withTags: map[string]string{
				"test a": "oh",
				"test b": "hai",
			},
			tags: map[string]string{
				"test a":       "oh",
				"test c":       "schwupp",
				"test b":       "hai",
				variantTagName: txLogArchiving,
			},
			withoutTxLogArchivingVariant: false,
			want:                         true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := ListOpts{
				withTags:                     tt.withTags,
				withoutTxLogArchivingVariant: tt.withoutTxLogArchivingVariant,
			}
			if got := o.matchingTags(tt.tags); got != tt.want {
				t.Errorf("ListOpts.matchingTags() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestApplyListOptions(t *testing.T) {
	tests := []struct {
		name           string
		usePgWal       bool
		skipViperSetup bool
		opts           []ListOpt
		want           ListOpts
		wantErr        bool
	}{
		{
			name:     "success with pg_wal",
			usePgWal: true,
			opts: []ListOpt{
				OnlyBundles{},
				WithPrefix{
					Prefix: "test prefix",
				},
				WithSuffix{
					Suffix: "test suffix",
				},
				WithTags{
					Tags: map[string]string{
						"test": "map",
					},
				},
			},
			want: ListOpts{
				returnBundles: true,
				withSuffix:    "test suffix",
				withPrefix:    "test prefix",
				withTags: map[string]string{
					"test": "map",
				},
			},
		},
		{
			name: "success without pg_wal",
			opts: []ListOpt{
				OnlyBundles{},
				WithPrefix{
					Prefix: "test prefix",
				},
				WithSuffix{
					Suffix: "test suffix",
				},
				WithTags{
					Tags: map[string]string{
						"test": "map",
					},
				},
			},
			want: ListOpts{
				returnBundles: true,
				withSuffix:    "test suffix",
				withPrefix:    "test prefix",
				withTags: map[string]string{
					"test": "map",
				},
				withoutTxLogArchivingVariant: true,
			},
		},
		{
			name:           "success without pg_wal",
			skipViperSetup: true,
			opts: []ListOpt{
				OnlyBundles{},
				WithPrefix{
					Prefix: "test prefix",
				},
				WithSuffix{
					Suffix: "test suffix",
				},
				WithTags{
					Tags: map[string]string{
						"test": "map",
					},
				},
			},
			want: ListOpts{
				returnBundles: true,
				withSuffix:    "test suffix",
				withPrefix:    "test prefix",
				withTags: map[string]string{
					"test": "map",
				},
				withoutTxLogArchivingVariant: false,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer delete(vipers, "dump")

			v := viper.New()
			initViper(v)
			v.Set("use-pg-wal", tt.usePgWal)
			vipers["dump"] = v
			if tt.skipViperSetup {
				delete(vipers, "dump")
			}

			got, err := ApplyListOptions(tt.opts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("ApplyListOptions() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ApplyListOptions() = %v, want %v", got, tt.want)
			}
		})
	}
}
