package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var cfgFile string
var loglevelFlag string

// A list of viper instances to keep track of suboptions with the same name but
// different meaning.
var vipers = make(map[string]*viper.Viper)

const (
	envVarPrefix = "MGNLBACKUP"
)

// initViper works around issue #233 in viper:
// https://github.com/spf13/viper/issues/233
func initViper(v *viper.Viper) {
	v.SetEnvPrefix(envVarPrefix)
	v.AutomaticEnv()
	v.SetEnvKeyReplacer(strings.NewReplacer("-", "_", ".", "_"))
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "mgnlbackup",
	Short: "Magnolia DB backup collector. Stores to object storage.",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main(). It only needs to happen once to the rootCmd.
func Execute(version string) {
	rootCmd.Version = version

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	v := viper.GetViper()
	initViper(v)

	flags := rootCmd.PersistentFlags()

	flags.String("loglevel", "info", "error, warn, info, debug, trace")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".mgnlbackup" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".mgnlbackup")
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

	loglevelFlag = viper.GetString("loglevel")

	formatter := &logrus.TextFormatter{}
	formatter.TimestampFormat = time.RFC3339
	formatter.FullTimestamp = true
	logrus.SetFormatter(formatter)

	switch loglevelFlag {
	case "error":
		logrus.SetLevel(logrus.ErrorLevel)
	case "warn":
		logrus.SetLevel(logrus.WarnLevel)
	case "info":
		logrus.SetLevel(logrus.InfoLevel)
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
	case "trace":
		logrus.SetLevel(logrus.TraceLevel)
		os.Setenv("GODEBUG", "http2debug=1")
	}

	logrus.Debugf("setting log level to %s", logrus.GetLevel())
	logrus.Infof("running version %s", version)
}
