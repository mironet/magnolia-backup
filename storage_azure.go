package main

import (
	"context"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blockblob"
	azsas "github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/sas"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
)

const (
	azAccountURLTpl     = "https://%s.blob.core.windows.net/"
	envAzureAccountName = "AZURE_STORAGE_ACCOUNT_NAME"
	envAzureAccountKey  = "AZURE_STORAGE_ACCOUNT_KEY"
)

var (
	azureMergeChunkSize   int64 = 64 * Mi
	azureMergeConcurrency       = 8
	testingNoDelete             = false // If true, source blobs in Merge() are not removed after successful merge.
)

func unptr[K comparable, V any](in map[K]*V) map[K]V {
	var out = make(map[K]V, len(in))
	for k, v := range in {
		out[k] = *v
	}
	return out
}

func toptr[K comparable, V any](in map[K]V) map[K]*V {
	var out = make(map[K]*V, len(in))
	for k, v := range in {
		v := v // 🤦‍♂️
		out[k] = &v
	}
	return out
}

// Returns a new map with all lowercase keys. On collision a random element will
// be chosen as the value.
func lowerKeys[V any](in map[string]V) map[string]V {
	var out = make(map[string]V, len(in))
	for k, v := range in {
		out[strings.ToLower(k)] = v
	}
	return out
}

type azureStorConfig struct {
	Prefix      string `yaml:"prefix,omitempty"`
	Container   string `yaml:"container,omitempty"`
	AccountName string `yaml:"accountName,omitempty"`
	AccountKey  string `yaml:"accountKey,omitempty"`
}

func objectStorageConfigFromAzure(conf azureStorConfig, readonly bool) ObjectStorageConfig {
	return ObjectStorageConfig{
		prefix:     conf.Prefix,
		bucketName: conf.Container,
		readonly:   readonly,
		azure: AzureConfig{
			enabled:     true,
			accountName: conf.AccountName,
			accountKey:  conf.AccountKey,
		},
	}
}

func (first *azureStorConfig) merge(name string, second *azureStorConfig) {
	if second.Prefix != "" {
		if first.Prefix != "" {
			logrus.Debugf("overwriting prefix %s with %s in storage %s", first.Prefix, second.Prefix, name)
		}
		first.Prefix = second.Prefix
	}
	if second.Container != "" {
		if first.Container != "" {
			logrus.Debugf("overwriting container %s with %s in storage %s", first.Container, second.Container, name)
		}
		first.Container = second.Container
	}
	if second.AccountName != "" {
		if first.AccountName != "" {
			logrus.Debugf("overwriting account name %s with %s in storage %s", first.AccountName, second.AccountName, name)
		}
		first.AccountName = second.AccountName
	}
	if second.AccountKey != "" {
		if first.AccountKey != "" {
			logrus.Debugf("overwriting account key in storage %s", name)
		}
		first.AccountKey = second.AccountKey
	}
}

type azureStorage struct {
	containerName string // In S3 this is usually called a "bucket".
	prefix        string
	lifecycle     Lifecycler
	cli           *azblob.Client
}

// Prove azureStorage is indeed an object storage.
var _ ObjectStorage = &azureStorage{}

// azureStorageOpt is an option for configuring Azure Blob storage.
type azureStorageOpt func(*azureStorage) error

var _ LifecyclerSetter = &azureStorage{}

func (as *azureStorage) SetLifecycler(lifecycler Lifecycler) error {
	as.lifecycle = lifecycler
	return nil
}

func withContainerName(name string) azureStorageOpt {
	return func(as *azureStorage) error {
		as.containerName = name
		return nil
	}
}

func withAzurePrefix(prefix string) azureStorageOpt {
	return func(as *azureStorage) error {
		as.prefix = prefix
		return nil
	}
}

func withDefaultClient() azureStorageOpt {
	return func(as *azureStorage) error {
		accountName, accountKey := os.Getenv(envAzureAccountName), os.Getenv(envAzureAccountKey)
		if accountName == "" {
			return fmt.Errorf("could not find env var %s", envAzureAccountName)
		}
		if accountKey == "" {
			return fmt.Errorf("could not find env var %s", envAzureAccountKey)
		}
		cred, err := azblob.NewSharedKeyCredential(accountName, accountKey)
		if err != nil {
			return fmt.Errorf("could not create new shared key credential: %w", err)
		}
		cli, err := azblob.NewClientWithSharedKeyCredential(fmt.Sprintf(azAccountURLTpl, accountName), cred, nil)
		if err != nil {
			return fmt.Errorf("could not create new azblob client: %w", err)
		}
		as.cli = cli
		return nil
	}
}

func (as *azureStorage) Close() error {
	return nil
}

// newAzureStorage creates a new Azure storage instance. If the client is nil,
// the default client will be used which expects env vars with the account name
// and a shared key. See here for more information on shared keys:
// https://learn.microsoft.com/en-us/azure/storage/common/storage-account-keys-manage?tabs=azure-portal
func newAzureStorage(cli *azblob.Client, opts ...azureStorageOpt) (*azureStorage, error) {
	as := &azureStorage{
		cli: cli,
	}
	if cli == nil {
		opts = append(opts, withDefaultClient())
	}
	for _, opt := range opts {
		if err := opt(as); err != nil {
			return nil, err
		}
	}
	return as, nil
}

func (as *azureStorage) Prepare(ctx context.Context) error {
	_, err := as.cli.CreateContainer(ctx, as.containerName, nil)
	if err != nil {
		if !bloberror.HasCode(err, bloberror.ContainerAlreadyExists) {
			return fmt.Errorf("error preparing container: %w", err)
		}
		logrus.Infof("container %s already exists", as.containerName)
	}
	return nil
}

func (as *azureStorage) Get(ctx context.Context, objectName string) (io.ReadCloser, error) {
	resp, err := as.cli.DownloadStream(ctx, as.containerName, objectName, nil)
	if err != nil {
		return nil, fmt.Errorf("error requesting blob download: %w", err)
	}
	return resp.Body, nil
}

func (as *azureStorage) Upload(ctx context.Context, objectName string, src io.Reader, tags map[string]string) (int64, error) {
	// Append lifecycle tags.
	var err error
	tags, err = appendLifecycleTags(ctx, as.lifecycle, tags)
	if err != nil {
		return 0, fmt.Errorf("could not append lifecycle tags: %w", err)
	}

	startTime := time.Now()
	var total int64
	// Azure Go client lib does not support a progress func for client.UploadStream() ...
	reader := newProgressReader(src, func(n int64) {
		rate := rateString(time.Since(startTime), n)
		total = n
		logrus.Debugf("uploaded %s (%s) of object %s", ByteCountBinary(n), rate, objectName)
	})

	logrus.Infof("uploading backup %s to Azure Blob Storage with tags: %s", objectName, tags)
	_, err = as.cli.UploadStream(ctx, as.containerName, objectName, reader, &azblob.UploadStreamOptions{
		Metadata: toptr(tags),
	})
	if err != nil {
		return 0, fmt.Errorf("error starting the upload: %w", err)
	}

	if as.lifecycle != nil {
		logrus.Infof("starting backup sweep cycle ...")
		if err := as.lifecycle.Sweep(ctx, time.Now()); err != nil {
			return 0, fmt.Errorf("could not recycle old backups: %w", err)
		}
		logrus.Infof("sweep cycle done")
	} else {
		logrus.Warnf("no lifecycle set, keeping backups forever (!)")
	}

	return total, nil
}

func (as *azureStorage) List(ctx context.Context, opts ...ListOpt) (backupItemList, error) {
	listOpts, err := ApplyListOptions(opts...)
	if err != nil {
		return nil, err
	}
	return as.list(ctx, listOpts)
}

func (as *azureStorage) ListBundles(ctx context.Context) (backupItemList, error) {
	listOpts, err := ApplyListOptions(OnlyBundles{})
	if err != nil {
		return nil, err
	}
	return as.list(ctx, listOpts)
}

func (as *azureStorage) list(ctx context.Context, listOpts ListOpts) (backupItemList, error) {
	var list, bundleList []*backupItem
	pagerOpts := &azblob.ListBlobsFlatOptions{}
	pagerOpts.Include.Metadata = true
	pagerOpts.Prefix = &as.prefix
	if listOpts.withPrefix != "" {
		pagerOpts.Prefix = &listOpts.withPrefix
	}
	pager := as.cli.NewListBlobsFlatPager(as.containerName, pagerOpts)
	for pager.More() {
		resp, err := pager.NextPage(ctx)
		if err != nil {
			return nil, fmt.Errorf("list(): error getting next page: %w", err)
		}
		for _, blob := range resp.Segment.BlobItems {
			if !listOpts.matchingSuffix(*blob.Name) {
				continue
			}
			tags := unptr(blob.Metadata)
			if !listOpts.matchingTags(tags) {
				continue
			}
			var lmodify time.Time
			var clength int64
			if blob.Properties != nil {
				prop := blob.Properties
				if prop.LastModified != nil {
					lmodify = *prop.LastModified
				}
				if prop.ContentLength != nil {
					clength = *prop.ContentLength
				}
			}
			item := &backupItem{
				Key:          *blob.Name,
				LastModified: lmodify,
				Size:         clength,
				Tags:         tags,
				BucketName:   as.containerName, // Azure calls buckets 'containers'.
			}
			// Apply tags now if relevant for backups.
			if daily, ok := tags[backupDaily]; ok {
				item.Daily = daily == "true"
			}
			if weekly, ok := tags[backupWeekly]; ok {
				item.Weekly = weekly == "true"
			}
			if monthly, ok := tags[backupMonthly]; ok {
				item.Monthly = monthly == "true"
			}
			link, err := as.Link(ctx, *blob.Name)
			if err != nil {
				logrus.Warnf("could not generate SAS URL: %s", err)
			}
			parsedLink, err := url.Parse(link)
			if err != nil {
				logrus.Warnf("could not parse SAS URL from Azure: %v", err)
			}
			item.Link = parsedLink
			list = append(list, item)
		}
	}
	sort.Sort(byULID(list))
	countBackups(list)
	if listOpts.returnBundles {
		return bundleList, nil
	}
	return list, nil
}

func (as *azureStorage) Link(ctx context.Context, objectName string) (string, error) {
	now := time.Now().UTC().Add(-5 * time.Minute) // Allow for small time skew.
	expiry := now.Add(7 * 24 * time.Hour)         // 7 days.

	scli := as.cli.ServiceClient().NewContainerClient(as.containerName).NewBlobClient(objectName)
	sasURL, err := scli.GetSASURL(azsas.BlobPermissions{Read: true}, expiry, &blob.GetSASURLOptions{StartTime: &now})
	if err != nil {
		return "", fmt.Errorf("could not generate SAS URL for object %s: %w", objectName, err)
	}

	return sasURL, nil
}

func (as *azureStorage) Delete(ctx context.Context, in <-chan string) <-chan error {
	errc := make(chan error, 1)

	go func() {
		defer close(errc)
		numDeletes := 0
		for name := range in {
			if _, err := as.cli.DeleteBlob(ctx, as.containerName, name, nil); err != nil {
				errc <- fmt.Errorf("could not delete object %s: %w", name, err)
				return
			}
			numDeletes = numDeletes + 1
		}
		logrus.Infof("deleted %d objects from Azure Blob Storage", numDeletes)
	}()

	return errc
}

type azSourceUnit struct {
	objectName string
	size       int64
	blockList  []string
	order      int
}

type azWorkUnit struct {
	source    string
	byteRange byteRange
	blockID   string
}

// Merge starts one or more concurrent PutFromURL operations on Azure, copying
// and concatenating all blobs in srcNames into a new block named 'targetName'.
// The original blobs in srcNames will be deleted as soon as the copy/merge
// operation has been completed successfully.
func (as *azureStorage) Merge(ctx context.Context, srcNames []string, targetName string, tags map[string]string) error {
	g, gctx := errgroup.WithContext(ctx)

	copyc := make(chan azSourceUnit)
	blobc := make(chan azSourceUnit)

	var err error
	tags, err = appendLifecycleTags(ctx, as.lifecycle, tags)
	if err != nil {
		return fmt.Errorf("Merge(): could not append lifecycle tags: %w", err)
	}

	g.Go(func() error {
		// Start sizig stage of pipeline.
		defer close(copyc)
		err := as.stageGetSizes(gctx, srcNames, copyc)
		if err != nil {
			return fmt.Errorf("size stage: %w", err)
		}
		return nil
	})

	g.Go(func() error {
		// Start copying fanout stage here.
		defer close(blobc)
		err := as.stageCopy(gctx, copyc, blobc, targetName)
		if err != nil {
			return fmt.Errorf("copy stage: %w", err)
		}
		return nil
	})

	var blocklist []string
	g.Go(func() error {
		// Collect all the finished block IDs.
		for su := range blobc {
			blocklist = append(blocklist, su.blockList...)
		}
		return nil
	})

	if err := g.Wait(); err != nil {
		return fmt.Errorf("Merge(): error executing merge pipeline: %w", err)
	}

	// Commit the block list.
	target := as.cli.ServiceClient().NewContainerClient(as.containerName).NewBlockBlobClient(targetName)
	sort.Strings(blocklist) // This works because ULIDs are unique, monotonous and lexicographically sortable.

	// Now base64-encode blocks so Azure is happy.
	b64blocks := make([]string, len(blocklist))
	for k, v := range blocklist {
		b64blocks[k] = base64.URLEncoding.EncodeToString([]byte(v))
	}

	if _, err := target.CommitBlockList(ctx, b64blocks, nil); err != nil {
		return fmt.Errorf("Merge(): error committing block list of object %s: %w", targetName, err)
	}

	// Set the metadata for the new object.
	if _, err := target.SetMetadata(ctx, toptr(tags), nil); err != nil {
		return fmt.Errorf("Merge(): error setting metadata for object %s: %w", targetName, err)
	}

	// Get the first 512 bytes of the new object to detect the content type.
	const buflen = 512
	var buf = make([]byte, buflen)
	n, err := target.DownloadBuffer(ctx, buf, &blob.DownloadBufferOptions{Range: blob.HTTPRange{Offset: 0, Count: buflen}})
	if err == nil {
		// Try to find the content type and set it in the blob properties.
		contentType := http.DetectContentType(buf[:n])
		if _, err := target.SetHTTPHeaders(ctx, blob.HTTPHeaders{
			BlobContentType: &contentType,
		}, nil); err != nil {
			logrus.Warnf("Merge(): error setting http headers: %v", err)
		}
	} else {
		// We do not break the whole Merge() operation just because of this.
		logrus.Warnf("Merge(): could not set content-type because of an error reading the first 512 bytes: %v", err)
	}

	logrus.Debugf("Merge(): done merging target object %q from parts %q", targetName, srcNames)

	// Delete source blobs after successful merge. Only delete if testing does
	// not prevent it.
	if testingNoDelete {
		return nil
	}
	delc := make(chan string)
	go func() {
		defer close(delc)
		defer logrus.Debugf("done removing parts")
		for _, v := range srcNames {
			select {
			case <-ctx.Done():
				return
			case delc <- v:
				logrus.Debugf("removing part %s", v)
			}
		}
	}()
	logrus.Debugf("removing parts ...")
	for err := range as.Delete(ctx, delc) {
		if err != nil {
			return fmt.Errorf("Merge(): error removing uploaded parts after composing: %w", err)
		}
	}

	return nil
}

func (as *azureStorage) stageGetSizes(ctx context.Context, srcNames []string, out chan<- azSourceUnit) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	g, ctx := errgroup.WithContext(ctx)
	in := make(chan azSourceUnit)

	logrus.Debugf("starting %d get size workers", azureMergeConcurrency)
	defer logrus.Debugf("stopped %d get size workers", azureMergeConcurrency)

	for i := 0; i < azureMergeConcurrency; i++ {
		g.Go(func() error {
			for su := range in {
				name := su.objectName
				size, err := as.Size(ctx, name)
				if err != nil {
					// TODO: Retry!
					return fmt.Errorf("error getting size of object %s: %w", name, err)
				}
				logrus.Debugf("source object %s is %s", name, ByteCountBinary(size))
				su.size = size

				select {
				case out <- su:
				case <-ctx.Done():
					return ctx.Err()
				}
			}
			return nil
		})
	}

	for i, v := range srcNames {
		select {
		case in <- azSourceUnit{
			objectName: v,
			order:      i, // Keep the ordering.
		}:
		case <-ctx.Done():
			return ctx.Err()
		}
	}
	close(in)

	return g.Wait()
}

func (as *azureStorage) stageCopy(ctx context.Context, in <-chan azSourceUnit, out chan<- azSourceUnit, targetName string) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	startTime := time.Now().Add(-1 * time.Hour) // Allow for clock skew and other weirdness.

	for su := range in {
		rangec := make(chan azWorkUnit)
		// We need to get SAS URLs for every "chunk" we're uploading to block
		// blob. A chunk cannot be larger than 100 MiB according to Microsoft
		// documentation
		// (https://learn.microsoft.com/en-us/rest/api/storageservices/put-block-from-url#remarks).
		// blockblob.MaxStageBlockBytes says 4 GiB. We probably want to be
		// smaller than that anyway to show some progress along the way. WE use
		// 'azureMergeChunkSize' for that now.
		scli := as.cli.ServiceClient().NewContainerClient(as.containerName).NewBlobClient(su.objectName)
		source, err := scli.GetSASURL(azsas.BlobPermissions{Read: true}, startTime.AddDate(0, 0, 1), &blob.GetSASURLOptions{StartTime: &startTime})
		if err != nil {
			return fmt.Errorf("could not create SAS url for object %s: %w", su.objectName, err)
		}

		// Fanout server-side block transfer.
		g, gctx := errgroup.WithContext(ctx)
		g.Go(func() error {
			if err := as.stageFanoutCopy(gctx, rangec, targetName); err != nil {
				return fmt.Errorf("fanout copy: %w", err)
			}
			return nil
		})

		rr := newRange(su.size, azureMergeChunkSize)
		for _, r := range rr {
			// All blocks of this source object have the same time in their
			// ULID.
			blockID := MustULID(startTime.Add(time.Duration(su.order) * time.Millisecond))

			select {
			case rangec <- azWorkUnit{
				source:    source,
				byteRange: r,
				blockID:   base64.URLEncoding.EncodeToString([]byte(blockID)),
			}:
				// Order is not super important here because ULIDs have a
				// monotonic ordering already.
				su.blockList = append(su.blockList, blockID)
			case <-gctx.Done():
				return gctx.Err()
			}
		}
		close(rangec)

		// Send source work unit with completed block list to next stage after
		// all blocks have been transferred.
		if err := g.Wait(); err != nil {
			return fmt.Errorf("error merging source %s: %w", su.objectName, err)
		}

		select {
		case out <- su:
		case <-ctx.Done():
			return ctx.Err()
		}
	}

	return nil
}

func (as *azureStorage) stageFanoutCopy(ctx context.Context, in <-chan azWorkUnit, targetName string) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	g, ctx := errgroup.WithContext(ctx)
	countc := make(chan int64)
	defer close(countc)

	go func() {
		var total int64
		var startTime = time.Now()
		// Log progress.
		for n := range countc {
			total += n
			rate := rateString(time.Since(startTime), total)
			logrus.Debugf("merged %s (%s) of object %s ...", ByteCountBinary(total), rate, targetName)
		}
	}()

	target := as.cli.ServiceClient().NewContainerClient(as.containerName).NewBlockBlobClient(targetName)

	for i := 0; i < azureMergeConcurrency; i++ {
		g.Go(func() error {
			for wu := range in {
				// Now call PutFromURL (why on earth is it called "Stage..."
				// instead of the REST API call "Put..." ?). If it does not
				// exist, the blob is created with a length of 0.
				_, err := target.StageBlockFromURL(ctx, wu.blockID, wu.source, &blockblob.StageBlockFromURLOptions{
					// This is where the magic happens. We need to split the
					// original object into sections of ranges and call
					// PutFromURL on each section. The last "chunk" has probably
					// a different size than the other chunks. When copying
					// Azure actually requests this byte range from the origin
					// server. In our case that's Azure itself which supports
					// Range headers. TODO(mathias.seiler): Find out how to make
					// this retryable.
					Range: blob.HTTPRange{
						Offset: wu.byteRange.offset,
						Count:  wu.byteRange.length,
					},
				})
				if err != nil {
					return fmt.Errorf("error copying from source url: %w", err)
				}
				// Send to progress routine non-blocking.
				select {
				case <-ctx.Done():
				case countc <- wu.byteRange.length:
				}
			}

			return nil
		})
	}

	return g.Wait()
}

// Returns the byte size of the object.
func (as *azureStorage) Size(ctx context.Context, objectName string) (int64, error) {
	scli := as.cli.ServiceClient().NewContainerClient(as.containerName).NewBlobClient(objectName)
	prop, err := scli.GetProperties(ctx, nil)
	if err != nil {
		return 0, fmt.Errorf("Size(): could not get object properties: %w", err)
	}
	return *prop.ContentLength, nil
}

func (as *azureStorage) Lifecycle() (Lifecycler, error) {
	if as.lifecycle == nil {
		return nil, errNotFound
	}
	return as.lifecycle, nil
}

func (as *azureStorage) Tag(ctx context.Context, objectName string, tags map[string]string) error {
	cli := as.cli.ServiceClient().NewContainerClient(as.containerName).NewBlobClient(objectName)

	// Fetch the existing tags and add the given tags to them (overwriting
	// existing tags on conflict).
	data, err := cli.GetProperties(ctx, nil)
	if err != nil {
		return fmt.Errorf("error getting blob object properties: %w", err)
	}

	// Merge the keys, match all keys on lowercase.
	meta := lowerKeys(unptr(data.Metadata))
	tags = lowerKeys(tags)
	for k, v := range tags {
		meta[k] = v
	}

	_, err = cli.SetMetadata(ctx, toptr(meta), nil)
	if err != nil {
		return fmt.Errorf("could not set blob object metadata: %w", err)
	}

	return nil
}
