package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/magnolia-backup/api"
)

func delayedResponse(delay time.Duration) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		time.Sleep(delay)
	}
}

func Test_findQuickestEndpoint(t *testing.T) {
	ts10 := httptest.NewTLSServer(delayedResponse(10 * time.Millisecond))
	defer ts10.Close()
	u, err := url.Parse(ts10.URL)
	if err != nil {
		t.Fatal(err)
	}
	addr10 := u.Host

	ts20 := httptest.NewTLSServer(delayedResponse(200 * time.Millisecond))
	defer ts20.Close()
	u, err = url.Parse(ts20.URL)
	if err != nil {
		t.Fatal(err)
	}
	addr200 := u.Host

	tsErr := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "meh", http.StatusInternalServerError)
	}))
	defer tsErr.Close()

	cli, err := api.New(
		api.WithHTTPClient(ts10.Client()),
	)
	if err != nil {
		t.Fatal(err)
	}

	type args struct {
		ctx    context.Context
		addrs  []string
		cli    *api.Client
		target string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "success, 10ms",
			args: args{
				ctx: context.TODO(),
				addrs: []string{
					addr10,
					addr200,
				},
				cli:    cli,
				target: "/test",
			},
			want: addr10,
		},
		{
			name: "success, 10ms, one target not resonding",
			args: args{
				ctx: context.TODO(),
				addrs: []string{
					addr10,
					tsErr.URL,
				},
				cli:    cli,
				target: "/test",
			},
			want:    addr10,
			wantErr: false,
		},
		{
			name: "should fail, no target ready",
			args: args{
				ctx: context.TODO(),
				addrs: []string{
					tsErr.URL,
				},
				cli:    cli,
				target: "/test",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := findQuickestEndpoint(tt.args.ctx, tt.args.addrs, tt.args.cli, tt.args.target)
			if (err != nil) != tt.wantErr {
				t.Errorf("findQuickestEndpoint() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("findQuickestEndpoint() = %v, want %v", got, tt.want)
			}
		})
	}
}

type mockResolver struct{}

func (m mockResolver) LookupSRV(ctx context.Context, service, proto, name string) (cname string, addrs []*net.SRV, err error) {
	results := []*net.SRV{
		{
			Target:   "pod-0.example.svc.cluster.local.",
			Port:     9998,
			Priority: 0,
			Weight:   50,
		},
		{
			Target:   "pod-1.example.svc.cluster.local.",
			Port:     9998,
			Priority: 0,
			Weight:   50,
		},
	}
	return name, results, nil
}

func Test_discoverEndpoints(t *testing.T) {
	type args struct {
		ctx         context.Context
		serviceName string
		resolver    srvResolver
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		{
			name: "simple",
			args: args{
				ctx:         context.TODO(),
				serviceName: "example.svc.cluster.local",
				resolver:    mockResolver{},
			},
			want: []string{
				"pod-0.example.svc.cluster.local:9998",
				"pod-1.example.svc.cluster.local:9998",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := discoverEndpointsWithResolver(tt.args.ctx, tt.args.serviceName, tt.args.resolver)
			if (err != nil) != tt.wantErr {
				t.Errorf("discoverEndpoints() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("discoverEndpoints() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Example_srvLookup() {
	// _collab-edge._tls.collab.mironet.ch
	r := &net.Resolver{
		PreferGo: true,
		Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
			d := net.Dialer{
				Timeout: time.Second * 10,
			}
			return d.DialContext(ctx, network, "1.1.1.1:53")
		},
	}
	cname, addrs, err := r.LookupSRV(context.Background(), "collab-edge", "tls", "collab.mironet.ch")
	if err != nil {
		logrus.Error(err)
		return
	}
	fmt.Println(cname, addrs)

	for _, addr := range addrs {
		fmt.Printf("%s:%d\n", strings.TrimSuffix(addr.Target, "."), addr.Port)
	}
}
