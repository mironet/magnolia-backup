package main

import "testing"

func Test_prependStorageName(t *testing.T) {
	type args struct {
		name string
		s    string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "simple test",
			args: args{
				name: "testStorage",
				s:    "myprefix/01G9M0MJ5CKKTDNZ610ZRFQ9HN",
			},
			want: "testStorage://myprefix/01G9M0MJ5CKKTDNZ610ZRFQ9HN",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := prependStorageName(tt.args.name, tt.args.s); got != tt.want {
				t.Errorf("prependStorageName() = %v, want %v", got, tt.want)
			}
		})
	}
}
