package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/klauspost/compress/gzip"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	podNameEnvVar     = "MGNLBACKUP_TAGS_POD_NAME"
	namespaceEnvVar   = "MGNLBACKUP_TAGS_NAMESPACE"
	pgVersionFileName = "PG_VERSION"
)

func stringToString(in string) (map[string]string, error) {
	items := strings.Split(in, ",")
	if len(items) == 0 {
		return nil, nil
	}
	out := make(map[string]string)
	for _, v := range items {
		kv := strings.Split(v, "=")
		if len(kv) != 2 {
			return nil, fmt.Errorf("%q should be in key=value format", v)
		}
		out[kv[0]] = kv[1]
	}
	return out, nil
}

var dumpCmd = &cobra.Command{
	Use:   "dump",
	Short: "dump databases and store to object storage target(s)",
	RunE: func(cmd *cobra.Command, _ []string) error {
		vip := vipers["dump"]

		if vip.GetString("heritage") != "" {
			logrus.Warnf("heritage flag is deprecated: its value (%s) will be ignored", vip.GetString("heritage"))
		}

		command := vip.GetString("cmd")
		args := vip.GetStringSlice("args")

		bucketName := vip.GetString("bucket")
		mode := vip.GetString("mode")

		httpAddress := vip.GetString("address")
		metricsAddress := vip.GetString("metrics-address")

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		prefix := vip.GetString("prefix")
		prefix = os.ExpandEnv(prefix)
		compression := vip.GetInt("compression")
		switch compression {
		case gzip.NoCompression:
			logrus.Debugf("using no compression (still producing a gzip file)")
		case gzip.HuffmanOnly:
			logrus.Debugf("using Huffman only compression")
		case gzip.DefaultCompression:
			logrus.Debugf("using default compression level")
		default:
			logrus.Debugf("using compression level %d", compression)
		}

		readonly := vip.GetBool("readonly")
		if readonly {
			logrus.Infof("starting in read only mode")
		}

		cron := vip.GetString("cron")

		conf, err := ReadViperFlags(vip)
		if err != nil {
			return fmt.Errorf("could not read object storage viper flags: %w", err)
		}
		if err := conf.Normalize(); err != nil {
			return fmt.Errorf("could not normalize storage config: %w", err)
		}
		stor, err := conf.CreateFrom(ctx)
		if err != nil {
			return fmt.Errorf("error creating object storage: %w", err)
		}
		defer stor.Close()

		pgDataPath := vip.GetString("pg-data")
		usePgWAL := vip.GetBool("use-pg-wal")
		var pgVersion string
		if usePgWAL && !readonly {
			bytes, err := os.ReadFile(filepath.Join(pgDataPath, pgVersionFileName))
			if err != nil {
				logrus.Errorf("could not read pg version file: %v", err)
				return nil
			}
			pgVersion = string(bytes)
			pgVersion = strings.TrimSuffix(pgVersion, "\n")
		}
		srv := &server{
			storage:            stor,
			prefix:             prefix,
			command:            command,
			args:               args,
			cronSchedule:       cron,
			pgDataPath:         pgDataPath,
			pgDbName:           vip.GetString("pg-name"),
			pgUserName:         vip.GetString("pg-user"),
			pgPassword:         vip.GetString("pg-pass"),
			pgHostName:         vip.GetString("pg-host"),
			usePgWAL:           usePgWAL,
			pgVersion:          pgVersion,
			compressionLevel:   compression,
			readOnly:           readonly,
			dumpTimeout:        vip.GetDuration("dump-timeout"),
			uploadReadLimit:    vip.GetInt("upload-read-limit") * Mi,
			partsSweepSchedule: vip.GetString("parts-sweep-schedule"),
			partsSweepTimeout:  vip.GetDuration("parts-sweep-timeout"),
			partsSweepDelay:    vip.GetDuration("parts-sweep-delay"),
			txlogSizeThreshold: vip.GetInt64("tx-log-size-threshold") * Gi,
		}
		srv.setup() // Initialize the server.
		if err := srv.setupCron(); err != nil {
			return fmt.Errorf("error setting up cron: %s", err)
		}

		// Setup webhooks.
		if gitlabAddress := vip.GetString("gitlab-address"); gitlabAddress != "" {
			// Parse vars.
			vars, err := stringToString(vip.GetString("gitlab-vars"))
			if err != nil {
				return err
			}
			logrus.Infof("setting up gitlab webhook")
			hook, err := newGitlabWebhook(
				"gitlab",
				gitlabAddress,
				vip.GetString("gitlab-project"),
				vip.GetString("gitlab-ref"),
				vip.GetString("gitlab-token"),
				bucketName,
				vars,
			)
			if err != nil {
				return err
			}
			srv.webhooks = append(srv.webhooks, hook)
		}

		// Setup directory sync.
		dirsync := vip.GetString("sync-dir")
		if dirsync != "" {
			srv.syncDirPath = dirsync
			if err := srv.StartTxLogDirSync(ctx); err != nil {
				return fmt.Errorf("error starting dir sync of directory %s: %w", dirsync, err)
			}
		}

		// Setup tx log target path for restores.
		txLogPath := vip.GetString("tx-log-path")
		if txLogPath != "" {
			srv.txLogPath = txLogPath
		}

		switch mode {
		case "server":
			// Make sure the required env vars for pg wal archiving are set.
			// (This must not be enforced in read-only mode.)
			if srv.usePgWAL && !readonly {
				if os.Getenv(podNameEnvVar) == "" {
					logrus.Errorf("env var %s cannot be empty", podNameEnvVar)
					return nil
				}
				if os.Getenv(namespaceEnvVar) == "" {
					logrus.Errorf("env var %s cannot be empty", namespaceEnvVar)
					return nil
				}
			}
			// Start server mode.
			var wg sync.WaitGroup
			wg.Add(1)
			go func() {
				defer wg.Done()
				logrus.Fatal(srv.Run(httpAddress))
			}()
			tlsAddress := vip.GetString("tls-address")
			if tlsAddress != "" {
				caCertFile := vip.GetString("tls-ca-cert")
				certFile := vip.GetString("tls-cert")
				keyFile := vip.GetString("tls-key")
				wg.Add(1)
				go func() {
					defer wg.Done()
					logrus.Fatal(srv.RunMutualTLS(tlsAddress, caCertFile, certFile, keyFile))
				}()
			}
			wg.Add(1)
			go func() {
				wg.Done()
				logrus.Fatal(metricsServerRun(metricsAddress))
			}()
			wg.Wait()
			return nil
		default:
			// Collect one dump.
			if srv.usePgWAL {
				logrus.Fatal("pg_wal enabled, not intended for this mode")
			}
			dumpCtx, dumpCtxCancel := context.WithTimeout(ctx, srv.dumpTimeout)
			defer dumpCtxCancel()
			_, err := srv.getNewDump(dumpCtx, false)
			if err != nil {
				logrus.Error(err)
				return nil
			}
		}

		return nil
	},
}

func getLifecycle(cycle string, usePgWal bool, keepdays int,
	maxSweepRepInterval time.Duration, stor ObjectStorage) (Lifecycler, error) {
	if usePgWal {
		keep := 24 * time.Hour * time.Duration(keepdays)
		lifecycle, err := NewContinuousLifecycle(keep, maxSweepRepInterval, stor)
		if err != nil {
			return nil, err
		}
		logrus.Infof("created new continuous lifecycle with keep=%s", keep)
		return lifecycle, nil
	}
	lifecycle, err := NewLifecycle(cycle, stor)
	if err != nil {
		return nil, err
	}
	return lifecycle, nil
}

func getMinioClient(endpoint, region, access, secret string, secure, skipVerify bool) (*minio.Client, error) {
	opts := &minio.Options{
		Creds:  credentials.NewStaticV4(access, secret, ""),
		Secure: secure,
		Region: region,
	}
	if skipVerify {
		// Get the default minio http transport.
		trsp, err := minio.DefaultTransport(secure)
		if err != nil {
			return nil, fmt.Errorf("could not create default minio http transport: %w", err)
		}
		if trsp.TLSClientConfig == nil {
			trsp.TLSClientConfig = &tls.Config{
				MinVersion: tls.VersionTLS12, // Still insist on that at least.
			}
		}
		trsp.TLSClientConfig.InsecureSkipVerify = true
		opts.Transport = trsp
	}
	cli, err := minio.New(endpoint, opts)
	if err != nil {
		return nil, fmt.Errorf("error getting minio client: %w", err)
	}

	return cli, nil
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["dump"] = v

	flags := dumpCmd.Flags()

	// General flags.
	flags.String("cmd", "", "Command producing a valid backup")
	flags.StringSlice("args", nil, "args to command for producing backup")
	flags.StringP("address", "a", "localhost:9999", "Where to listen on") // We listen on localhost to not expose stuff accidentally.
	flags.String("metrics-address", ":9997", "Where to listen on")
	flags.String("heritage", "", "Heritage flag is deprecated. Its value will be ignored.")
	flags.Bool("ignore-heritage", false, "Ignore heritage flag is deprecated. Its value will be ignored.")
	flags.String("mode", "server", "'server' in case you want to start as a server")
	flags.String("cron", "@every 24h", "cron-compatible string for backup schedule")
	flags.Duration("dump-timeout", 10*time.Hour, "Timeout (max duration) for a single dump operation")
	flags.String("parts-sweep-schedule", "@every 10h", "cron-compatible string for part sweeping schedule")
	flags.Duration("parts-sweep-delay", 48*time.Hour, "Parts will not get swept as long as they are not older than this duration")
	flags.Duration("parts-sweep-timeout", 10*time.Hour, "Timeout (max duration) for a single part sweep operation")
	flags.IntP("compression", "z", gzip.DefaultCompression, "Compression level (1 = best speed, 9 = best compression)")
	flags.Int("upload-read-limit", 256, "Maximum size of parts (in MB) during upload of a base backup (or transaction log)")
	flags.Int64("tx-log-size-threshold", 16, "Threshold for merged transaction log size (in GiB). If the size of the \"to be merged\" transaction log exceeds this threshold, it will not be merged.")

	// TLS flags. This is needed for the /download endpoint (mTLS).
	flags.String("tls-address", "", "Address for mTLS server, e.g. ':9998'") // This is secure by default, we can expose it.
	flags.String("tls-ca-cert", "/opt/tls/ca.crt", "Path to CA cert PEM file.")
	flags.String("tls-cert", "/opt/tls/tls.crt", "Path to server certificate PEM file.")
	flags.String("tls-key", "/opt/tls/tls.key", "Path to private key PEM file (unencrypted).")

	// PostgresSQL WAL archiving flags.
	flags.StringP("sync-dir", "d", "", "Directory to continuously sync to the cloud target (use for continuous archiving of WAL logs)")
	flags.String("pg-data", defaultPgDataPath, "Where postgres data is stored at.")
	flags.String("pg-name", "postgres", "Data base name to connect to.")
	flags.String("pg-user", "postgres", "User to connect to db for pg_wal.")
	flags.String("pg-pass", "", "Password to connect to db for pg_wal.")
	flags.String("pg-host", "localhost:5432", "Host to connect to db for pg_wal.")
	flags.String("tx-log-path", "archive", "Path relative to $PGDATA where the tx logs are restored to.")

	// Webhooks fired after successful backups.
	// Gitlab.
	flags.String("gitlab-address", "", "Where to send gitlab pipeline triggers to (https://gitlab.example.com)")
	flags.String("gitlab-project", "", "Which gitlab project to trigger")
	flags.String("gitlab-ref", "", "Which ref in the gitlab project to trigger")
	flags.String("gitlab-token", "", "Gitlab token to use with the request")
	// flags.StringToString does not work with env vars: https://github.com/spf13/viper/issues/911
	flags.String("gitlab-vars", "", "Vars to send along the trigger http request as params (key=value,key2=value2,...). Use '%f' as a placeholder for the backup object name and '%b' as the bucket's name.")

	// Object storage flags.
	AddObjectStorageFlags(flags)

	// Keep stuff compatible.
	v.RegisterAlias("s3-bucket", "bucket")
	v.RegisterAlias("gcs-bucket", "bucket")
	v.RegisterAlias("s3-keepdays", "keepdays")
	v.RegisterAlias("s3-cycle", "cycle")
	v.RegisterAlias("s3-prefix", "prefix")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

	rootCmd.AddCommand(timeCmdMiddleware(dumpCmd))
}
