package main

import (
	_ "net/http/pprof"
	"testing"
)

func Test_isBackupHistoryFile(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: ".backup file",
			args: args{
				name: "000000010000000000000042.00000028.backup",
			},
			want: true,
		},
		{
			name: ".history file",
			args: args{
				name: "00000001.history",
			},
			want: true,
		},
		{
			name: "wal segment file",
			args: args{
				"0000000100001234000055CD",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isBackupHistoryFile(tt.args.name); got != tt.want {
				t.Errorf("isBackupHistoryFile() = %v, want %v", got, tt.want)
			}
		})
	}
}
