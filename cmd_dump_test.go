package main

import (
	"reflect"
	"testing"
)

func Test_stringToString(t *testing.T) {
	type args struct {
		in string
	}
	tests := []struct {
		name    string
		args    args
		want    map[string]string
		wantErr bool
	}{
		{
			name: "key-value",
			args: args{in: "key=value"},
			want: map[string]string{
				"key": "value",
			},
			wantErr: false,
		},
		{
			name: "multiple key-value",
			args: args{in: "key=value,key2=value2"},
			want: map[string]string{
				"key":  "value",
				"key2": "value2",
			},
			wantErr: false,
		},
		{
			name:    "incomplete key-value",
			args:    args{in: "key=value,key2"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := stringToString(tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("stringToString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil {
				t.Log(err)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("stringToString() = %v, want %v", got, tt.want)
			}
		})
	}
}
