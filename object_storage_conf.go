package main

import (
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob"
	"github.com/cenkalti/backoff/v4"
	"github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

type ObjectStorageConfig struct {
	readonly            bool
	bucketName          string
	prefix              string
	cycle               string
	keepdays            int
	usePgWal            bool
	maxSweepRepInterval time.Duration
	gcs                 GcsConfig
	multisource         MultisourceConfig
	azure               AzureConfig
	s3                  S3Config
}

func (conf ObjectStorageConfig) Validate() error {
	if conf.bucketName == "" {
		return fmt.Errorf("bucket name cannot be empty")
	}

	enabledFlagList := []bool{
		conf.gcs.enabled,
		conf.multisource.enabled,
		conf.azure.enabled,
		conf.s3.enabled,
	}
	numEnabled := 0
	for _, isEnabled := range enabledFlagList {
		if isEnabled {
			numEnabled = numEnabled + 1
		}
	}
	if numEnabled != 1 {
		return fmt.Errorf("expect exactly 1 storage type to be enabled, but %d are", numEnabled)
	}

	if conf.gcs.enabled {
		return conf.gcs.validate()
	}
	if conf.azure.enabled {
		return conf.azure.validate()
	}
	if conf.s3.enabled {
		return conf.s3.validate()
	}
	if conf.multisource.enabled {
		return conf.multisource.validate()
	}

	return nil
}

func (conf ObjectStorageConfig) CreateFrom(ctx context.Context) (ObjectStorage, error) {
	if err := conf.Validate(); err != nil {
		return nil, fmt.Errorf("given config is invalid: %w", err)
	}

	var stor ObjectStorage
	switch {
	case conf.s3.enabled:
		// Create a minio client.
		cli, err := getMinioClient(conf.s3.endpoint, conf.s3.region, conf.s3.accessKey, conf.s3.secretKey, !conf.s3.insecure, conf.s3.insecureSkipVerify)
		if err != nil {
			return nil, err
		}
		s3, err := newS3Storage(cli,
			withS3Prefix(conf.prefix),
			withS3Region(conf.s3.region),
			withS3BucketName(conf.bucketName),
			withObjectTaggingFallback(true),
		)
		if err != nil {
			return nil, err
		}
		stor = s3

	case conf.azure.enabled:
		var cli *azblob.Client
		if conf.azure.accountKey == "" {
			return nil, fmt.Errorf("account key not set")
		}
		if conf.azure.accountName == "" {
			// This should not happen, but just to be sure.
			return nil, fmt.Errorf("account key set but missing account name")
		}
		cred, err := azblob.NewSharedKeyCredential(conf.azure.accountName, conf.azure.accountKey)
		if err != nil {
			return nil, fmt.Errorf("could not create new shared key credential: %w", err)
		}
		cli, err = azblob.NewClientWithSharedKeyCredential(fmt.Sprintf(azAccountURLTpl, conf.azure.accountName), cred, nil)
		if err != nil {
			return nil, fmt.Errorf("could not create new azblob client: %w", err)
		}
		var opts = []azureStorageOpt{
			withContainerName(conf.bucketName),
			withAzurePrefix(conf.prefix),
		}
		azure, err := newAzureStorage(cli, opts...)
		if err != nil {
			return nil, fmt.Errorf("could not create new azure client: %w", err)
		}
		stor = azure

	case conf.gcs.enabled:
		client, err := storage.NewClient(ctx)
		if err != nil {
			return nil, err
		}
		gcs, err := newGCSStorage(client,
			withGCSPrefix(conf.prefix),
			withGCSProjectID(conf.gcs.projectID),
			withGCSLocation(
				conf.gcs.location,
				conf.gcs.locationType,
			),
			withBucketName(conf.bucketName),
		)
		if err != nil {
			return nil, err
		}
		stor = gcs

	case conf.multisource.enabled:
		out := &multiStor{
			stores: make(map[string]ObjectStorage),
		}
		// Create google cloud storages.
		for name, c := range conf.multisource.conf.GCSConfigs {
			cfg := objectStorageConfigFromGCS(*c, conf.readonly)
			gcs, err := cfg.CreateFrom(ctx)
			if err != nil {
				return nil, fmt.Errorf("could not create gcs %s: %w", name, err)
			}
			out.stores[name] = gcs
		}
		// Create s3 storages.
		for name, c := range conf.multisource.conf.S3Configs {
			// Make sure that there are no duplicate storage names.
			if _, ok := out.stores[name]; ok {
				return nil, fmt.Errorf("found duplicate storage name %s", name)
			}
			cfg := objectStorageConfigFromS3(*c, conf.readonly)
			s3, err := cfg.CreateFrom(ctx)
			if err != nil {
				return nil, fmt.Errorf("could not create s3 storage %s: %w", name, err)
			}
			out.stores[name] = s3
		}
		// Create Azure storages.
		for name, c := range conf.multisource.conf.AZConfigs {
			if _, ok := out.stores[name]; ok {
				return nil, fmt.Errorf("found duplicate storage name %s", name)
			}
			cfg := objectStorageConfigFromAzure(*c, conf.readonly)
			az, err := cfg.CreateFrom(ctx)
			if err != nil {
				return nil, fmt.Errorf("could not create azure storage %s: %w", name, err)
			}
			out.stores[name] = az
		}
		stor = out

	default:
		return nil, fmt.Errorf("given config is invalid: no storage type enabled")
	}

	// Set lifecycle (if required).
	var lifecycle Lifecycler
	if !conf.readonly &&
		(conf.gcs.enabled || conf.azure.enabled || conf.s3.enabled) {
		var err error
		lifecycle, err = getLifecycle(conf.cycle, conf.usePgWal, conf.keepdays, conf.maxSweepRepInterval, stor)
		if err != nil {
			return nil, err
		}
		s, ok := stor.(LifecyclerSetter)
		if ok {
			if err := s.SetLifecycler(lifecycle); err != nil {
				return nil, err
			}
		}
	}

	logrus.Infof("preparing upload location, bucket = %s, region = %s", conf.bucketName, defaultRegion)
	wait, waitCancel := context.WithTimeout(ctx, time.Hour)
	defer waitCancel()

	bo := backoff.WithContext(backoff.NewExponentialBackOff(), wait)
	err := backoff.Retry(func() error {
		logrus.Debugf("trying to connect to remote storage ...")
		err := stor.Prepare(wait)
		if err != nil {
			logrus.Debugf("error connecting to remote storage, retrying: %v", err)
		}
		return err
	}, bo)
	if err != nil {
		return nil, fmt.Errorf("error while preparing remote storage: %w", err)

	}
	logrus.Infof("successfully initialized remote storage location")

	return stor, nil
}

type GcsConfig struct {
	enabled       bool
	projectID     string
	location      string
	locationType  string
	defaultRegion string
}

func (conf GcsConfig) validate() error {
	// All members must be present.
	if conf.projectID == "" {
		return fmt.Errorf("project id cannot be empty")
	}
	if conf.location == "" {
		return fmt.Errorf("location cannot be empty")
	}
	if conf.locationType == "" {
		return fmt.Errorf("location type cannot be empty")
	}
	return nil
}

type MultisourceConfig struct {
	enabled   bool
	yamlPaths string
	conf      multiStorConfig
}

func (conf MultisourceConfig) validate() error {
	if len(conf.yamlPaths) == 0 {
		return fmt.Errorf("at least one config file path required")
	}
	return nil
}

type AzureConfig struct {
	enabled          bool
	accountName      string
	accountKey       string
	mergeConcurrency int
}

func (conf AzureConfig) validate() error {
	if conf.accountName == "" {
		return fmt.Errorf("account name cannot be empty")
	}
	if conf.accountKey == "" {
		return fmt.Errorf("account key cannot be empty")
	}
	return nil
}

type S3Config struct {
	enabled            bool
	region             string
	endpoint           string
	accessKey          string
	secretKey          string
	insecure           bool
	insecureSkipVerify bool
}

func (conf S3Config) validate() error {
	// Only insecure and insecureSkipVerify are allowed to be empty.
	if conf.endpoint == "" {
		return fmt.Errorf("endpoint cannot be empty")
	}
	if conf.region == "" {
		return fmt.Errorf("region cannot be empty")
	}
	if conf.accessKey == "" {
		return fmt.Errorf("access key cannot be empty")
	}
	if conf.secretKey == "" {
		return fmt.Errorf("secret key cannot be empty")
	}
	return nil
}

type ObjectStorageConfigArgs struct {
	Readonly            bool
	Bucket              string
	Prefix              string
	Cycle               string
	Keepdays            int
	UsePgWal            bool
	MaxSweepRepInterval time.Duration
	GCS                 struct {
		ProjectID    string
		Location     string
		LocationType string
	}
	Multisource struct {
		Enabled   bool
		YamlPaths string
	}
	Azure struct {
		AccountName      string
		AccountKey       string
		MergeConcurrency int
	}
}

func AddObjectStorageFlags(flags *pflag.FlagSet) {
	flags.Bool("readonly", false, "Do not alter the target object storage with the exception of uploading restore bundles (basically JSON artifacts with metadata).")
	flags.String("bucket", "mgnl-backup", "Object storage bucket/container name")
	flags.String("prefix", "", "String to append before the date in object names on object storage")
	flags.String("cycle", "15,4,3", "Object retention cycle in the format [daily,weekly,monthly], overrides 'keepdays', ignored if --use-pg-wal=true")
	flags.Int("keepdays", 0, "Keep this many days of backups max. 0 means do not delete anything. Also used if --use-pg-wal=true")
	flags.Bool("use-pg-wal", false, "Use PostgreSQL WAL archiving to object storage")
	flags.Duration("max-sweep-rep-interval", time.Hour, "The duration that must (at least) pass between two runs of the backup sweeping procedure.")

	// GCS-specific flags.
	flags.String("gcs-projectid", "", "Google Cloud Storage project ID (see Google Console -> Storage -> Settings -> Project Access)")
	flags.String("gcs-location", "EUROPE-WEST6", "Region where to create the bucket if not yet present")
	flags.String("gcs-location-type", "region", "Replication type (multi-region, region or dual-region)")

	// Multisource specific flags.
	flags.Bool("multisource", false, "Set to true if object storages are defined in a yaml referenced by 'multisource-yaml-path'.")
	flags.String("multisource-yaml-paths", "", "Semicolon separated list of paths pointing to files containing the multisource storage configuration. Configs of all files are merged. Configs from later files overwrite configs from earlier files.")

	// Azure Blob Storage flags.
	flags.String("az-account-name", "", "Azure storage account name")
	flags.String("az-account-key", "", "Azure storage account shared key")
	flags.Int("az-merge-concurrency", azureMergeConcurrency, "Azure merge operation concurrency level")

	// S3-specific flags.
	flags.String("s3-endpoint", "minio:9000", "S3 endpoint address to upload backups to")
	flags.String("s3-accesskey", "", "S3 server access key")
	flags.String("s3-secretkey", "", "S3 server secret key")
	flags.String("s3-region", defaultRegion, "S3 region")
	flags.Bool("s3-insecure", false, "Connect to http:// instead of https://")
	flags.Bool("s3-insecure-skip-verify", false, "Don't check S3 server's TLS certificate (⚠)")
}

func ReadViperFlags(vip *viper.Viper) (ObjectStorageConfig, error) {
	out := ObjectStorageConfig{}
	if vip == nil {
		return out, fmt.Errorf("given viper pointer cannot be nil")
	}

	// General.
	out.readonly = vip.GetBool("readonly")
	out.bucketName = vip.GetString("bucket")
	out.prefix = vip.GetString("prefix")
	out.cycle = vip.GetString("cycle")
	out.keepdays = vip.GetInt("keepdays")
	out.usePgWal = vip.GetBool("use-pg-wal")
	out.maxSweepRepInterval = vip.GetDuration("max-sweep-rep-interval")

	// GCS.
	out.gcs = GcsConfig{
		projectID:    vip.GetString("gcs-projectid"),
		location:     vip.GetString("gcs-location"),
		locationType: vip.GetString("gcs-location-type"),
	}

	// Multisource.
	out.multisource = MultisourceConfig{
		enabled:   vip.GetBool("multisource"),
		yamlPaths: vip.GetString("multisource-yaml-paths"),
	}

	// Azure.
	out.azure = AzureConfig{
		accountName:      vip.GetString("az-account-name"),
		accountKey:       vip.GetString("az-account-key"),
		mergeConcurrency: vip.GetInt("az-merge-concurrency"),
	}

	// S3.
	out.s3 = S3Config{
		region:             vip.GetString("s3-region"),
		endpoint:           vip.GetString("s3-endpoint"),
		accessKey:          vip.GetString("s3-accesskey"),
		secretKey:          vip.GetString("s3-secretkey"),
		insecure:           vip.GetBool("s3-insecure"),
		insecureSkipVerify: vip.GetBool("s3-insecure-skip-verify"),
	}

	return out, nil
}

func (conf *ObjectStorageConfig) Normalize() error {
	if conf == nil {
		return fmt.Errorf("object storage config pointer cannot be nil")
	}

	// Normalize shared config.
	conf.prefix = os.ExpandEnv(conf.prefix)
	if conf.keepdays > 0 {
		conf.cycle = fmt.Sprintf("%d,0,0", conf.keepdays)
	}

	// Normalize GCS specific config.
	conf.gcs.enabled = conf.gcs.projectID != "" || os.Getenv("GOOGLE_APPLICATION_CREDENTIALS") != ""
	conf.gcs.defaultRegion = conf.gcs.location

	// Normalize Azure specific config.
	conf.azure.enabled = conf.azure.accountName != ""

	// S3 specific config.
	conf.s3.enabled = !conf.gcs.enabled && !conf.multisource.enabled && !conf.azure.enabled

	switch {
	case conf.multisource.enabled:
		logrus.Infof("using multisource object storage ...")
		// We use multiple object storage sources.
		if !conf.readonly {
			return fmt.Errorf("multisource storage only available in readonly mode")
		}
		// Make sure there is always a prefix defined for the multisource
		// server. It is only used when generating the json bundle names.
		// (Note: This is not the prefix used for the "sub"-storages. The
		// prefixes of the "sub" storages are configured in the multistore
		// config yaml.)
		if conf.prefix == "" {
			conf.prefix = "multisource"
		}
		var err error
		conf.multisource.conf, err = composeMultisourceConf(strings.Split(conf.multisource.yamlPaths, ";"), conf.readonly)
		if err != nil {
			return fmt.Errorf("could not compose multisource configuration: %w", err)
		}
	case conf.gcs.enabled:
		logrus.Infof("using Google Cloud Storage ...")
		defaultRegion = conf.gcs.defaultRegion
	case conf.azure.enabled:
		logrus.Infof("using Azure Blob Storage ...")
		logrus.Infof("az: merge concurrency: %d", azureMergeConcurrency)
		azureMergeConcurrency = conf.azure.mergeConcurrency
	case conf.s3.enabled:
		logrus.Infof("using S3 Object Storage ...")
		if conf.s3.insecureSkipVerify {
			logrus.Warn("⚠ using insecure skip certificate verify option ⚠")
		}
		defaultRegion = conf.s3.region
	}

	return nil
}
