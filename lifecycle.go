package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

type LifecyclerSetter interface {
	SetLifecycler(lifecycler Lifecycler) error
}

// Lifecycler does backup housekeeping.
type Lifecycler interface {
	Marker
	Sweeper
}

// Sweeper deletes old backups.
type Sweeper interface {
	Sweep(ctx context.Context, t time.Time) error
}

// Marker returns tags of new backup which should be used to mark it.
type Marker interface {
	Mark(context.Context) (map[string]string, error)
}

// ExpirationTimer returns an expiration time for an object that is created at
// the time t.
type ExpirationTimer interface {
	ExpirationTime(t time.Time) time.Time
}

// SimpleLifecycle maintains a daily, weekly and monthly backup schedule,
// implementing the grandfather-father-son backup scheme.
type SimpleLifecycle struct {
	daily   int // Keep x days of backups.
	weekly  int // Keep x weeks of backups.
	monthly int // Keep x months of backups.

	stor ObjectStorage // We interact with this to sweep/move old backups.
}

type lastSwept struct {
	t              time.Time
	mu             sync.Mutex
	maxRepInterval time.Duration
}

func (l *lastSwept) atTime() time.Time {
	if l == nil {
		return time.Time{}
	}
	l.mu.Lock()
	defer l.mu.Unlock()
	return l.t
}

func (l *lastSwept) set(t time.Time) {
	if l == nil {
		return
	}
	l.mu.Lock()
	defer l.mu.Unlock()
	l.t = t
}

func (l *lastSwept) repInterval() time.Duration {
	if l == nil {
		return 0
	}
	return l.maxRepInterval
}

// ContinuousLifecycle is used for continous backup scenarios like tx log
// archiving (pg_wal mode).
type ContinuousLifecycle struct {
	max       time.Duration // Maximum amount of time we keep (base) backups and associated tx logs.
	stor      ObjectStorage // We interact with this to sweep/move old backups.
	lastSwept *lastSwept
}

// NewLifecycle returns a new lifecycle manager.
func NewLifecycle(cycle string, stor ObjectStorage) (*SimpleLifecycle, error) {
	l := &SimpleLifecycle{
		stor: stor,
	}
	var err error
	l.daily, l.weekly, l.monthly, err = parseLifecycle(cycle)
	if err != nil {
		return nil, err
	}
	return l, nil
}

// NewContinuousLifecycle returns a new lifecycle manager for use with tx log
// archiving.
func NewContinuousLifecycle(max, maxSweepRepInterval time.Duration, stor ObjectStorage) (*ContinuousLifecycle, error) {
	if max < 0 {
		return nil, fmt.Errorf("unexpected lifecycle duration of %s, want >= 0", max)
	}
	if max == 0 {
		logrus.Warnf("lifecycle keepdays is set to 0, backup will grow indefinitely")
	}
	l := &ContinuousLifecycle{
		max:  max,
		stor: stor,
		lastSwept: &lastSwept{
			maxRepInterval: maxSweepRepInterval,
		},
	}
	return l, nil
}

func parseLifecycle(cycle string) (daily, weekly, monthly int, err error) {
	cc := strings.Split(cycle, ",")
	if len(cc) != 3 {
		return 0, 0, 0, fmt.Errorf("unexpected lifecycle string %s, should be in the form daily,weekly,monthly (e.g. 15,4,3)", cycle)
	}
	daily, err = strconv.Atoi(cc[0])
	if err != nil {
		return
	}
	weekly, err = strconv.Atoi(cc[1])
	if err != nil {
		return
	}
	monthly, err = strconv.Atoi(cc[2])
	if err != nil {
		return
	}
	switch {
	case daily < 0, weekly < 0, monthly < 0:
		return 0, 0, 0, fmt.Errorf("no negative interval allowed, cycle invalid: %s", cycle)
	case daily+weekly+monthly < 1:
		return 0, 0, 0, fmt.Errorf("all periods zero, cycle invalid: %s", cycle)
	}
	return
}

const (
	backupDaily    = "daily"
	backupWeekly   = "weekly"
	backupMonthly  = "monthly"
	txLogArchiving = "tx_log_archiving"
	variantTagName = "variant"
)

// Sweep old backups (non-pg_wal mode).
func (s *SimpleLifecycle) Sweep(ctx context.Context, t time.Time) error {
	list, err := s.stor.List(ctx)
	if err != nil {
		return fmt.Errorf("sweep(): could not get list of current backups: %w", err)
	}
	sort.Sort(byTimeFromKey(list))
	tooOld := make(chan string)
	errc := make(chan error, 1)

	go func() {
		defer close(tooOld)
		defer close(errc)
		for _, v := range list {
			vt, err := v.TimeFromKey()
			if err != nil {
				errc <- err
				return
			}
			if v.Monthly {
				if vt.Before(t.AddDate(0, s.monthly*-1, 0)) {
					v.Monthly = false
				}
			}
			if v.Weekly {
				if vt.Before(t.AddDate(0, 0, s.weekly*7*-1)) {
					v.Weekly = false
				}
			}
			if v.Daily {
				if vt.Before(t.AddDate(0, 0, s.daily*-1)) {
					v.Daily = false
				}
			}

			if !(v.Daily || v.Weekly || v.Monthly) {
				logrus.Infof("removing old backup %s, created on %s", v.Key, vt)
				select {
				case tooOld <- v.Key: // Send for deletion, the object has no retention tags left.
				case <-ctx.Done():
					return
				}
			}
		}
	}()

	// Remove older backups from object storage.
	for err := range s.stor.Delete(ctx, tooOld) {
		return fmt.Errorf("error while removing old backup %w", err)
	}

	return <-errc
}

// Sweep does delete backups older than the configured max duration.
func (c *ContinuousLifecycle) Sweep(ctx context.Context, t time.Time) error {
	// Sweep at most every defined interval.
	lastSwept := c.lastSwept.atTime()
	interval := c.lastSwept.repInterval()
	since := time.Since(lastSwept)
	if !lastSwept.IsZero() && since < interval {
		logrus.Infof("not sweeping because last swept %s ago (sweeping at most every %s)", since, interval)
		return nil
	}

	list, err := c.stor.List(ctx)
	if err != nil {
		return fmt.Errorf("sweep(): could not get list of current backups: %w", err)
	}
	sort.Sort(byULID(list))
	tooOld := make(chan string)

	go func() {
		defer close(tooOld)
		curBundle, err := byULID(list).Section(t)
		if err != nil {
			logrus.Errorf("could not remove old backups: error fetching current bundle: %v", err)
			return
		}
		backupID := curBundle[0].Tags[tagKeyBackupID]
		for _, v := range list {
			if v.Tags[tagKeyBackupID] == backupID {
				// If this object is part of the current section of backup
				// objects (basebackup + txlogs), do not delete it yet.
				continue
			}
			expiryStr := v.Tags[tagKeyExpiry]
			if expiryStr == "" {
				// If no expiry is defined, we don't need to remove the backup.
				continue
			}
			expiry, err := time.Parse(time.RFC3339, expiryStr)
			if err != nil {
				logrus.Errorf("could not parse expiry string (%v): continue with next list element", err)
				continue
			}
			if !expiry.Before(t) {
				// If the expiry is exactly t or after t, we cannot remove
				// the backup.
				continue
			}
			logrus.Infof("removing old backup %s, last modified on %s", v.Key, v.LastModified)
			select {
			case tooOld <- v.Key: // Send off for deletion.
			case <-ctx.Done():
				return
			}
		}
	}()

	// Remove older backups from object storage.
	for err := range c.stor.Delete(ctx, tooOld) {
		return fmt.Errorf("error while removing old backup: %w", err)
	}

	// Delete older backup bundles (if possible).
	if bundleLister, ok := c.stor.(interface {
		ListBundles(ctx context.Context) (backupItemList, error)
	}); ok {
		bundleList, err := bundleLister.ListBundles(ctx)
		if err != nil {
			return fmt.Errorf("could not list bundles: %w", err)
		}
		logrus.Info("start bundle deletion")
		if err := DeleteOlderBundles(ctx, c.stor, bundleList, c.max); err != nil {
			return fmt.Errorf("could not delete older bundles: %w", err)
		}
	} else {
		logrus.Warnf("cannot delete bundles: storage of type %T does not implement ListBundles()", c.stor)
	}
	c.lastSwept.set(time.Now())
	return nil
}

// Mark for classic backup cycles.
func (s *SimpleLifecycle) Mark(ctx context.Context) (map[string]string, error) {
	now := time.Now()
	out := map[string]string{
		backupDaily: "true",
	}
	list, err := s.stor.List(ctx)
	if err != nil {
		return nil, fmt.Errorf("mark(): could not get list of current backups: %w", err)
	}
	lastWeek := now.AddDate(0, 0, -7)
	lastMonth := now.AddDate(0, -1, 0)
	if !isWithin(list, lastWeek, weeklyBackups) {
		// We have no weekly backup, set a tag then.
		out[backupWeekly] = "true"
	}
	if !isWithin(list, lastMonth, monthlyBackups) {
		// We have no monthly backup, set a tag then.
		out[backupMonthly] = "true"
	}
	envTags := tagsFromEnv(os.Environ())
	// Merge tag maps into tag map overwriting existing ones.
	for k, v := range envTags {
		out[k] = v
	}
	return out, nil
}

// Mark backup sets for a continuous lifecycle.
func (c *ContinuousLifecycle) Mark(ctx context.Context) (map[string]string, error) {
	out := map[string]string{
		variantTagName: txLogArchiving,
	}
	envTags := tagsFromEnv(os.Environ())
	// Merge tag maps into tag map overwriting existing ones.
	for k, v := range envTags {
		out[k] = v
	}
	return out, nil
}

var (
	envVarTagPrefix         = envVarPrefix + "_TAGS"
	envVarTagSeparator byte = '_'
)

// Return tag list of relevant env vars to be used as tags (labels). It expects
// a slice of strings in the format key=value, e.g. the result of os.Environ().
func tagsFromEnv(in []string) map[string]string {
	env := make(map[string]string)
	for _, v := range in {
		vv := strings.Split(v, "=") // vv = EXAMPLE_TAGS_POD_NAME=mypodname
		if len(vv) != 2 {
			continue // Ignore invalid env format key=value (shouldn't happen).
		}
		key, val := vv[0], vv[1] // key = EXAMPLE_TAGS_POD_NAME
		// Filter our prefix to figure out the new tag name.
		n := strings.Index(key, envVarTagPrefix)
		if n < 0 {
			// Not a match, continue.
			continue
		}
		key = strings.TrimPrefix(key[n:], envVarTagPrefix) // key = _POD_NAME
		if key == "" {
			// Not an acceptable tag name.
			continue
		}
		if key[0] != envVarTagSeparator || len(key) < 2 {
			// Not an acceptable tag name.
			continue
		}
		key = key[1:]              // We know we have at least 2 bytes, key = POD_NAME
		key = strings.ToLower(key) // key = pod_name
		env[key] = val
	}
	return env
}

// filterFunc returns true if the backup item should be considered in isWithin()
// checks. True = consider, false = skip as a valid backup.
type filterFunc func(*backupItem) bool

var weeklyBackups = func(it *backupItem) bool {
	return it.Weekly
}

var monthlyBackups = func(it *backupItem) bool {
	return it.Monthly
}

func isWithin(list []*backupItem, date time.Time, f filterFunc) bool {
	for _, v := range list {
		if consider := f(v); !consider {
			continue
		}
		if v.LastModified.After(date) {
			return true
		}
	}
	return false
}

func (s *ContinuousLifecycle) ExpirationTime(t time.Time) time.Time {
	if s.max == 0 {
		return time.Time{}
	}
	return t.Add(s.max)
}

func expiryString(exp ExpirationTimer, id string) (string, error) {
	t, err := ULIDTime(id)
	if err != nil {
		return "", err
	}
	expt := exp.ExpirationTime(t)
	if expt.IsZero() {
		return "", nil
	}
	return expt.Format(time.RFC3339), nil
}

func appendLifecycleTags(ctx context.Context, lifecycle Lifecycler, tags map[string]string) (map[string]string, error) {
	if lifecycle != nil {
		ltags, err := lifecycle.Mark(ctx)
		if err != nil {
			return nil, fmt.Errorf("error while marking backup: %w", err)
		}
		if tags == nil {
			tags = make(map[string]string)
		}
		for k, v := range ltags {
			tags[k] = v
		}
	}
	return tags, nil
}
