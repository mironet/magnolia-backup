package main

import (
	"fmt"
	"regexp"
	"strings"
)

// podOrdinal returns the ordinal number of this pod. Useful for statefulSets
// when you want to 1:1 match pods based on their ordinal number e.g.
//
//  app-pod-0	-> db-pod-0
//  app-pod-1	-> db-pod-1
//  app-pod-2	-> db-pod-2
//  ...
func podOrdinal(hostname string) (string, error) {
	const rePodOrdinal = `-[\d]+$`
	hostname = strings.TrimSpace(hostname)
	re := regexp.MustCompile(rePodOrdinal)
	sub := re.FindString(hostname)
	if sub == "" {
		return "", fmt.Errorf("no ordinal number found in hostname: %s", hostname)
	}
	return strings.TrimPrefix(sub, "-"), nil
}
