package main

import (
	"context"

	"github.com/sirupsen/logrus"
)

const (
	maxWebhooks = 10 // Send this many webhooks concurrently.
)

// sendWebhooks dispatches all webhooks asynchronously.
func (s *server) sendWebhooks(ctx context.Context, objectName string) {
	sem := make(chan struct{}, maxWebhooks)

	send := func(hook webhook) {
		defer func() {
			select {
			case <-sem:
			case <-ctx.Done():
			}
		}()
		logrus.Infof("sending webhook: %s", hook)
		if err := hook.Send(ctx, objectName); err != nil {
			logrus.Errorf("error while sending webhook: %v", err)
			return
		}
		logrus.Infof("webhook sent successfully: %s", hook)
	}

	go func() {
		for _, v := range s.webhooks {
			select {
			case sem <- struct{}{}:
				go send(v)
			case <-ctx.Done():
				return
			}
		}
	}()
}
