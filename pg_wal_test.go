package main

import "testing"

func Test_pgParseVersionString(t *testing.T) {
	type args struct {
		version string
	}
	tests := []struct {
		name    string
		args    args
		wantMaj int
		wantErr bool
	}{
		{
			name: "simple version",
			args: args{
				version: "12.15",
			},
			wantMaj: 12,
			wantErr: false,
		},
		{
			name: "ubuntu packaged pg version",
			args: args{
				version: "14.9 (Ubuntu 14.9-0ubuntu0.22.04.1)",
			},
			wantMaj: 14,
			wantErr: false,
		},
		{
			name: "with bugfix version",
			args: args{
				version: "12.15.2",
			},
			wantMaj: 12,
			wantErr: false,
		},
		{
			name: "only major version",
			args: args{
				version: "12",
			},
			wantMaj: 12,
			wantErr: false,
		},
		{
			name: "invalid version",
			args: args{
				version: "dgshjkahgr",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotMaj, err := pgParseVersionString(tt.args.version)
			if (err != nil) != tt.wantErr {
				t.Errorf("pgParseVersionString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotMaj != tt.wantMaj {
				t.Errorf("pgParseVersionString() gotMaj = %v, want %v", gotMaj, tt.wantMaj)
			}
		})
	}
}
