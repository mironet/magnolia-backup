package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mironet/magnolia-backup/api"
)

func initTLSConfig(caCertFile, certFile, keyFile string) (*tls.Config, error) {
	caCert, err := os.ReadFile(caCertFile)
	if err != nil {
		return nil, fmt.Errorf("could not read caCert file: %w", err)
	}
	pool := x509.NewCertPool()
	pool.AppendCertsFromPEM(caCert)

	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return nil, fmt.Errorf("could not load client certificate: %w", err)
	}
	cfg := &tls.Config{
		RootCAs:      pool,
		Certificates: []tls.Certificate{cert},
		MinVersion:   tls.VersionTLS12,
	}
	return cfg, nil
}

var bootCmd = &cobra.Command{
	Use:   "boot",
	Short: "Lock and fetch content from other public instances.",
	RunE: func(cmd *cobra.Command, _ []string) error {
		vip := vipers["boot"]
		hostname := vip.GetString("hostname")

		var dataSourceURL *url.URL
		if v := vip.GetString("datasource"); v != "" {
			var err error
			dataSourceURL, err = url.ParseRequestURI(v)
			if err != nil {
				return fmt.Errorf("could not parse url --datasource: %w", err)
			}
		}

		// If we are doing a content sync from another instance ...
		var cli *api.Client
		if dataSourceURL == nil {
			// ... we require TLS.
			tlsCfg, err := initTLSConfig(
				vip.GetString("tls-ca-cert"),
				vip.GetString("tls-cert"),
				vip.GetString("tls-key"),
			)
			if err != nil {
				return fmt.Errorf("error loading TLS config: %w", err)
			}
			httpClient := &http.Client{
				Transport: &http.Transport{
					Dial: (&net.Dialer{
						Timeout:   30 * time.Second,
						KeepAlive: 30 * time.Second,
					}).Dial,
					TLSHandshakeTimeout:   30 * time.Second,
					ResponseHeaderTimeout: 15 * time.Minute,
					ExpectContinueTimeout: 1 * time.Second,
					IdleConnTimeout:       15 * time.Minute, // 90s is the default.
					TLSClientConfig:       tlsCfg,
				},
			}
			cli, err = api.New(
				api.WithHTTPClient(httpClient),
			)
			if err != nil {
				return fmt.Errorf("error creating HTTP client: %w", err)
			}
		}

		ctx := cmd.Context()
		conf, err := ReadViperFlags(vip)
		if err != nil {
			return fmt.Errorf("could not read object storage viper flags: %w", err)
		}
		if err := conf.Normalize(); err != nil {
			return fmt.Errorf("could not normalize storage config: %w", err)
		}
		// Create storage from config.
		stor, err := conf.CreateFrom(ctx)
		// Only display warnings for restores (not for content syncs).
		if err != nil && dataSourceURL != nil {
			logrus.Warnf("could not create object storage: %v", err)
			logrus.Warn("unable to recover from possible 404's occuring during point-in-time recovery restore")
		}
		if stor != nil {
			defer stor.Close()
		}

		if vip.GetInt("postgres-version") != -1 {
			logrus.Warn("postgres-version flag is deprecated, pg version can be determined internally using PG_VERSION file")
		}

		boot := &bootProcess{
			ctx:            ctx,
			stor:           stor,
			hostname:       hostname,
			cli:            cli,
			markerFilePath: filepath.Clean(vip.GetString("data-dir") + string(filepath.Separator) + vip.GetString("markerfile")),
			serviceName:    vip.GetString("service-name"),
			dataDir:        vip.GetString("data-dir"),
			dataSourceURL:  dataSourceURL,
		}
		for state := stateInit; state != nil; {
			state = state(boot)
		}
		return boot.Err
	},
}

func postgresVersionFromImageTag(tag string) (int, error) {
	if tag == "" {
		return 0, fmt.Errorf("postgres tag cannot be empty")
	}
	if tag == "latest" {
		return 0, fmt.Errorf("postgres tag cannot be latest")
	}
	parts := strings.Split(tag, ".")
	pgVersion, err := strconv.Atoi(parts[0])
	if err != nil {
		// Try to recover by spliting by hyphen separator. (Some postgres images
		// are tagged like "16-bullseye")
		parts := strings.Split(tag, "-")
		pgVersion, err = strconv.Atoi(parts[0])
		if err != nil {
			return 0, fmt.Errorf("could not extract postgres version from tag %s", tag)
		}
	}
	return pgVersion, nil
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["boot"] = v

	flags := bootCmd.Flags()

	// General flags.
	flags.String("hostname", "", "Override hostname, used for testing.")
	flags.String("data-dir", "", "Where to extract the source data to (data base data directory).")
	flags.String("markerfile", ".mgnl-backup-bootstrapped", "Path to markerfile used for checking if we already ran.")

	// Flags when syncing content from another instance.
	flags.String("service-name", "", "The other instance's data source service name.")

	// "Restore from configured source" mode flags.
	flags.String("datasource", "", "URL to metadata (bundle) file (in JSON format) for txlog restoration.")

	// Postgres version flag.
	flags.Int("postgres-version", -1, "Deprecated. No longer required because PG_VERSION file is used to determine the version internally.")

	// TLS flags. This is needed for the /download endpoint (mTLS, client
	// authentication) in case we sync content from another instance.
	flags.String("tls-ca-cert", "/opt/tls/ca.crt", "Path to CA cert PEM file.")
	flags.String("tls-cert", "/opt/tls/tls.crt", "Path to server certificate PEM file.")
	flags.String("tls-key", "/opt/tls/tls.key", "Path to private key PEM file (unencrypted).")

	// Object storage flags.
	AddObjectStorageFlags(flags)

	// Keep stuff compatible.
	v.RegisterAlias("s3-bucket", "bucket")
	v.RegisterAlias("gcs-bucket", "bucket")
	v.RegisterAlias("s3-keepdays", "keepdays")
	v.RegisterAlias("s3-cycle", "cycle")
	v.RegisterAlias("s3-prefix", "prefix")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

	rootCmd.AddCommand(timeCmdMiddleware(bootCmd))
}
