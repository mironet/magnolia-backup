# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [v0.12.3] - 2024-12-10

### Fixes

- [MR-91](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/91)
  The restore time must be "relocated" to UTC before formatting.

## [v0.12.2] - 2024-08-13

### Fixes

- [MR-89](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/89)
  Multisource configs must preserve global readonly information.

## [v0.12.1] - 2024-05-21

All fixes in this release have been introduced by
[MR-86](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/86) &
[MR-87](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/87).

### Fixes

- Pod ordinal check in content sync too simple to determine whether "we are the
  first pod in the deployment". Instead check if any endpoint for the respective
  db service is available.
- Return explicit error in case recovery from txlog not found error is not
  possible.
- Fix deprecation message of postgres-version flag.
- Don't display warnings, which are only relevant for restores, when doing a
  content sync.

## [v0.12.0] - 2024-04-11

All additions and changes in this release have been introduced by
[MR-85](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/85)

### Added

- Define a max txlog size for which no more txlog merges are attempted. It can
  be set via the `--tx-log-size-threshold` flag or the
  `MGNLBACKUP_TX_LOG_SIZE_THRESHOLD` environment variable (defaults to 16 GiB).

### Changed

- Upgrade golang.org/x/net. Just in case. Because of
  <https://pkg.go.dev/vuln/GO-2024-2687>.

## [v0.11.2] - 2024-03-12

All additions and changes in this release have been introduced by
[MR-82](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/82)

### Fixed

- Recreate wal parser in case of WAL page parsing error.

### Changed

- Remove unused parameters.
- Upgrade protobuf module.

## [v0.11.1], [v0.10.5], [v0.9.2] - 2024-03-07

### Fixed

- Bug in function lastCommitedTxTime() (used during restores) revealed by leap
  year 2024: The function determines the last committed transaction time by
  reading the last wal segment. Starting from 1st of March 2024, the returned
  time was off by one day. With the fix the time is now again determined
  correctly.

## [v0.11.0] - 2023-12-12

All additions and changes in this release have been introduced by
[MR-79](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/79)

### Added

- In archvie: Move unassignable wal segments & backup history files to ".old"
  subdirectory.

### Changed

- Upgrade to go1.21.

## [v0.10.4] - 2023-12-11

All fixes and changes in this release have been introduced by
[MR-77](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/77).

### Fixed

- TLS config & hostname flags are not required for restores.
- Docker compose file for restore (used for local testing).

### Changed

- Mark `boot` command flag `postgres-version` as deprecated. Instead during
  restores determine postgres version using the `PG_VERSION` file which is part
  of the base backup.
- Make Test_findQuickestEndpoint more robust.
- Change getPgServerVersion() func (which is used during startPgBackup() to
  determine the postgres major version).

## [v0.10.3] - 2023-11-08

### Fixed

- [MR-74](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/74) PG
  version must only be read in pg wal mode.

## [v0.10.2] - 2023-11-02

### Fixed

- [MR-72](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/72) PG
  version must not be read in readonly mode.

## [v0.10.1] - 2023-11-02

### Fixed

- [MR-71](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/71)
  Storage prefix should not be mandatory.

## [v0.10.0] - 2023-10-24

### Added

- [MR-68](https://gitlab.com/mironet/magnolia-backup/-/merge_requests/68)
  Automated handling of Postgres backup commands. They have changed in version
  15.

## [v0.9.1] - 2023-09-05

### Changed

- Refactor object storage config & creation code.

### Fixed

- During backup restore: Recover from a not found error caused by txlog merge.

## [v0.9.0] - 2023-07-13

### Changed

- Rework recovery/restore mechanism to enable support for postgres >= 12.

### Added

- Extract last committed transaction time from wal segments in order to decide
  whether recovery_target_time must be set during restore (for postgres >= 13).
- Postgres version tag to uploaded backup data.

## [v0.8.7] - 2023-07-12

### Changed

- Upgrade minio-go version to also support aws s3 region eu-south-2.
- Upgrade to go1.20.

## [v0.8.6]

Was only a release candidate. It has ultimately been released as
[`v0.9.1`](#v091---2023-09-05).

## [v0.8.5]

Is still only in the state of being a release candidate and might be released in
the future.

## [v0.8.4] - 2023-06-07

### Fixed

- Keep bundles indefinitely if keepdays is set to 0.

## [v0.8.3] - 2023-05-24

### Fixed

- Zero time displayed for non ULID object keys in html list output.

## [v0.8.2] - 2023-05-04

### Fixed

- ULID backup name only required in wal mode.
- Must list backup items without tag "variant=tx_log_archiving" (if not in pg_wal mode).
- SimpleLifecycle.Sweep().

## [v0.8.0] - 2023-02-14

### Added

- Azure Blob Storage support.

## [v0.7.4] - 2023-01-25

### Fixed

- Use ComposeObject instead of CopyObject in Tag method of s3 storage.

## [v0.7.3] - 2023-01-19

### Fixed

- Fix: Only list storage objects that have the tag `variant=tx_log_archiving`.

## [v0.7.2] - 2023-01-19

### Fixed

- Don't need to initialize & use backup metas list in read only mode.

## [v0.7.1] - 2023-01-19

### Fixed

- Do not enforce prefix to be set in s3 multi-source storage config.

## [v0.7.0] - 2023-01-19

### Changed

- Use metadata instead of tags in s3 object storage. Use fallback to object
  tagging if the tags could not be fetched from metadata.

### Fixed

- Actually drop opOnDumpErr struct.
- Move dump ports to the correct container in docker compose file.

## [v0.6.2] - 2023-01-17

### Added

- Flag to accept any TLS certificate if secure=true.

## [v0.6.1] - 2023-01-17

### Added

- Documentation on how to test download endpoint.

### Changed

- Simplify getNewDump() call.
- Make retrying of dump on failure a default.
- Remove errNothingToSync (was never thrown, only checked).
- Regenerate mTLS test certs.

### Fixed

- [Issue-20](https://gitlab.com/mironet/magnolia-backup/-/issues/20) Use (&
  cancel) a separate context for file syncs.
- Allow omitting pod_name & namespace environment variables in read-only mode.

## [v0.6.0] - 2023-01-10

### Added

- Add list options to the list command of the storage interface.
- Implement withSuffix & withTags list options in gcs & s3 storages.
- Add parts sweeping routine for s3 storages.

### Changed

- [Issue-17](https://gitlab.com/mironet/magnolia-backup/-/issues/17) Don't use
  fsnotify to sync tx logs. Repeatedly (every 31 seconds) run a routine that
  syncs all pg wal segments in the archive.
- Use metadata files stored in the object storage to assign the wal segments to
  the correct base backup. Only list backup meta files once on server start.
  Afterwards keep book on the backup meta data by adding a meta data entry
  whenever a new basebackup is created.
- Rename parts objects. Such that they have a different prefix than the other
  objects of the same instance. This way we omit the parts "for free" when
  listing objects.
- Use weighted semaphores instead of struct channels to lock/unlock.
- Sweep at most every defined interval.

### Fixed

- Allow only one txlog merge at a time.
- HasBaseBackup() check.
- Ignore objects without ulid key in Section() method. But log a warning if such
  an object is encountered.
- Must retry if dump in cronjob fails.
- Do not refresh the cache every minute.
