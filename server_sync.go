package main

import (
	"archive/tar"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/klauspost/compress/gzip"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"golang.org/x/exp/slices"
	"golang.org/x/sync/errgroup"
)

const (
	fileExtensionPart  = "part"
	fileExtensionTarGz = "tar.gz"
	methodUpload       = "upload"
	methodMerge        = "merge"

	txlogSuffix = "txlog"

	oldArchiveDir = ".old"
)

func listBackupMetas(ctx context.Context, storage ObjectStorage) (*backupMetaList, error) {
	// List all backup items.
	metaItems, err := storage.List(ctx, WithTags{
		Tags: map[string]string{
			tagKeyNamespace: os.Getenv(namespaceEnvVar),
			tagKeyPodName:   os.Getenv(podNameEnvVar),
		},
	}, WithSuffix{
		// We care only about the base backup meta items.
		Suffix: metaFileSuffix,
	})
	if err != nil {
		return nil, fmt.Errorf("could not list backup items: %w", err)
	}

	// Parse a base backup meta struct for each base backup meta item.
	backupMetas := NewBackupMetaList()
	for _, backupItem := range metaItems {
		backupMeta, err := backupItem.parseBackupMeta(ctx, storage)
		if err != nil {
			return nil, fmt.Errorf("could not parse base backup meta object: %w", err)
		}
		backupMetas.Add(backupMeta)
	}

	return backupMetas, nil
}

func moveToOld(fpath, subdirectory string) error {
	dest := filepath.Join(filepath.Dir(fpath), oldArchiveDir, subdirectory, filepath.Base(fpath))
	if err := os.MkdirAll(filepath.Dir(dest), 0755); err != nil {
		return fmt.Errorf("could not create directory %s: %w", filepath.Dir(dest), err)
	}
	return os.Rename(fpath, dest)
}

func (s *server) compileSyncMap(list []finfo) map[backupMeta][]finfo {
	syncMap := make(map[backupMeta][]finfo)
	backupMetas := s.backupMetas.Sorted()
	for _, v := range list {
		name := filepath.Base(v.path)
		idx := -1
		// Loop throught the backup meta files sorted by newest ulid.
		for i, backupMeta := range backupMetas {
			if name >= backupMeta.firstWALSegment {
				idx = i
				break
			}
		}
		if idx < 0 {
			logrus.Warnf("cannot assign wal file %s to a base backup", v.path)
			if v.info != nil && time.Since(v.info.ModTime()) > 48*time.Hour {
				subdirectoryName := "unassignable"
				if err := moveToOld(v.path, subdirectoryName); err != nil {
					logrus.Errorf("could not move %s to old: %v", v.path, err)
				} else {
					logrus.Infof("moved %s to %s", v.path, filepath.Join("/.old", subdirectoryName))
				}
			}
			continue
		}

		// Create a new sync list if there is non for the current backup yet.
		if _, ok := syncMap[backupMetas[idx]]; !ok {
			syncMap[backupMetas[idx]] = make([]finfo, 0)
		}

		// Append v to the sync list.
		elem := syncMap[backupMetas[idx]]
		elem = append(elem, v)
		syncMap[backupMetas[idx]] = elem
	}

	return syncMap
}

// syncTxLogDir copies all files from this directory to a tarball on object storage.
// It is intended for use of WALs (transaction logs) of databases and the
// resulting tarball's name contains the modification time of the first log file
// encountered. After successfully uploading files they are deleted from the
// local disk.
func (s *server) syncTxLogDir(ctx context.Context, list []finfo, exp ExpirationTimer) error {
	switch s.syncDirSem.TryAcquire(1) {
	case true:
		defer func() {
			s.syncDirSem.Release(1)
		}()
	default:
		return errBusy
	}

	// The files in the incoming list can belong to different base backups. Thus
	// we must first establish which files belong to which base backup. For that
	// we compile a sync map. Each element of the map maps from a backup
	// metadata stuct (i.e. from a base backup) to a a list of wal segments that
	// must be synced to this base backup.
	syncMap := s.compileSyncMap(list)

	// Sync one txlog object (containing all files from the syncList) for each
	// base backup in the syncMap.
	errs := make([]error, 0)
	for backupMeta, syncList := range syncMap {
		expt, err := expiryString(exp, backupMeta.ulid)
		if err != nil {
			return err
		}
		tags := map[string]string{
			tagKeyIsBaseBackup:    "false",
			tagKeyIsTxLog:         "true",
			tagKeyBackupID:        backupMeta.ulid,
			tagKeyExpiry:          expt,
			tagKeyPostgresVersion: s.pgVersion,
		}

		syncCtx, cancelSyncCtx := context.WithCancel(ctx)

		var listc = make(chan finfo)
		go func() {
			defer close(listc)
			for _, v := range syncList {
				select {
				case listc <- v:
				case <-syncCtx.Done():
					return
				}
			}
		}()

		l := make([]string, len(syncList))
		var oldest time.Time
		for i, elem := range syncList {
			l[i] = filepath.Base(elem.path)
			modTime := elem.info.ModTime()
			if oldest.IsZero() ||
				modTime.Before(oldest) {
				oldest = modTime
			}
		}
		objectName := newTxLogName(s.prefix, backupMeta.ulid, oldest)
		logrus.Debugf("syncing files %v to object %s", l, objectName)
		_, err = doSyncFiles(syncCtx, listc, s.syncDirPath, objectName, s.storage, s.compressionLevel, true, tags, s.uploadReadLimit)
		if err != nil {
			errs = append(errs, err)
		}
		cancelSyncCtx()
	}

	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errs[0]
	default:
		var sb strings.Builder
		for i, e := range errs {
			sb.WriteString(fmt.Sprintf("error %d: %v;", i, e))
		}
		return fmt.Errorf(sb.String())
	}
}

func (s *server) txlogMerge(ctx context.Context, backupID string) {
	switch s.syncDirSem.TryAcquire(1) {
	case true:
		defer func() {
			s.syncDirSem.Release(1)
		}()
	default:
		logrus.Errorf("could not merge tx logs: :%v", errBusy)
		return
	}

	// Compose the path of the directory in which we want to merge transaction
	// logs.
	txlogsDir := path.Join(s.prefix, backupID, txlogSuffix)

	// Fetch all items in the transaction log directory.
	allItems, err := s.storage.List(ctx, WithPrefix{
		Prefix: txlogsDir,
	})
	if err != nil {
		logrus.Errorf("could not merge tx logs in %q: could not list backup items: %v", txlogsDir, err)
		return
	}

	// Filter items such that only txlogs for the current base backup remain.
	txlogItems := make(backupItemList, 0)
	for _, item := range allItems {
		if item.Tags[tagKeyBackupID] != backupID {
			continue
		}
		if item.Tags[tagKeyIsTxLog] != "true" {
			continue
		}
		txlogItems = append(txlogItems, item)
	}

	// Sort the tx logs by their ULID.
	sort.Sort(byULID(txlogItems))

	// Trim list elements that should not be merged according to our merge
	// algorithm.
	startIdx := 0
	for {
		// Stop if we have seen all tx log items.
		if startIdx >= len(txlogItems) {
			// In that case we know that for no index i of the objects list the
			// desired constraint on the sizes is met. Thus we don't need to merge
			// any tx logs.
			logrus.Debugf("nothing to merge in %q", txlogsDir)
			return
		}
		// Stop if the total size of the tx logs that follow the current one
		// (including the current one) is at least twice the size of the current
		// one AND at most the txlog size threshold.
		if txlogItems[startIdx:].totalSize() >= 2*txlogItems[startIdx].Size &&
			2*txlogItems[startIdx].Size <= s.txlogSizeThreshold {
			break
		}

		startIdx++
	}
	// Here it holds that whatever follows after the tx log with index startIdx
	// (including the tx log with that index), has at least twice the size of
	// the object with index startIdx.

	// Merge tx log items only from start index on.
	txlogItems = txlogItems[startIdx:]

	// Compose tags and object name of merged tx log.
	mergeName, err := txlogItems.mergeName()
	if err != nil {
		logrus.Errorf("could not get merge name from tx log list: %v", err)
		return
	}
	tags, err := txlogItems.mergeTags()
	if err != nil {
		logrus.Errorf("could not get merge tags from tx log list: %v", err)
		return
	}

	// Make sure that the "is_txlog"-tag of the merged tx log is set to "false".
	// We will only set it to "true" once we successfully uploaded the merged
	// object. (Note: Successfully oploaded means not only that the merged
	// object was uploaded, but also that the objects it is composed of are
	// deleted.)
	tags[tagKeyIsTxLog] = "false"

	// Stream, unzip, untar and write to merge stream.
	r, w := io.Pipe()
	tw := tar.NewWriter(w)
	go func() {
		defer w.Close()
		for _, txlogItem := range txlogItems {
			// Stream item.
			rc, err := s.storage.Get(ctx, txlogItem.Key)
			if err != nil {
				w.CloseWithError(fmt.Errorf("could not get object %s: %w", txlogItem.Key, err))
				return
			}
			func() {
				// Unzip the item (while streaming).
				zr, err := gzip.NewReader(rc)
				if err != nil {
					w.CloseWithError(fmt.Errorf("expected gzip-compressed content in object %s: %w", txlogItem.Key, err))
					return
				}
				defer zr.Close()
				tr := tar.NewReader(zr)
				for {
					// Expect the next tar header.
					f, err := tr.Next()
					if errors.Is(err, io.EOF) {
						break
					}
					if err != nil {
						w.CloseWithError(fmt.Errorf("tar error: %w", err))
						return
					}
					// Write the header to the tar writer.
					err = tw.WriteHeader(f)
					if err != nil {
						w.CloseWithError(fmt.Errorf("could not write header: %w", err))
						return
					}
					// Copy the content to the tar writer.
					_, err = io.Copy(tw, tr)
					if err != nil {
						w.CloseWithError(fmt.Errorf("could not copy object %s to writer: %w", txlogItem.Key, err))
						return
					}
				}
				rc.Close()
			}()
		}
	}()

	// Upload the merged tx log.
	_, err = s.storage.Upload(ctx, mergeName, newGzipReader(r, s.compressionLevel), tags)
	if err != nil {
		logrus.Errorf("could not merge tx logs in %q: could not upload merged object %q: upload failed: %v", txlogsDir, mergeName, err)
		return
	}

	// Delete all the objects the merged tx log is composed of.
	toDeleteObjs := make(chan string)
	go func() {
		defer close(toDeleteObjs)
		for _, v := range txlogItems {
			select {
			case toDeleteObjs <- v.Key:
			case <-ctx.Done():
				return
			}
		}
	}()
	errChan := s.storage.Delete(ctx, toDeleteObjs)
	err = nil
	for err = range errChan {
		logrus.Errorf("deletion error: %v", err)
	}
	if err != nil {
		return
	}

	// Set the "is_txlog"-tag to "true" once we successfully uploaded the merged
	// object and deleted all the objects it is composed of.
	err = s.storage.Tag(ctx, mergeName, map[string]string{tagKeyIsTxLog: "true"})
	if err != nil {
		logrus.Errorf("could not merge tx logs in %q: could not update %q-tag on %s: %v", txlogsDir, tagKeyIsTxLog, mergeName, err)
		return
	}

	// Report what was merged.
	names := make([]string, len(txlogItems))
	for i, item := range txlogItems {
		names[i] = item.Key
	}
	logrus.Infof("merged txlogs %s into %s in %q", names, mergeName, txlogsDir)
}

type finfo struct {
	path        string      // Path where the file is found on the local system.
	info        os.FileInfo // Information about the file (permissions, size etc.)
	targetPath  string      // Target path in the tar archive for this file, relative to rootDir, with basename.
	deleteAfter bool        // Delete the file after the upload operation.
}

func copyFileToTar(f finfo, w *tar.Writer, rootDir string) error {
	var path string
	var err error
	switch f.targetPath {
	case "":
		// Figure out relative path to scanning dir.
		path, err = filepath.Rel(rootDir, f.path)
		if err != nil {
			return fmt.Errorf("error finding path for %s relative to %s: %w", f.path, rootDir, err)
		}
	default:
		path = f.targetPath
	}

	// Open file to upload.
	file, err := os.Open(f.path)
	if err != nil {
		return err
	}
	defer file.Close() // We don't care if closing returns an error.

	// Check for symlinks.
	var link string
	if f.info.Mode()&os.ModeSymlink == os.ModeSymlink {
		if link, err = os.Readlink(f.path); err != nil {
			return fmt.Errorf("could not find symlink's target: %w", err)
		}
	}
	// Create a new tar header for the file. This automatically handles
	// directories and other file system objects correctly.
	hdr, err := tar.FileInfoHeader(f.info, link)
	if err != nil {
		return fmt.Errorf("could not prepare tar file header: %w", err)
	}
	hdr.Name = filepath.ToSlash(path) // Add relative path to base name.
	if err := w.WriteHeader(hdr); err != nil {
		return fmt.Errorf("error writing tar file headers: %w", err)
	}

	if !f.info.Mode().IsRegular() {
		return nil // Nothing more to do here.
	}
	if _, err := io.Copy(w, file); err != nil {
		return fmt.Errorf("error writing tar file body: %w", err)
	}
	return nil
}

func uploadToObject(ctx context.Context, list <-chan finfo, rootDir, objectName string, upMerge UploadMerger, compressionLevel int,
	tags map[string]string, readLimit int) (int64, error) {

	// Copy file to a temporary location before uploading.
	const max = 4
	g, ctx := errgroup.WithContext(ctx)

	filec := make(chan finfo)
	workers := int32(max)

	for i := 0; i < max; i++ {
		j := i
		g.Go(func() error {
			logrus.Debugf("starting file copy goroutine #%d", j)
			defer func(j int) {
				logrus.Debugf("stopping file copy goroutine #%d", j)
				// Last one out closes shop.
				if atomic.AddInt32(&workers, -1) == 0 {
					close(filec)
				}
			}(j)
			for f := range list {
				// If this is not a regular file, just send it off. No need to
				// copy it.
				if !f.info.Mode().IsRegular() {
					select {
					case filec <- f:
					case <-ctx.Done():
						return ctx.Err()
					}
					continue
				}

				tf, err := os.CreateTemp("", "")
				if err != nil {
					return fmt.Errorf("could not create temporary file while copying: %w", err)
				}
				err = func() error {
					defer tf.Close()
					file, err := os.Open(f.path)
					if err != nil {
						if os.IsNotExist(err) {
							logrus.Warnf("error opening source file %s, ignoring, err = %v", f.path, err)
							return nil
						}
						return fmt.Errorf("error opening source file %s: %w", f.path, err)
					}
					defer file.Close()

					// Actually do copy the file.
					logrus.Debugf("copying file %s to %s ...", file.Name(), tf.Name())
					if _, err := io.Copy(tf, file); err != nil {
						return fmt.Errorf("error copying to temp file: %w", err)
					}

					// Sync the file (flush to disk).
					if err := tf.Sync(); err != nil {
						return fmt.Errorf("error while sync()-ing file: %w", err)
					}

					// Fixup the source and target path.
					if f.targetPath == "" {
						path, err := filepath.Rel(rootDir, f.path)
						if err != nil {
							return fmt.Errorf("error finding path for %s relative to %s: %w", f.path, rootDir, err)
						}
						f.targetPath = path
					}
					f.deleteAfter = true
					f.path = tf.Name()
					fi, err := tf.Stat()
					if err != nil {
						return err
					}

					// Override file size info.
					f.info = fileInfo{
						FileInfo: f.info,
						size:     fi.Size(),
					}

					// Now send it off to the copy to tar goroutine.
					select {
					case filec <- f:
					case <-ctx.Done():
						return ctx.Err()
					}
					return nil
				}()
				if err != nil {
					return err
				}
			}
			return nil
		})
	}

	// Upload the files to a single tarball.
	r, w := io.Pipe()
	g.Go(func() error {
		tw := tar.NewWriter(w)

		// Make sure the writers are closed when either the context or this go
		// routine is done.
		closeWriters := func() {
			if err := tw.Close(); err != nil {
				w.CloseWithError(err)
			}
			w.Close()
		}
		once := sync.Once{}
		defer func() {
			once.Do(closeWriters)
		}()
		go func() {
			<-ctx.Done()
			once.Do(closeWriters)
		}()

		for v := range filec {
			logrus.Debugf("syncing file %s ...", v.path)
			if err := copyFileToTar(v, tw, rootDir); err != nil {
				w.CloseWithError(err)
				return err
			}
			if v.deleteAfter {
				logrus.Debugf("deleting temp file %s ...", v.path)
				if err := os.Remove(v.path); err != nil {
					logrus.Warnf("could not delete temp file %s: %v", v.path, err)
				}
			}
		}

		return nil
	})

	var size int64
	g.Go(func() error {
		objectName = objectName + ".tar.gz"
		backoffFunc := func() backoff.BackOff {
			return backoff.NewExponentialBackOff()
		}
		var uploadErr, mergeErr error
		size, uploadErr, mergeErr = divideUploadObject(
			ctx, objectName, newGzipReader(r, compressionLevel), upMerge, tags, readLimit, backoffFunc,
		)
		if uploadErr != nil {
			return fmt.Errorf("error while uploading: %w", uploadErr)
		}
		if mergeErr != nil {
			// TODO: Can we be less "destructive" when "only" a merge error
			// happend?
			return fmt.Errorf("error while merging: %w", mergeErr)
		}
		return nil
	})

	return size, g.Wait()
}

func measureAndLogDuration(timer *prometheus.Timer, size int64, logger *logrus.Entry) {
	duration := timer.ObserveDuration()
	speed := rateString(duration, size)
	logger.Infof("transferred %s in %s (%s)", ByteCountBinary(size), duration, speed)
}

func doSyncFiles(ctx context.Context, list <-chan finfo, rootDir, objectName string, upMerge UploadMerger, compressionLevel int,
	deleteAfter bool, tags map[string]string, readLimit int) (info *objectInfo, err error) {
	logger := logrus.WithField("facility", "syncFiles")
	if objectName == "" {
		return nil, fmt.Errorf("unexpected empty object name")
	}
	timer := prometheus.NewTimer(metricSummary)
	defer func() {
		if err != nil {
			metricErrors.Inc()
			return
		}
	}()

	var files []finfo
	var filesc = make(chan finfo)
	go func() {
		defer close(filesc)
		for v := range list {
			files = append(files, v)
			select {
			case filesc <- v:
			case <-ctx.Done():
				return
			}
		}
	}()
	size, err := uploadToObject(ctx, filesc, rootDir, objectName, upMerge, compressionLevel, tags, readLimit)
	if err != nil {
		return nil, fmt.Errorf("doSyncDir(): %w", err)
	}

	// After a successful upload remove all files we just synced and then empty
	// directories.
	measureAndLogDuration(timer, size, logger)
	if deleteAfter {
		if err := removeFilesAndDirs(files, logger); err != nil {
			return nil, fmt.Errorf("doSyncDir(): %w", err)
		}
	}

	return &objectInfo{Name: objectName, Size: size}, nil
}

var (
	pgWalExcludedPrefixes = []string{
		"pg_wal/",
		"pg_replslot/",
		"pg_dynshmem/",
		"pg_notify/",
		"pg_serial/",
		"pg_snapshots/",
		"pg_stat_tmp/",
		"pg_subtrans/",
		"pgsql_tmp",
	}
	pgWalExcludedFilenames = []string{
		"postmaster.pid",
		"postmaster.opts",
		"pg_internal.init",
	}
	pgWalExcludedSuffixes = []string{
		"/pg_internal.init",
	}
)

func includeInBasebackup(path string) bool {
	if path == "" {
		return false
	}
	if hasPrefixes(path, pgWalExcludedPrefixes) {
		return false
	}
	if hasSuffixes(path, pgWalExcludedSuffixes) {
		return false
	}
	if slices.Contains(pgWalExcludedFilenames, path) {
		return false
	}
	return true
}

func hasPrefixes(in string, prefixes []string) bool {
	for _, v := range prefixes {
		if strings.HasPrefix(in, v) {
			return true
		}
	}
	return false
}

func hasSuffixes(in string, suffixes []string) bool {
	for _, v := range suffixes {
		if strings.HasSuffix(in, v) {
			return true
		}
	}
	return false
}

func removeFilesAndDirs(list []finfo, logger *logrus.Entry) error {
	for _, v := range list {
		if v.info.IsDir() {
			continue // Skip directories for now.
		}
		if err := os.Remove(v.path); err != nil {
			return fmt.Errorf("error removing file %s: %w", v.path, err)
		}
		logger.Debugf("removed file %s", v.path)
	}

	// Now take care of empty dirs.
	for _, v := range list {
		if !v.info.IsDir() {
			continue // Skip files.
		}
		if err := removeEmptyDirs(v.path, func(path string) {
			logger.Debugf("removed dir %s", path)
		}); err != nil {
			return fmt.Errorf("error removing dir %s after successful upload: %w", v.path, err)
		}
	}

	return nil
}

// removeEmptyDirs recursively removes all empty dirs starting at 'dir'. It will
// remove 'dir' itself if it is empty. On each successful delete it will call
// 'callback' with the dir path it just deleted.
func removeEmptyDirs(dir string, callback func(dir string)) error {
	file, err := os.Open(dir)
	if err != nil && os.IsNotExist(err) {
		return nil // Simple case, nothing to do.
	}
	if err != nil {
		return fmt.Errorf("os.Open(): %w", err) // Report all other errors.
	}
	defer file.Close()
	contents, err := file.Readdir(-1)
	if err != nil && err != io.EOF {
		// Exit early in case of errors.
		return fmt.Errorf("file.Readdir(): %w", err)
	}
	// Recurse.
	for _, v := range contents {
		if !v.IsDir() {
			continue
		}
		// This could remove some more diretories.
		if err := removeEmptyDirs(filepath.Join(dir, v.Name()), callback); err != nil {
			return err
		}
	}

	// We need to re-open the dir since the OS could have been reshuffling
	// the directory list.
	file, err = os.Open(dir)
	if err != nil && os.IsNotExist(err) {
		return nil // Nothing to remove.
	}
	if err != nil {
		return fmt.Errorf("os.Open(): %w", err) // Report all other errors.
	}
	defer file.Close()

	// Check again if we are an empty dir now.
	contents, err = file.Readdir(-1)
	if err != nil && err != io.EOF {
		// A unrecoverable error.
		return fmt.Errorf("file.Readdir(-1): %w", err)
	}
	if len(contents) > 0 {
		return nil // We do not delete this directory since it is not empty.
	}

	// The dir is empty.
	if err := os.Remove(dir); err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("os.Remove(): %w", err)
	}
	if callback != nil {
		callback(dir)
	}
	return nil
}

func sum(summands []int64) int64 {
	sum := int64(0)
	for _, s := range summands {
		sum = sum + s
	}
	return sum
}

type toMergeObj struct {
	name string
	size int64
}

type toMergeObjList []toMergeObj

func (l toMergeObjList) names() []string {
	out := make([]string, len(l))
	for i, e := range l {
		out[i] = e.name
	}
	return out
}

func (l toMergeObjList) totalSize() int64 {
	out := int64(0)
	for _, e := range l {
		out = out + e.size
	}
	return out
}

func logAndCount(method string, err error) {
	switch method {
	case methodUpload, methodMerge:
	default:
		logrus.Errorf("could not log and count error for unknown method %q", method)
		return
	}
	logrus.Errorf("error for method %s: %v", method, err)

	// Count upload errors.
	label := labelUpload
	if method == methodMerge {
		label = labelMerge
	}
	count, err := metricUpMergeErrors.GetMetricWith(label)
	if err != nil {
		logrus.Errorf("could not get upmerge errors counter: %v", err)
		return
	}
	count.Add(1)
}

func divideUploadObject(ctx context.Context, objectName string, r io.Reader, upMerger UploadMerger,
	tags map[string]string, readLimit int, backoffFunc func() backoff.BackOff) (int64, error, error) {
	if readLimit <= 0 {
		return 0, fmt.Errorf("read limit must be a positive integer, but is %d", readLimit), nil
	}

	var wgMerge, wgUpload sync.WaitGroup
	sizes := make([]int64, 0)

	toMergeObjChan := make(chan toMergeObj, 100)

	// Start uploading go-routine.
	wgUpload.Add(1)
	var uploadErr error
	go func() {
		defer func() {
			close(toMergeObjChan)
			wgUpload.Done()
		}()
		divideReader := newDivideReader(r, readLimit)
		for divideReader.hasNext() {
			objectPartName := divideReader.nextObjectPartName(objectName)

			// Upload object part and retry in case of failure.
			var n int64
			var err error
			n, err = upMerger.Upload(ctx, objectPartName, divideReader.next(), nil)
			if err != nil {
				logAndCount(methodUpload, err)
				uploadErr = fmt.Errorf("error uploading backup %s: %w", objectPartName, err)
				return
			}
			sizes = append(sizes, n)

			// Either send the uploaded object to toMergeObjChan or exit the
			// go-routine if the context expired.
			select {
			case toMergeObjChan <- toMergeObj{
				name: objectPartName,
				size: n,
			}:
			case <-ctx.Done():
				uploadErr = ctx.Err()
				return
			}
		}
	}()

	// Start continuous merging go-routine.
	wgMerge.Add(1)
	var mergeErr error
	go func() {
		defer wgMerge.Done()
		objs := make(toMergeObjList, 0)
		closed := false

		ticker := time.NewTicker(time.Second)
		defer ticker.Stop()

		// Until toMergeObjChan is closed.
		for !closed {
			canRead := true
			// Either ...
			select {
			// ... receive the next "to merge"-object if it is ready ...
			case toMergeObj, open := <-toMergeObjChan:
				closed = !open
				if !closed {
					objs = append(objs, toMergeObj)
				}
			case <-ticker.C:
				// ... or note that we can not read at the moment (and therefore
				// want to progress with merging).
				canRead = false
			}

			// If we can no longer read more objects and at least two objects can
			// be merged.
			if !canRead && len(objs) > 1 {
				startIdx := 0
				for ; startIdx < len(objs) && objs[startIdx:].totalSize() < 2*objs[startIdx].size; startIdx++ {
					// Don't do anything inside the loop. We are only interested
					// in the value of startIdx after the loop.
				}
				// Here it holds that either startIdx is out of bounds (with
				// respect to the objects list) or that, whatever follows after
				// the object with index startIdx, has at least twice the size
				// of the object with index startIdx.

				if startIdx >= len(objs) {
					// In that case we know that for no index i of the objects
					// list the desired constraint on the sizes is met. Thus we
					// can continue reading further objects.
					continue
				}

				// We give the merged object the same name as the first to be
				// merged object had, but with an added letter "m" (to avoid
				// name collision).
				contMergeName := objs[startIdx].name + "m"

				// Merge all objects from the start index on and retry in case
				// of failure.
				op := func() error {
					err := upMerger.Merge(ctx, objs[startIdx:].names(), contMergeName, tags)
					if err != nil {
						logAndCount(methodMerge, err)
						return err
					}
					return nil
				}
				err := backoff.Retry(op, backoff.WithContext(backoffFunc(), ctx))
				// If there was a merge error.
				if err != nil {
					// Note the error ...
					mergeErr = err
					// ... and read from toMergeObjChan until its closed.
					for obj := range toMergeObjChan {
						_ = obj
					}
					closed = true
					continue
				}

				// If merging was successful, remember the name & size of the
				// merged object and remove all merged objects from the objects
				// list.
				objs[startIdx].name = contMergeName
				objs[startIdx].size = objs[startIdx:].totalSize()
				objs = objs[:startIdx+1]
			}
		}

		// Wait until the uploading go-routine is done and exit the continuous
		// merging go-routine if we encountered an upload or merge error.
		wgUpload.Wait()
		if uploadErr != nil || mergeErr != nil {
			return
		}

		// A final merge call is required here, to merge whatever is left to be
		// merged into the final object.
		mergeErr = upMerger.Merge(ctx, objs.names(), objectName, tags)
	}()

	// Wait until the continuous merging go-routine is done and return.
	wgMerge.Wait()
	return sum(sizes), uploadErr, mergeErr
}
