package main

import (
	"context"
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"sync"
)

type connectionCloser struct {
	ctx  context.Context // When this closes, cancel everything.
	conn *sql.Conn       // An open sql connection.
	// These byte slices will be filled with all the fields the closing command
	// returns. Postgres for example needs this information to be stored in
	// files for a restore to happen correctly. See
	// https://www.postgresql.org/docs/12/continuous-archiving.html#BACKUP-LOWLEVEL-BASE-BACKUP
	// for details.
	lastRequiredFile string
	labelfile        []byte
	mapfile          []byte
	// The postgres major version, used for selecting the correct pg backup
	// queries. See issue #24 for details:
	// https://gitlab.com/mironet/magnolia-backup/-/issues/24
	serverMajVersion int
	sync.Once
}

const (
	pgWalFileQuery     = `SELECT * FROM pg_walfile_name($1)`
	pgShowVersionQuery = "SHOW server_version;"
	pgStartBackup      = "pg_start_backup"
	pgStopBackup       = "pg_stop_backup"
	pgStartBackup15    = "pg_backup_start"
	pgStopBackup15     = "pg_backup_stop"
)

// This needs to happen on the same connection. This is why it is encapsulated
// here.
func (c *connectionCloser) Close() error {
	var err error
	c.Do(func() {
		maj := c.serverMajVersion
		var query string
		switch {
		case maj >= 11 && maj < 15:
			query = fmt.Sprintf(`SELECT * FROM %s(false, true);`, pgStopBackup)
		case maj >= 15:
			query = fmt.Sprintf(`SELECT * FROM %s(true);`, pgStopBackup15)
		}
		var lsn string
		defer c.conn.Close()
		row := c.conn.QueryRowContext(c.ctx, query)
		err = row.Scan(&lsn, &c.labelfile, &c.mapfile)
		if err != nil {
			err = fmt.Errorf("error calling %s: %w", query, err)
			return
		}
		err = c.conn.QueryRowContext(c.ctx, pgWalFileQuery, lsn).Scan(&c.lastRequiredFile)
		if err != nil {
			err = fmt.Errorf("error calling pg_walfile_name(%s): %w", lsn, err)
			return
		}
	})
	return err
}

func (c *connectionCloser) LastRequiredFile() string {
	return c.lastRequiredFile
}

func (c *connectionCloser) LabelFile() []byte {
	return c.labelfile
}

func (c *connectionCloser) MapFile() []byte {
	return c.mapfile
}

type MapAndLabelFiler interface {
	LabelFile() []byte
	MapFile() []byte
}

const (
	// The filenames for the label and map file are fixed, see here for details:
	// https://www.postgresql.org/docs/12/continuous-archiving.html#BACKUP-LOWLEVEL-BASE-BACKUP-NONEXCLUSIVE
	labelFileName = "backup_label"
	mapFileName   = "tablespace_map"
)

// Starts a backup and returns an opaque io.Closer to close the connection as
// soon as the backup has been done completely.
func startPgBackup(ctx context.Context, db *sql.DB, immediate bool) (*connectionCloser, string, error) {
	const label = "startPgBackup"
	// Check the version to select to correct query for starting a non-exclusive
	// backup.
	conn, err := db.Conn(ctx)
	if err != nil {
		return nil, "", fmt.Errorf("could not get a db connection: %w", err)
	}
	maj, err := getPgServerVersion(ctx, conn)
	if err != nil {
		return nil, "", fmt.Errorf("could not get pg server version: %w", err)
	}
	var query string
	switch {
	case maj >= 11 && maj < 15:
		query = fmt.Sprintf(`SELECT %s('%s', %t, %t);`, pgStartBackup, label, immediate, false)
	case maj >= 15:
		query = fmt.Sprintf(`SELECT %s('%s', %t)`, pgStartBackup15, label, immediate)
	default:
		return nil, "", fmt.Errorf("unknown major version number: %d", maj) // Not sure if that's sensible.
	}
	var lsn string
	row := conn.QueryRowContext(ctx, query)
	if err := row.Scan(&lsn); err != nil {
		return nil, "", fmt.Errorf("error starting pg backup: %w", err)
	}
	var walFileName string
	if err := conn.QueryRowContext(ctx, pgWalFileQuery, lsn).Scan(&walFileName); err != nil {
		return nil, "", fmt.Errorf("could not find wal file name for lsn %s: %w", lsn, err)
	}

	closer := &connectionCloser{
		ctx:              ctx,
		conn:             conn,
		serverMajVersion: maj,
	}
	return closer, walFileName, nil
}

func getPgServerVersion(ctx context.Context, conn *sql.Conn) (maj int, err error) {
	var version string
	if err := conn.QueryRowContext(ctx, pgShowVersionQuery).Scan(&version); err != nil {
		return 0, fmt.Errorf("error querying for version: %w", err)
	}
	// Parse the version string.
	return pgParseVersionString(version)
}

func pgParseVersionString(version string) (int, error) {
	parts := strings.Split(version, ".")
	if len(parts) < 1 {
		return 0, fmt.Errorf("could not parse version string: %s", version)
	}
	return strconv.Atoi(parts[0])
}
